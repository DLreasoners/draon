Version (1.5.1):
----------------------

From this version, IDDLReasoner is renamed DRAOn.

In this version we implement optimisations for reducing the number of configurations to be considered and for improving communication protocol between local reasoners.
Entailment service for the IDDL semantics is introduced.


Building:
---------

$ ant compileall

Starting:
---------

A Main.java class is given at the root of the project. It can be tested in different ways.

$ javac -cp .:./lib/draon.jar:./lib/procalign.jar:./lib/ontowrap.jar:./lib/HermiT.jar:./lib/slf4j-api.jar:./lib/jena-core.jar:./lib/jena-base.jar:./lib/jena-shaded-guava.jar:./lib/jena-iri.jar Main.java

$ java -cp .:./lib/draon.jar:./lib/procalign.jar:./lib/ontowrap.jar:./lib/HermiT.jar:./lib/slf4j-api.jar:./lib/jena-core.jar:./lib/jena-base.jar:./lib/jena-shaded-guava.jar:./lib/jena-iri.jar Main

A LOGS variable can be modified at the start of the program to deactivate the logs..

Tutorial and Technical details
------------------------------
From the version 1.5, we refer users to iddl.gforge.inria.fr for all details about DRAOn.

