/*
 * $Id:  $
 *
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.paris8.iut.info.draon;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.net.UnknownHostException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;
import java.lang.Thread;
import java.lang.Exception;
import java.lang.ClassNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.ArrayList;
import java.net.ServerSocket;
import java.net.Socket;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.owllink.OWLlinkHTTPXMLReasonerFactory;
import org.semanticweb.owlapi.owllink.OWLlinkReasoner;
import org.semanticweb.owlapi.owllink.OWLlinkReasonerConfiguration;
import org.semanticweb.owlapi.owllink.builtin.requests.CreateKB;
import org.semanticweb.owlapi.owllink.builtin.requests.ReleaseKB;
import org.semanticweb.owlapi.owllink.builtin.requests.Tell;
import org.semanticweb.owlapi.owllink.builtin.response.KB;
import org.semanticweb.owlapi.owllink.builtin.response.OK;

import fr.paris8.iut.info.draon.DRAONServices;
import fr.paris8.iut.info.draon.DRAONRuntimeException;
import fr.paris8.iut.info.draon.conf.ConfFromGlobal;
import fr.paris8.iut.info.draon.conf.GlobalEntity;
import fr.paris8.iut.info.draon.conf.Result;
import fr.paris8.iut.info.draon.conf.SystemData;

public class Connector extends Thread 
{
	private static OWLOntologyManager manager;
	public URL getUrl() 
	{
		return url;
	}
	private Set<OWLAxiom> axioms;
	private URL url = null;
	private Map<String, HashSet<Result>> results;
	private DRAONServices globalReasoner = null;
	private String uri = null;
	private int port;

	private ArrayList<GlobalEntity> arrayMap = new ArrayList<GlobalEntity>();
	// uri : uri of local ontology, cRepresentatives : all concepts,
	// cRepresentatives : all roles,
	public Connector(String urlReasoner, int p, String u, Set<String> names, DRAONServices global) 
	{
		super(urlReasoner);
		uri = u;
		port = p;
		// results = remoteResults;
		globalReasoner = global;
		start();
	}

	public void run() {
		boolean stop = false;
		// from local reasoner
		Socket requestSocket = null;
		ObjectOutputStream out = null;
		ObjectInputStream in = null;
		try {
			// 1. creating a socket to connect to the server
			requestSocket = new Socket("localhost", port);
			System.out.println("Connected to localhost in port :" + port);
			// 2. get Input and Output streams
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			// 3: Communicating with the server
			for (int i = 0; i < globalReasoner.globalEnts.length; i++) 
			{
				arrayMap.add(globalReasoner.getSystemData().getGlobalEntityMap().get(globalReasoner.globalEnts[i]));
			}
			//arrayMap is sent once to globalReasoner. This corresponds to axioms
			out.writeObject(arrayMap);
			//stop is set to "true" if globalReasoner.terminated == 1
			while (!stop) 
			{
				try {
					Result res = null;
					//This loop gets Result through "globalReasoner.confsFromGlobal"
					do {
						synchronized (globalReasoner.confsFromGlobal) {
							res = globalReasoner.confsFromGlobal.get(uri).get();
						}
						synchronized (globalReasoner.terminated) 
						{
							if (globalReasoner.terminated.intValue() == 1) 
							{
								stop = true;
							}
						}
					} while (res == null && !stop);
					//Notify to "globalReasoner"
					if (stop) {
						out.writeObject(new Integer(1));
						System.err.println("Connector terminated=" + uri);
						break;
					}

					Result tmpRes = globalReasoner.checkLocalResult(uri, res.getConf(), res.getNewPosition());
					//Until now, "res" is computed
					if (tmpRes != null)
						res = tmpRes;
					else {
						// telling global reasoner is not terminated yet
						out.writeObject(new Integer(0));
						// sending a conf to local reasoner
						out.writeObject(res);
						// getting result from the local reasoner
						res = (Result) in.readObject();
					}
					//Add the computed to "res" to  globalReasoner.remoteResults
					synchronized (globalReasoner.remoteResults) {
						globalReasoner.remoteResults.get(uri).add(res);

					}

					synchronized (globalReasoner.terminated) {
						if (globalReasoner.terminated.intValue() == 1) {
							stop = true;
						}
					}

					if (stop) {
						out.writeObject(new Integer(1));
						System.err.println("Connector terminated=" + uri);
						break;
					}

				} catch (ClassNotFoundException classNot) {
					System.err.println("data received in unknown format");
				}
			}// while

		} catch (UnknownHostException unknownHost) {
			System.err.println("You are trying to connect to an unknown host!");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} catch (Exception ex) {
			// 4: Closing connection
			ex.printStackTrace();
			try {
				in.close();
				out.close();
				requestSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
			System.out.println("Connector : no socket .");
		}

	}

	/*
	 * public boolean isConsistent(String ontoUri, Set<OWLAxiom> axioms) {
	 * boolean response = false; OWLOntology onto = null; try { //It was
	 * "createOntology" onto = manager.loadOntology(IRI.create(ontoUri)); }
	 * catch (OWLOntologyCreationException ex) { ex.printStackTrace(); throw new
	 * DRAONRuntimeException("DRAON: cannot create ontology"); } reasoner =
	 * rFactory.createReasoner(onto,reasonerConfiguration); //Create a new
	 * (empty) knowledge base ... //CreateKB createKBRequest = new CreateKB();
	 * //KB kbResponse = reasoner.answer(createKBRequest); //... and transfer a
	 * set of axioms (e.g., from another ontology, an temporary ontology etc.)
	 * to it Tell tellRequest = new Tell(IRI.create(ontoUri), axioms); OK
	 * okResponse = reasoner.answer(tellRequest);
	 * 
	 * response = reasoner.isConsistent();
	 * 
	 * //Don't forget to release the knowledge base before terminating the
	 * application ReleaseKB releaseKBRequest = new
	 * ReleaseKB(IRI.create(ontoUri)); okResponse =
	 * reasoner.answer(releaseKBRequest);
	 * 
	 * return response; }
	 * 
	 * 
	 * public boolean isConsistent(OWLOntology ontology) { reasoner
	 * =rFactory.createReasoner(ontology);
	 * 
	 * if (reasoner.isConsistent() ) { return true; } else return false; }
	 * 
	 * 
	 * 
	 * public void Request() throws MalformedURLException,
	 * OWLOntologyCreationException { OWLOntology onto =
	 * manager.createOntology(IRI
	 * .create("http://alignapi.gforge.inria.fr/tutorial/myOnto.owl")); reasoner
	 * = rFactory.createReasoner(onto,reasonerConfiguration); //Create a new
	 * (empty) knowledge base ... CreateKB createKBRequest = new CreateKB(); KB
	 * kbResponse = reasoner.answer(createKBRequest); //... and transfer a set
	 * of axioms (e.g., from another ontology, an temporary ontology etc.) to it
	 * Tell tellRequest = new Tell(kbResponse.getKB(),axioms); OK okResponse =
	 * reasoner.answer(tellRequest);
	 * 
	 * //Don't forget to release the knowledge base before terminating the
	 * application ReleaseKB releaseKBRequest = new
	 * ReleaseKB(kbResponse.getKB()); okResponse =
	 * reasoner.answer(releaseKBRequest); }
	 * 
	 * 
	 * public String GetName() { try { OWLOntology onto =
	 * manager.createOntology(IRI.create("http://127.0.0.1:8082")); reasoner =
	 * rFactory.createReasoner(onto,reasonerConfiguration);
	 * 
	 * } catch(Exception e) {
	 * 
	 * } return reasoner.getReasonerName(); }
	 */
}
