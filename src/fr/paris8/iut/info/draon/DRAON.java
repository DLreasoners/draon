/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.paris8.iut.info.draon;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.reasoner.AxiomNotInProfileException;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import org.semanticweb.owlapi.reasoner.ClassExpressionNotInProfileException;
import org.semanticweb.owlapi.reasoner.FreshEntitiesException;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.ReasonerInterruptedException;
import org.semanticweb.owlapi.reasoner.TimeOutException;
import org.semanticweb.owlapi.reasoner.UnsupportedEntailmentTypeException;
import org.semanticweb.owlapi.util.Version;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.HasInstanceRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.align.impl.rel.InstanceOfRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.paris8.iut.info.draon.conf.Semantics;
import fr.paris8.iut.info.draon.localnode.LocalNode;

/*
 (1) A network N is defined as a set of ontologies Oi with alignments Aij.
 Therefore, for each (i,j) with i<>j, only one Aij is allowed in N. 

 (2) The current DRAON does not support revision. Therefore, data (ontologies and alignments) must be determined when defining a DRAONReasoner. 

 (3) In the same way, the semantics (DL, IDDL, (DDL)) must be given when defining a DRAONReasoner ( it takes the DL semantics by default)
 */

public class DRAON implements OWLReasoner 
{
	// By default, the semantics of system is DL
	private Semantics sem = Semantics.DL;
	private int distributed = 0;
	private Set<URI> ontos = new HashSet<URI>();
	private Set<URI> aligns = new HashSet<URI>();

	private Hashtable<URI, URI> distributedOnto = new Hashtable<URI, URI>();

	public LocalReasoner localReasoner = null;
	public DRAONServices globalReasoner = null;

	public static String uriRoot = null;

	public static OWLOntologyManager manager = null;
	public static OWLDataFactory owlfactory = null;
	public static OntologyFactory ontoFact = null;

	public DRAONServices getDRAONReasoner() {
		return globalReasoner;
	}

	public LocalReasoner getLocalReasoner() {
		return localReasoner;
	}

	// public void addOntology(URI onto) { ontos.add( onto ); }
	//
	//
	// public void addAlignment(URI align){ globalReasoner.loadDRAONSystem(
	// aligns ); aligns.add( align ); }
	//
	// public void addAlignment(Alignment align){ globalReasoner.addAlignment(
	// align ); }
	//
	// //for distributed reasoning public void addOntology(URI onto, URI
	// service){ if(service != null) distributedOnto.put( onto, service );
	// ontos.add( onto ); }
	//
	// public DRAONReasoner(Semantics s) { manager =
	// OWLManager.createOWLOntologyManager( ); owlfactory =
	// manager.getOWLDataFactory();
	//
	// if(s == Semantics.DDL) sem = Semantics.DDL; else if( s == Semantics.IDDL
	// ) sem = Semantics.IDDL; else sem = Semantics.DL; try { File rootOnto =
	// File.createTempFile( "uriRoot", "" ); rootOnto.deleteOnExit(); uriRoot =
	// "file:" + rootOnto.getAbsolutePath() + File.separator;
	// //System.out.println("uriroot 2="+ uriRoot); } catch (IOException ex){ }
	//
	// localReasoner = new LocalReasoner(); globalReasoner = new
	// DRAONReasonerImpl( localReasoner ); }

	// This interface is used for DRAONReasonerFactory that does not need
	// alignment
	// It needs DRAONReasoner.loadOntology(...)
	// This constructor is used for ditributed local reasoners
	public DRAON(OWLOntology onto) {
		sem = Semantics.DL1;
		localReasoner = new LocalReasoner(onto);
	}

	// This interface loads alignments and build a list of ontology URIs
	public DRAON(Set<URI> als) throws DRAONException {
		init(Semantics.DL);
		try {
			globalReasoner.loadDRAONSystem(als, sem);
		} catch (DRAONException ex) {
			throw new DRAONException("DRAON: invalid data.", ex);
		}
	}

	public DRAON(Set<URI> als, Semantics s) throws DRAONException {
		init(s);
		try {
			globalReasoner.loadDRAONSystem(als, s);
		} catch (DRAONException ex) {
			ex.printStackTrace();
			throw new DRAONException("DRAON: invalid data.", ex);
		}
	}

	// distributed reasoning
	public DRAON(Set<URI> als, Vector<LocalNode> nodes, Semantics s)
			throws DRAONException {
		if (s != Semantics.IDDL) {
			throw new DRAONException(
					"DRAON: unsupported semantics for distributed reasoning.");
		}

		if (nodes == null || nodes.size() == 0) {
			throw new DRAONException("DRAON:  no node is defined.");
		}

		distributed = 1;
		init(s);
		try {
			globalReasoner.loadDRAONSystem(als, nodes, s);

		} catch (DRAONException ex) {
			throw new DRAONException("DRAON: invalid data.", ex);
		}
		if (globalReasoner.getSystemData().getLocalUris().size() != nodes.size()) {
			throw new DRAONException(
					"DRAON: the number of nodes is different from the number of ontologies.");
		}
	}

	// This method does not provide "aligns"
	public DRAON(List<Alignment> als) throws DRAONException {
		init(Semantics.DL);
		try {
			globalReasoner.loadDRAONSystem(als, sem);
		} catch (DRAONException ex) {
			throw new DRAONException("DRAON: invalid data.", ex);
		}
	}

	public DRAON(List<Alignment> als, Semantics s)
			throws DRAONException {
		init(s);
		try {
			globalReasoner.loadDRAONSystem(als, s);
		} catch (DRAONException ex) {
			throw new DRAONException("DRAON: invalid data.", ex);
		}
	}

	public void init(Semantics s) throws DRAONException {
		sem = s;
		manager = OWLManager.createOWLOntologyManager();
		owlfactory = manager.getOWLDataFactory();
		ontoFact = OntologyFactory.getFactory();
		try {
			File rootOnto = File.createTempFile("uriRoot", "");
			rootOnto.deleteOnExit();
			uriRoot = "file:" + rootOnto.getAbsolutePath() + File.separator;

		} catch (IOException ex) {
			throw new DRAONException("Cannot creat a temporary file.", ex);
		}
		localReasoner = new LocalReasoner();
		globalReasoner = new DRAONServices(localReasoner);
	}

	// The semantics must be determined when defining a reasoner since data
	// loading depends on the semantics

	public void setSemantics(Semantics s) throws DRAONRuntimeException {
		if (!(s == Semantics.DL || s == Semantics.IDDL || s == Semantics.APPROX ))
			// sem = s;
			// else {
			throw new DRAONRuntimeException("DRAON: unsupported semantics.");
		// }
	}

	public Semantics getSemantics() {
		return sem;
	}

	public boolean isConsistent() {
		boolean result = false;
		switch (sem) {
		case DL1: // this semantics is not visible to users
			result = localReasoner.isConsistent();
			return result;
		case DL:
			//result = localReasoner.isConsistent(globalReasoner.getSystemData().getOntoLocations(), 
			//		                            globalReasoner.getSystemData().getOWLAlignOnto());
			//result = localReasoner.isConsistent(globalReasoner.getSystemData().getOntoLocations(), 
            //         globalReasoner.getSystemData().getOWLAlignOnto());
			OWLOntology onto =  mergeOntosAlign( globalReasoner.getSystemData().getNetwork() );
			result = localReasoner.isConsistent(globalReasoner.getSystemData().getOntoLocations(), onto);

			return result;
		case IDDL:
			if (distributed == 1) {
				Vector<LocalNode> nodes = new Vector<LocalNode>();
				for (String key : globalReasoner.getDistributedNodes().keySet())
					nodes.add(globalReasoner.getDistributedNodes().get(key));
				result = globalReasoner.isConsistent(nodes);
			} else {
				result = globalReasoner.isConsistent();
			}
			return result;
		case APPROX :
			//
			return result;
		default:
			throw new DRAONRuntimeException("DRAON: unsupported semantics.");
		}
	}

	public boolean isEntailed(OWLAxiom cons) {
		boolean result = false;

		switch (sem) {
		case DL1:
			result = localReasoner.isEntailed(cons);
			return result;
		case DL:
			result = localReasoner.isEntailed(globalReasoner.getSystemData()
					.getOntoLocations(), globalReasoner.getSystemData()
					.getOWLAlignOnto(), cons);
			return result;
		case IDDL:
			if (cons instanceof OWLClassAssertionAxiom || cons instanceof OWLSubClassOfAxiom) {
				if (distributed == 1)
					result = globalReasoner.isEntailed(cons,
							(Vector<LocalNode>) globalReasoner
									.getDistributedNodes().values());
				else
					result = globalReasoner.isEntailed(cons);
				return result;
			} else
				throw new DRAONRuntimeException("DRAON: unsupported axiom :" + cons);
		case APPROX :
			//From PropagateMain
		default:
			throw new DRAONRuntimeException("DRAON: Unsupported semantics.");
		}
	}

	public boolean isEntailed(Alignment align) throws DRAONException {
		boolean result = false;

		switch (sem) {
		case DL:
			result = localReasoner.isEntailed(globalReasoner.getSystemData()
					.getOntoLocations(), globalReasoner.getSystemData()
					.getOWLAlignOnto(), globalReasoner.getSystemData()
					.OWLAlign2OWLOnto(align));
			return result;

		case IDDL:
			for (Cell cell : align) {
				try {
					result = isEntailed(correspToAxiom(align, cell,
							Semantics.IDDL));
					if (result == false)
						return false;
				} catch (DRAONException ex) {
					throw new DRAONRuntimeException(
							"DRAON: unsupported axiomm : " + cell, ex);
				}
			}
			return true;

		default:
			throw new DRAONException("Unsupported semantics.");
		}

	}

	public boolean isEntailed(Alignment align, Cell cell) throws DRAONException {
		return isEntailed(correspToAxiom(align, cell, sem));
	}
	
	/*
	 * Merge ontologies and alignments
	 */
	public OWLOntology mergeOntosAlign(List<Alignment> als) {
		OWLOntology mergedOnto = null;
		Set<String> usedOntologies = new HashSet<String>();
		try {
			mergedOnto = manager.createOntology();
			for (Alignment a : als) {
				BasicAlignment ba = (BasicAlignment) a;
				// deal with ontology 1
				String ontoFile1 = new File(ba.getOntologyObject1().getFile().toString()).getAbsolutePath();
				if (!usedOntologies.contains(ontoFile1)) {
					usedOntologies.add(ontoFile1);
					OWLOntology onto1 = manager.loadOntologyFromOntologyDocument(new File(ontoFile1));
					manager.addAxioms(mergedOnto, onto1.getAxioms());
				}
				// deal with ontology 2
				String ontoFile2 = new File(ba.getOntologyObject2().getFile().toString()).getAbsolutePath();
				if (!usedOntologies.contains(ontoFile2)) {
					usedOntologies.add(ontoFile2);
					OWLOntology onto2 = manager.loadOntologyFromOntologyDocument(new File(ontoFile2));
					manager.addAxioms(mergedOnto, onto2.getAxioms());
				}
			}
			// deal with aligment axioms
			manager.addAxioms(mergedOnto, globalReasoner.getSystemData().getOWLAlignOnto().getAxioms());
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		return mergedOnto;
	}

	// JE : In fact, it should be possible to do all EDOAL
	public OWLAxiom correspToAxiom(Alignment align, Cell corresp, Semantics sem) throws DRAONException 
	{
		// LoadedOntology onto1 = al.ontology1();
		// LoadedOntology onto2 = al.ontology2();
		//Object e1 = corresp.getObject1();
		//Object e2 = corresp.getObject2();
		Relation r = corresp.getRelation();
		
		BasicAlignment al = (BasicAlignment) align;
		// retrieve entity1 and entity2
		// create the axiom in function of their labels

		try {
			IRI ur1 = IRI.create( corresp.getObject1AsURI(al) );
			IRI ur2 = IRI.create(corresp.getObject2AsURI(al));

			String obUri1 = ur1.toString();
			String obUri2 = ur2.toString();
			String ontoFile1 = new File(al.getOntologyObject1().getFile().toString()).getAbsolutePath();
			String ontoFile2 = new File(al.getOntologyObject2().getFile().toString()).getAbsolutePath();
			OWLOntology onto1 = manager.loadOntologyFromOntologyDocument(new File(ontoFile1));
			OWLOntology onto2 = manager.loadOntologyFromOntologyDocument(new File(ontoFile2));
			
			//LoadedOntology onto1 = ontoFact.loadOntology(al.getOntologyObject1().getFile());
			//LoadedOntology onto2 = ontoFact.loadOntology(al.getOntologyObject2().getFile());

			if (onto1.containsClassInSignature(ur1)) {
				if ( onto2.containsClassInSignature(ur2) ) {
					OWLClass entity1 = owlfactory.getOWLClass(ur1);
					OWLClass entity2 = owlfactory.getOWLClass(ur2);
					if (r instanceof EquivRelation) {
						return owlfactory.getOWLEquivalentClassesAxiom(entity1,
								entity2);
					} else if (r instanceof SubsumeRelation) {
						return owlfactory.getOWLSubClassOfAxiom(entity2,
								entity1);
					} else if (r instanceof SubsumedRelation) {
						return owlfactory.getOWLSubClassOfAxiom(entity1,
								entity2);
					} else if (r instanceof IncompatRelation) {
						if (sem == Semantics.IDDL)
							throw new DRAONException(
									"DRAON : OWLDisjointClassesAxiom is not supported : "
											+ corresp);
						return owlfactory.getOWLDisjointClassesAxiom(entity1,
								entity2);
					}
				} else if (onto2.containsIndividualInSignature(ur2)&& (r instanceof HasInstanceRelation)) {
					return owlfactory.getOWLClassAssertionAxiom(owlfactory.getOWLClass(ur1),
							owlfactory.getOWLNamedIndividual(ur2) );
				}
			} else if (onto1.containsDataPropertyInSignature(ur1) && onto2.containsDataPropertyInSignature(ur2)) {
				OWLDataProperty entity1 = owlfactory.getOWLDataProperty(ur1);
				OWLDataProperty entity2 = owlfactory.getOWLDataProperty(ur2);
				if (r instanceof EquivRelation) {
					return owlfactory.getOWLEquivalentDataPropertiesAxiom(
							entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					return owlfactory.getOWLSubDataPropertyOfAxiom(entity2,
							entity1);
				} else if (r instanceof SubsumedRelation) {
					return owlfactory.getOWLSubDataPropertyOfAxiom(entity1,
							entity2);
				} else if (r instanceof IncompatRelation) {
					if (sem == Semantics.IDDL)
						throw new DRAONException(
								"DRAON : OWLDisjointDataPropertiesAxiom is not supported : "
										+ corresp);
					return owlfactory.getOWLDisjointDataPropertiesAxiom(
							entity1, entity2);
				}
			} else if (onto1.containsDataPropertyInSignature(ur1) && onto2.containsDataPropertyInSignature(ur2)) {
				OWLObjectProperty entity1 = owlfactory.getOWLObjectProperty(ur1);
				OWLObjectProperty entity2 = owlfactory.getOWLObjectProperty(ur2);
				if (r instanceof EquivRelation) {
					return owlfactory.getOWLEquivalentObjectPropertiesAxiom(
							entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					return owlfactory.getOWLSubObjectPropertyOfAxiom(entity2,
							entity1);
				} else if (r instanceof SubsumedRelation) {
					return owlfactory.getOWLSubObjectPropertyOfAxiom(entity1,
							entity2);
				} else if (r instanceof IncompatRelation) {
					if (sem == Semantics.IDDL)
						throw new DRAONException(
								"DRAON : OWLDisjointObjectPropertiesAxiom is not supported : "
										+ corresp);
					return owlfactory.getOWLDisjointObjectPropertiesAxiom(
							entity1, entity2);
				}
			} else if (onto1.containsIndividualInSignature(ur1)) {
				if (onto2.containsIndividualInSignature(ur2)) {
					OWLIndividual entity1 = owlfactory
							.getOWLNamedIndividual(ur1);
					OWLIndividual entity2 = owlfactory
							.getOWLNamedIndividual(ur2);
					if (r instanceof EquivRelation) {
						return owlfactory.getOWLSameIndividualAxiom(entity1,
								entity2);
					} else if (r instanceof IncompatRelation) {
						return owlfactory.getOWLDifferentIndividualsAxiom(
								entity1, entity2);
					}
				} else if (onto2.containsClassInSignature(ur1)
						&& (r instanceof InstanceOfRelation)) {
					return owlfactory.getOWLClassAssertionAxiom(owlfactory.getOWLClass(ur2),
							owlfactory.getOWLNamedIndividual(ur1));
				}
			}
		} catch (AlignmentException ex) {
			throw new DRAONException("Cannot convert correspondence.");
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		} 
		throw new DRAONException("Cannot convert correspondence " + corresp);
	}

	public void dispose() {
		// TODO Auto-generated method stub
	}

	public long getTimeOut() {
		// TODO Auto-generated method stub
		return (long) 0.;
	}

	public org.semanticweb.owlapi.reasoner.IndividualNodeSetPolicy getIndividualNodeSetPolicy() {
		// TODO Auto-generated method stub
		return null;
	}

	public org.semanticweb.owlapi.reasoner.FreshEntityPolicy getFreshEntityPolicy() {
		// TODO Auto-generated method stub
		return null;
	}

	public org.semanticweb.owlapi.reasoner.Node<OWLClass> getEquivalentClasses(
			OWLClassExpression arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLClass> getInconsistentClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLClass>> getSubClasses(OWLClassExpression arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLClass>> getSuperClasses(OWLClassExpression arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isEquivalentClass(OWLClassExpression arg0,
			OWLClassExpression arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSubClassOf(OWLClassExpression arg0, OWLClassExpression arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSatisfiable(OWLClassExpression arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public Map<OWLDataProperty, Set<OWLLiteral>> getDataPropertyRelationships(
			OWLIndividual arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLIndividual> getIndividuals(OWLClassExpression arg0,
			boolean arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<OWLObjectProperty, Set<OWLIndividual>> getObjectPropertyRelationships(
			OWLIndividual arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLIndividual> getRelatedIndividuals(OWLIndividual arg0,
			OWLObjectPropertyExpression arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLLiteral> getRelatedValues(OWLIndividual arg0,
			OWLDataPropertyExpression arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLClass>> getTypes(OWLIndividual arg0, boolean arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean hasDataPropertyRelationship(OWLIndividual arg0,
			OWLDataPropertyExpression arg1, OWLLiteral arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean hasObjectPropertyRelationship(OWLIndividual arg0,
			OWLObjectPropertyExpression arg1, OWLIndividual arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean hasType(OWLIndividual arg0, OWLClassExpression arg1,
			boolean arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	public Set<Set<OWLObjectProperty>> getAncestorProperties(
			OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLDataProperty>> getAncestorProperties(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLObjectProperty>> getDescendantProperties(
			OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLDataProperty>> getDescendantProperties(
			OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLClassExpression>> getDomains(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLClassExpression>> getDomains(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLObjectProperty> getEquivalentProperties(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLDataProperty> getEquivalentProperties(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLObjectProperty>> getInverseProperties(
			OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLClassExpression> getRanges(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLDataRange> getRanges(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLObjectProperty>> getSubProperties(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLDataProperty>> getSubProperties(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLObjectProperty>> getSuperProperties(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Set<OWLDataProperty>> getSuperProperties(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isAntiSymmetric(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isFunctional(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isFunctional(OWLDataProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isInverseFunctional(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isIrreflexive(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isReflexive(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSymmetric(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isTransitive(OWLObjectProperty arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public NodeSet<OWLNamedIndividual> getDifferentIndividuals(
			OWLNamedIndividual ind) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLNamedIndividual> getSameIndividuals(OWLNamedIndividual ind)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLLiteral> getDataPropertyValues(OWLNamedIndividual ind,
			OWLDataProperty pe) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLNamedIndividual> getObjectPropertyValues(
			OWLNamedIndividual ind, OWLObjectPropertyExpression pe)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLNamedIndividual> getInstances(OWLClassExpression ce,
			boolean direct) throws InconsistentOntologyException,
			ClassExpressionNotInProfileException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;

	}

	public NodeSet<OWLClass> getTypes(OWLNamedIndividual ind, boolean direct)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;

	}

	public NodeSet<OWLClass> getDataPropertyDomains(OWLDataProperty pe,
			boolean direct) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		// TODO Auto-generated method stub
		return null;

	}

	public NodeSet<OWLDataProperty> getDisjointDataProperties(
			OWLDataPropertyExpression pe) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		// TODO Auto-generated method stub
		return null;

	}

	public Node<OWLDataProperty> getEquivalentDataProperties(OWLDataProperty pe)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;

	}

	public NodeSet<OWLDataProperty> getSuperDataProperties(OWLDataProperty pe,
			boolean direct) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		// TODO Auto-generated method stub
		return null;

	}

	public void flush() {
		// TODO Auto-generated method stub

	}

	public Node<OWLClass> getBottomClassNode() {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLDataProperty> getBottomDataPropertyNode() {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLObjectPropertyExpression> getBottomObjectPropertyNode() {
		// TODO Auto-generated method stub
		return null;
	}

	public BufferingMode getBufferingMode() {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLClass> getDisjointClasses(OWLClassExpression arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLObjectPropertyExpression> getDisjointObjectProperties(
			OWLObjectPropertyExpression arg0)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLObjectPropertyExpression> getEquivalentObjectProperties(
			OWLObjectPropertyExpression arg0)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLObjectPropertyExpression> getInverseObjectProperties(
			OWLObjectPropertyExpression arg0)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLClass> getObjectPropertyDomains(
			OWLObjectPropertyExpression arg0, boolean arg1)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLClass> getObjectPropertyRanges(
			OWLObjectPropertyExpression arg0, boolean arg1)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLAxiom> getPendingAxiomAdditions() {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<OWLAxiom> getPendingAxiomRemovals() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<OWLOntologyChange> getPendingChanges() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getReasonerName() {
		// TODO Auto-generated method stub
		return null;
	}

	public Version getReasonerVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	public OWLOntology getRootOntology() {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLClass> getSubClasses(OWLClassExpression arg0, boolean arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLDataProperty> getSubDataProperties(OWLDataProperty arg0,
			boolean arg1) throws InconsistentOntologyException,
			FreshEntitiesException, ReasonerInterruptedException,
			TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLObjectPropertyExpression> getSubObjectProperties(
			OWLObjectPropertyExpression arg0, boolean arg1)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLClass> getSuperClasses(OWLClassExpression arg0,
			boolean arg1) throws InconsistentOntologyException,
			ClassExpressionNotInProfileException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public NodeSet<OWLObjectPropertyExpression> getSuperObjectProperties(
			OWLObjectPropertyExpression arg0, boolean arg1)
			throws InconsistentOntologyException, FreshEntitiesException,
			ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLClass> getTopClassNode() {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLDataProperty> getTopDataPropertyNode() {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLObjectPropertyExpression> getTopObjectPropertyNode() {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<OWLClass> getUnsatisfiableClasses()
			throws ReasonerInterruptedException, TimeOutException {
		// TODO Auto-generated method stub
		return null;
	}

	public void interrupt() {
		// TODO Auto-generated method stub

	}

	public boolean isEntailed(Set<? extends OWLAxiom> arg0)
			throws ReasonerInterruptedException,
			UnsupportedEntailmentTypeException, TimeOutException,
			AxiomNotInProfileException, FreshEntitiesException {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isEntailmentCheckingSupported(AxiomType<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public void prepareReasoner() throws ReasonerInterruptedException,
			TimeOutException {
		// TODO Auto-generated method stub

	}

	public Set<InferenceType> getPrecomputableInferenceTypes() {
		return null;
	}

	public boolean isPrecomputed(InferenceType inferenceType) {
		return false;
	}

	public void precomputeInferences(InferenceType... inferenceTypes)
			throws ReasonerInterruptedException, TimeOutException,
			InconsistentOntologyException {
	}

}
