/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.paris8.iut.info.draon;

import fr.paris8.iut.info.draon.DRAON;

import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.IllegalConfigurationException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

public class DRAONFactory implements OWLReasonerFactory {

	public String getReasonerName() {
		return "IDDL reasoner";
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology owlo) {
		DRAON reasoner = new DRAON(owlo);
		// reasoner.localReasoner.loadOntology(owlo);
		return reasoner;
	}

	public OWLReasoner createReasoner(OWLOntology owlo) {
		DRAON reasoner = new DRAON(owlo);
		// reasoner.localReasoner.loadOntology(owlo);
		return reasoner;
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology owlo,
			OWLReasonerConfiguration owlrc)
			throws IllegalConfigurationException {
		DRAON reasoner = new DRAON(owlo);
		// reasoner.localReasoner.loadOntology(owlo);
		return reasoner;
	}

	public OWLReasoner createReasoner(OWLOntology owlo,
			OWLReasonerConfiguration owlrc)
			throws IllegalConfigurationException {
		return new DRAON(owlo);
	}

}
