/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package fr.paris8.iut.info.draon;

import fr.inrialpes.exmo.align.parser.AlignmentParser;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.URIAlignment;

import org.semanticweb.owl.align.AlignmentException;

import fr.paris8.iut.info.draon.Connector;
import fr.paris8.iut.info.draon.DRAONException;
import fr.paris8.iut.info.draon.conf.ConfFromGlobal;
import fr.paris8.iut.info.draon.conf.Duo;
import fr.paris8.iut.info.draon.conf.GlobalEntity;
import fr.paris8.iut.info.draon.conf.LocalEntityMap;
import fr.paris8.iut.info.draon.conf.Result;
import fr.paris8.iut.info.draon.conf.Semantics;
import fr.paris8.iut.info.draon.conf.StateOfConf;
import fr.paris8.iut.info.draon.conf.SystemData;
import fr.paris8.iut.info.draon.localnode.LocalNode;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.Collection;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Set;
import java.util.NoSuchElementException;
import java.util.Enumeration;

import javax.swing.JTextArea;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLPropertyAxiom;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.util.OWLOntologyChangeVisitorAdapter;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import fr.paris8.iut.info.draon.AlignServer;
import fr.paris8.iut.info.draon.DRAONExplanation;
import fr.paris8.iut.info.draon.DRAON;
import fr.paris8.iut.info.draon.DRAONServices;
import fr.paris8.iut.info.draon.DRAONRuntimeException;
import fr.paris8.iut.info.draon.LocalReasoner;

public class DRAONServices 
{
	private int distDebug = 0;
	private int nondistDebug = 0;
	private int checkLocalConf = 0;
	private int checkInit = 0;

	private Set<URI> alignUris = new HashSet<URI>();

	private SystemData systemData = null;
	private LocalReasoner reasoner = null;

	private DRAONExplanation explanation = null;

	private HashMap<String, Set<String>> conceptProjections = null;
	private HashMap<String, Set<String>> objectRoleProjections = null;
	private HashMap<String, Set<String>> dataRoleProjections = null;
	private HashMap<String, Set<String>> roleProjections = null;
	private HashMap<String, Set<String>> individualProjections = null;

	private LocalReasoner localReasoner = null;

	private static int globalEntityId = 0;
	private static int globalOntoName = 0;
	private static int localEntityId = 0;
	private static OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	private static OWLDataFactory facto = manager.getOWLDataFactory();
	public final static String uriRoot = "file:/liasd.univ-paris8.fr/draon/";

	Hashtable<String, Alignment> alignTable = null;

	Map<String, LocalNode> distributedNodes = null;

	// this array is used to keep the order of elements for sending to local
	// reasoner
	public String[] globalEnts = null;

	// each reasoner "uri" has a Map of (conf, state) where "Map(conf) = null"
	// not valid; "Map(conf) = 1" 1: YES; "Map(conf) = 2":NO
	public Map<String, HashSet<Result>> remoteResults = null;
	public Map<String, ConfFromGlobal> confsFromGlobal = null;

	// shared with Connector
	public Integer terminated = new Integer(0);

	public DRAONServices(LocalReasoner reasoner) 
	{
		localReasoner = reasoner;
		manager = OWLManager.createOWLOntologyManager();
		facto = manager.getOWLDataFactory();
		alignTable = new Hashtable<String, Alignment>();

		distributedNodes = new HashMap<String, LocalNode>();
		remoteResults = new HashMap<String, HashSet<Result>>();
		confsFromGlobal = new HashMap<String, ConfFromGlobal>();
	}

	/*
	 * public void addAlignment(Alignment align){ alignTable.put(
	 * Integer.toString(getGlobalEntityId()) , align ); }
	 */

	public void unloadDRAONSystem() 
	{

		globalEntityId = 0;
		globalOntoName = 0;
		localEntityId = 0;
	}

	public void unloadMDRAONSystem(Vector<URI> uris) 
	{
		globalEntityId = 0;
		globalOntoName = 0;
		localEntityId = 0;
	}

	public static int getGlobalEntityId() {

		return globalEntityId++;
	}

	public static int getLocalEntityId() {

		return localEntityId++;
	}

	public static int getGlobalOntoName() {

		return globalOntoName++;
	}

	public String getExplanation() {
		return explanation.getExplanation();
	}

	public SystemData getSystemData() {
		return systemData;
	}

	public Map<String, LocalNode> getDistributedNodes() {
		return distributedNodes;
	}

	public Map<String, HashSet<Result>> getRemoteResults() {
		return remoteResults;

	}

	public Integer getTerminated() {
		return terminated;

	}

	public void setTerminated(Integer s) {
		synchronized (terminated) {
			terminated = s;
		}
	}

	// public void setStateOfRemoteReasoner(String uri, Set<String> conf, int
	// value) {
	// statesOfRemoteReasoners.put(uri, value);
	// }

	// public int getStateOfRemoteReasoner(String uri) {
	// return statesOfRemoteReasoners.get(uri);
	// }

	// when building concept conf, we can omit individuals since they are
	// non-empty (obvious?)
	// "uris" are uris of "Alignment"
	public void loadDRAONSystem(Set<URI> uris, Semantics sem)
			throws DRAONException 
	{
		if (uris == null || uris.size() == 0)
			throw new DRAONException("DRAON: empty system.");
		for (URI uri : uris) {
			if (!alignUris.contains(uri))
				alignUris.add(uri);
		}
		// explanation = new DRAONExplanation();

		setSystemData(sem);
		try {
			if( !(sem==Semantics.DL) )// 14/06 : to avoid inconsistence from construction of conf
			    initDRAONSystem();
		} catch (InconsistentOntologyException ioe) {
			throw new DRAONException("Inconsistent ontology");
		}
		// Rethink the uniqueness of "GlobalOntoUri"
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());
		for (String uri : systemData.getLocalUris()) {
			remoteResults.put(uri, new HashSet<Result>());
			confsFromGlobal.put(uri, new ConfFromGlobal());
		}
		// Rethink the uniqueness of "GlobalOntoUri"
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());
	}

	public void loadDRAONSystem(Set<URI> uris, Vector<LocalNode> nodes,
			Semantics sem) throws DRAONException {
		if (uris == null || uris.size() == 0)
			throw new DRAONException("DRAON: empty system.");
		for (URI uri : uris) {
			if (!alignUris.contains(uri))
				alignUris.add(uri);
		}

		setSystemData(sem);
		try {
			initDRAONSystem();
		} catch (InconsistentOntologyException ioe) {
			throw new DRAONException("Inconsistent ontology");
		}

		for (LocalNode node : nodes) {
			//System.out.println( "node =" + node.getUri() );
			distributedNodes.put(node.getUri(), node);

			remoteResults.put(node.getUri(), new HashSet<Result>());
			confsFromGlobal.put(node.getUri(), new ConfFromGlobal());
		}
		// Rethink the uniqueness of "GlobalOntoUri"
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());
	}

	// Alignment
	public void loadDRAONSystem(List<Alignment> aligns, Semantics sem)
			throws DRAONException {
		if (aligns == null || aligns.size() == 0)
			throw new DRAONException("DRAON: empty system.");

		setSystemData(aligns, sem);
		try {
			initDRAONSystem();
		} catch (InconsistentOntologyException ioe) {
			throw new DRAONException("Inconsistent ontology");
		}

		// Rethink the uniqueness of "GlobalOntoUri"
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());
		for (String uri : systemData.getLocalUris()) {
			remoteResults.put(uri, new HashSet<Result>());
			confsFromGlobal.put(uri, new ConfFromGlobal());
		}
		// Rethink the uniqueness of "GlobalOntoUri"
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());
	}

	public void initDRAONSystem() throws InconsistentOntologyException {
		reasoner = new LocalReasoner();
		//changeVisitor = new OWLOntologyChangeVisitorAdapter();
		conceptProjections = new HashMap<String, Set<String>>();
		objectRoleProjections = new HashMap<String, Set<String>>();
		dataRoleProjections = new HashMap<String, Set<String>>();
		roleProjections = new HashMap<String, Set<String>>();
		individualProjections = new HashMap<String, Set<String>>();

		// this function builds "globalEntityMap", and XXXProjections
		buildLocalConceptProj(systemData.getLocalMap().getLocalUris());
		// fill "roleProjections"
		buildLocalObjectRoleProj(systemData.getLocalMap().getLocalUris());
		buildLocalDataRoleProj(systemData.getLocalMap().getLocalUris());
		buildLocalRoleProj(systemData.getLocalMap().getLocalUris());
		buildLocalIndProj(systemData.getLocalMap().getLocalUris());
		 
		// the equivalents concepts and roles are eliminated
		Set<String> representatives = buildEquivCls(
				systemData.getOWLAlignOnto(), (Set<String>) systemData
						.getGlobalEntityMap().keySet());
		HashMap<String, GlobalEntity> reducedMap = new HashMap<String, GlobalEntity>();
		for (String key : representatives) {
			reducedMap.put(key, systemData.getGlobalEntityMap().get(key));
		}
		systemData.setGlobalEntityMap(reducedMap);
	}

	// fill "systemData"
	public void setSystemData(List<Alignment> aligns, Semantics s)
			throws DRAONException {
		for (int i = 0; i < aligns.size(); i++) {
			alignTable.put(Integer.toString(DRAONServices.getGlobalEntityId()),
					(Alignment) aligns.get(i));
		}
		// building data of the network
		systemData = new SystemData(alignTable, s);
	}

	// fill "systemData" with a set of alignments
	public void setSystemData(Semantics s) throws DRAONException {
		String defaultHost = "aserv.inrialpes.fr";
		String defaultPort = "80";
		AlignServer onlineAlign = new AlignServer(defaultPort, defaultHost);
		try {
			AlignmentParser ap = new AlignmentParser();
			//ap.setEmbedded(true);

			for (int i = 0; i < alignUris.size(); i++) 
			{
				Alignment align = null;
				//ObjectAlignment al = null;
				if (alignUris.toArray()[i].toString().startsWith("http:/")) {
					String rdfStr = onlineAlign.getRDFAlignment(alignUris.toArray()[i].toString());
					align = (Alignment) ap.parseString(rdfStr);
					//al = ObjectAlignment	.toObjectAlignment((URIAlignment) align);
				} else {
					//System.out.println("align="+ alignUris.toArray()[i].toString());
					align = (Alignment) ap.parse(alignUris.toArray()[i].toString());
					//al = ObjectAlignment
					//		.toObjectAlignment((URIAlignment) align);
				}
				// alignTable.put( alignUris.toArray()[i].toString(), align );
				alignTable.put(Integer.toString(DRAONServices.getGlobalEntityId()),align);
			}
		} catch (AlignmentException ex) {
			// ex.printStackTrace();
			throw new DRAONException("DRAON : cannot build system data.", ex);
		}
		// building data of the network
		systemData = new SystemData(alignTable, s);
	}

	// "conf" contains indexes of entities in systemData.getGlobalEntityMap()
	public boolean isLocalConsistent(String uri, int size, Set<Integer> conf) {
		Set<String> cConf = new HashSet<String>();
		Set<String> rConf = new HashSet<String>();
		int nbOfEntities = systemData.getGlobalEntityMap().size();
		Set<String> keys = systemData.getGlobalEntityMap().keySet();
		Set<String> locals = new HashSet<String>();
		for (String c : conceptProjections.get(uri)) {
			if (!locals.contains(c))
				locals.add(c);
		}

		for (String c : roleProjections.get(uri)) {
			if (!locals.contains(c))
				locals.add(c);
		}

		// locals.addAll( conceptProjections.get(uri) );
		// locals.addAll( roleProjections.get(uri) );

		for (int i = 0; i < nbOfEntities; i++) {
			String en = ((GlobalEntity) systemData.getGlobalEntityMap().get(
					keys.toArray()[i])).getName();
			if (conf.contains(new Integer(i)) && locals.contains(en)) {
				// concepts
				if (((GlobalEntity) systemData.getGlobalEntityMap().get(en))
						.getAttr() == 0) {
					cConf.add(en);
				}
				// roles
				if (((GlobalEntity) systemData.getGlobalEntityMap().get(en))
						.getAttr() == 1
						|| ((GlobalEntity) systemData.getGlobalEntityMap().get(
								en)).getAttr() == 2) {
					rConf.add(en);
				}
			}
		}
		Set<OWLAxiom> localAxioms = createLocalAxiomsFromConf(uri, cConf, rConf);
		return localReasoner.isConsistent(systemData.getLocationFromUri(uri), localAxioms);
	}

	// "conf" contains indexes of entities in systemData.getGlobalEntityMap(),
	// "size" is the size of "conf",
	// "ax" is the index of the entity, if "attr": 0 then "ax" is emptiness; if
	// "attr": 1 then "ax" is non-emptiness
	public boolean isLocalEntailed(String uri, int size, Set<Integer> conf,
			int ax, int attr) {
		Set<String> cConf = new HashSet<String>();
		Set<String> rConf = new HashSet<String>();
		int nbOfEntities = systemData.getGlobalEntityMap().size();
		Set<String> keys = systemData.getGlobalEntityMap().keySet();
		Set<String> locals = new HashSet<String>();

		for (String c : conceptProjections.get(uri)) {
			if (!locals.contains(c))
				locals.add(c);
		}
		for (String c : roleProjections.get(uri)) {
			if (!locals.contains(c))
				locals.add(c);
		}

		boolean result = false;

		String ent = ((GlobalEntity) systemData.getGlobalEntityMap().get(
				keys.toArray()[ax])).getName();

		OWLAxiom axiom = entityToAxiom(ent, systemData.getGlobalEntityMap(),
				attr);
 
		if (conf == null || conf.size() == 0)
			result = localReasoner.isEntailed(
					systemData.getLocationFromUri(uri),
					new HashSet<OWLAxiom>(), axiom);
		else {
			for (int i = 0; i < nbOfEntities; i++) 
			{
				String en = ((GlobalEntity) systemData.getGlobalEntityMap()
						.get(keys.toArray()[i])).getName();
				if (conf.contains(new Integer(i)) && locals.contains(en))
					// concepts
					if (((GlobalEntity) systemData.getGlobalEntityMap().get(en))
							.getAttr() == 0) {
						cConf.add(en);
					}
				// roles
				if (((GlobalEntity) systemData.getGlobalEntityMap().get(en))
						.getAttr() == 1
						|| ((GlobalEntity) systemData.getGlobalEntityMap().get(
								en)).getAttr() == 2) {
					rConf.add(en);
				}
			}

			Set<OWLAxiom> localAxioms = createLocalAxiomsFromConf(uri, cConf, rConf);
			result = localReasoner.isEntailed(
					systemData.getLocationFromUri(uri), localAxioms, axiom);
		}
		return result;
	}

	/* "attr=0" : empty axiom, "attr=1" : nonempty axiom
	 * 
	 */
	public static OWLAxiom entityToAxiom(String name, HashMap<String, GlobalEntity> map, int attr) 
	{
		OWLAxiom axiom = null;
		// concept axiom
		if (map.get(name).getAttr() == 0) {
			if (attr == 0) {
				OWLClass noth = facto.getOWLNothing();
				OWLClass cls = facto.getOWLClass(IRI.create(name));
				axiom = facto.getOWLSubClassOfAxiom(cls, noth);
			}
			if (attr == 1) {
				OWLIndividual ind = facto.getOWLNamedIndividual(IRI.create(name + getLocalEntityId()));//"#"+
				//System.out.println("name="+name);
				//System.out.println("ind 1="+ind);
				
				OWLClass cls = facto.getOWLClass(IRI.create(name));
				axiom = facto.getOWLClassAssertionAxiom(cls, ind);
			}
		}
		// object role axiom
		if (map.get(name).getAttr() == 1) {
			if (attr == 0) {
				OWLClass th = facto.getOWLThing();
				OWLClass noth = facto.getOWLNothing();

				OWLProperty r = facto.getOWLObjectProperty(IRI.create(name));
				OWLObjectAllValuesFrom cls = facto.getOWLObjectAllValuesFrom(
						(OWLObjectProperty) r, noth);
				axiom = facto.getOWLSubClassOfAxiom(th, cls);
			}
			if (attr == 1) {
				OWLProperty r = facto.getOWLObjectProperty(IRI.create(name));
				OWLIndividual ind1 = facto.getOWLNamedIndividual(IRI.create(name +getLocalEntityId()));//+"#"
				
				//System.out.println("ind 2="+ind1);
				
				OWLIndividual ind2 = facto.getOWLNamedIndividual(IRI.create(name + getLocalEntityId()));//"#"+
				axiom = facto.getOWLObjectPropertyAssertionAxiom(
						(OWLObjectProperty) r, ind1, ind2);
			}
		}

		// data role axiom
		if (map.get(name).getAttr() == 2) {
			if (attr == 0) {
				OWLClass th = facto.getOWLThing();
				OWLDataRange noth = facto.getOWLDataComplementOf(facto.getTopDatatype());

				OWLProperty r = facto.getOWLDataProperty(IRI.create(name));
				OWLDataAllValuesFrom cls = facto.getOWLDataAllValuesFrom(
						(OWLDataProperty) r, noth);
				axiom = facto.getOWLSubClassOfAxiom(th, cls);
			}
			if (attr == 1) {
				OWLProperty r = facto.getOWLDataProperty(IRI.create(name));
				OWLIndividual ind1 = facto.getOWLNamedIndividual(IRI.create(name + getLocalEntityId()));//"#"+
				
				//System.out.println("ind 3="+ind1);
				
				OWLLiteral ind2 = facto.getOWLLiteral(name+"#"+getLocalEntityId());
				axiom = facto.getOWLDataPropertyAssertionAxiom((OWLDataProperty)r, ind1, ind2);
			}
		}
		return axiom;
	}

	// This method can discover inconsistency of the system. If it returns
	// "false" the system is not conistent,
	// otherwise, consistency is not known and "systemData.getGlobalEntityMap()"
	// may be changed

	public boolean initialEntailments() {
		Set<String> ents = systemData.getGlobalEntityMap().keySet();
		Set<String> deleted = new HashSet<String>();
		HashMap<String, GlobalEntity> reducedMap = new HashMap<String, GlobalEntity>();
		HashMap<String, Set<Boolean>> emptyRes = new HashMap<String, Set<Boolean>>();
		HashMap<String, Set<Boolean>> nonemptyRes = new HashMap<String, Set<Boolean>>();
		boolean empty = false;
		boolean nonempty = false;

		for (int i = 0; i < ents.size(); i++) {
			String key = (String) ents.toArray()[i];

			// System.out.println("index= "+i+ ", key = "+key);
			empty = false;
			nonempty = false;
			Set<Boolean> sE = new HashSet<Boolean>();
			Set<Boolean> nE = new HashSet<Boolean>();

			OWLAxiom emptyAx = entityToAxiom(key,
					systemData.getGlobalEntityMap(), 0);
			OWLAxiom nonemptyAx = entityToAxiom(key,
					systemData.getGlobalEntityMap(), 1);

			empty = localReasoner.isEntailed(systemData.getOWLAlignOnto(),
					new HashSet<OWLAxiom>(), emptyAx);

			boolean empty1 = false;

			nonempty = localReasoner.isEntailed(systemData.getOWLAlignOnto(),
					new HashSet<OWLAxiom>(), nonemptyAx);

			boolean nonempty1 = false;

			sE.add(new Boolean(empty));
			nE.add(new Boolean(nonempty));
			if (empty && nonempty)
				return false;
			for (String uri : systemData.getLocalUris()) {
 
				empty = localReasoner.isEntailed(
						systemData.getLocationFromUri(uri),
						new HashSet<OWLAxiom>(), emptyAx);

				// empty1 = isLocalEntailed(uri, 0, new HashSet<Integer>(), i,
				// 0);

				if ((empty == true && empty1 == false)
						|| (empty == false && empty1 == true))
					System.err.println("diff empty");

				nonempty = localReasoner.isEntailed(
						systemData.getLocationFromUri(uri),
						new HashSet<OWLAxiom>(), nonemptyAx);

				// nonempty1 = isLocalEntailed(uri, 0, new HashSet<Integer>(),
				// i, 1);

				if ((nonempty == true && nonempty1 == false)
						|| (nonempty == false && nonempty1 == true))
					System.err.println("diff nonempty");

				sE.add(new Boolean(empty));
				nE.add(new Boolean(nonempty));
				if (empty && nonempty)
					return false;
			}
			emptyRes.put(key, sE);
			nonemptyRes.put(key, nE);
		}

		for (String str : (Set<String>) emptyRes.keySet()) {
			boolean deletedEmpty = true;
			boolean deletedNonempty = true;
			for (Boolean b1 : (Set<Boolean>) emptyRes.get(str)) {
				if (b1.booleanValue() == false) {
					deletedEmpty = false;
					break;
				}
			}

			for (Boolean b1 : (Set<Boolean>) nonemptyRes.get(str)) {
				if (b1.booleanValue() == false) {
					deletedNonempty = false;
					break;
				}
			}
			// the element systemData.getGlobalEntityMap()(str) is not copied if
			// deletedEmpty=true or deletedNonempty=true
			if (!deletedEmpty) {
				reducedMap.put(str, systemData.getGlobalEntityMap().get(str));
			}
			if (!deletedNonempty) {
				reducedMap.put(str, systemData.getGlobalEntityMap().get(str));
			}

		}
		systemData.setGlobalEntityMap(reducedMap);

		return true;
	}

	// if found, it returns "Result", or null otherwise
	public Result checkLocalResult(String uri, Vector<Duo> conf,
			String newPosition) {
		synchronized (remoteResults) {
			HashSet<Result> resSet = remoteResults.get(uri);
			for (Result res : resSet) {
				Vector<Duo> v = res.getConf();
				Map<String, Integer> m1 = new HashMap<String, Integer>();
				Map<String, Integer> m2 = new HashMap<String, Integer>();
				// System.out.println("Checked00=");
				if (v.size() > 0 && conf.size() > 0) {
					for (int i = 0; i < v.size(); i++)
						m1.put(v.get(i).getKey(), new Integer(v.get(i)
								.getState()));
					for (int i = 0; i < conf.size(); i++)
						m2.put(conf.get(i).getKey(), new Integer(conf.get(i)
								.getState()));
				}

				boolean found = true;
 
				// considering two cases v=([], key1), conf=([some], null),
				// some=key1
				if (v.size() == 0 && conf.size() == 1
						&& conf.get(0).getKey().equals(res.getNewPosition()))
					return res;
				// considering two cases v=([some], key1), conf=([], key),
				// some=key
				// it is not possible that v=([some], null)
				if (v.size() == 1 && conf.size() == 0)
					if (v.get(0).getKey().equals(newPosition))
						return res;
 
				if (v.size() == conf.size() && v.size() > 0) {
					// Two conf are identiacal if each key has the same value
					for (String str : m1.keySet()) {
						if (m1.get(str) == null || m2.get(str) == null) {
							found = false;
							break;
						}
						if (m1.get(str).intValue() != m2.get(str).intValue()) {
							found = false;
							break;
						}
					}
				} else if (v.size() == 0 && conf.size() == 0) {
					// check onto consistency with empty conf
					if (newPosition == null)
						return res;
					else if (res.getNewPosition() == null)
						found = false;
					else if (newPosition.equals(res.getNewPosition()))
						return res;
					else
						found = false;
				} else
					// v.size <> conf.size() and both sizes > 0
					found = false;
 
				// when both conf (size<>0) are identical
				if (found)
					// if "v.conf = conf" and "newPosition=null" or
					// res.getNewPosition()==null then it returns this "res"
					if (newPosition == null || res.getNewPosition() == null)
						return res;
					else if (newPosition.equals(res.getNewPosition()))
						return res;
			}
		} // synchronized
		return null;
	}

	// conf is in binary
	Set<OWLAxiom> confToAxioms(Vector<Duo> conf, boolean flag) {
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		Set<String> ents = systemData.getGlobalEntityMap().keySet();
		String[] arrayEnts = (String[]) ents.toArray();
		for (int i = 0; i < ents.size(); i++) {
			OWLAxiom ax = null;
			String key = (String) systemData.getGlobalEntityMap()
					.get(arrayEnts[i]).getName();
			if (i == conf.get(i).getState()) {
				if (flag) {
					ax = entityToAxiom(key, systemData.getGlobalEntityMap(), 1);
				} else {
					ax = entityToAxiom(key, systemData.getGlobalEntityMap(), 0);
				}
				axioms.add(ax);
			}

		}
		return axioms;
	}

	// convert a global conf to local conf
	//
	public Vector<Duo> globalToLocalConf(String uri,
			HashMap<String, GlobalEntity> global, Vector<Duo> conf, int c) {
		Set<String> ents = global.keySet();
		String[] keys = new String[ents.size()];
		int q = 0;
		for (String s : ents)
			keys[q++] = s;
		Vector<Duo> res = new Vector<Duo>();

		for (int i = 0; i < c; i++) {
			String key = (String) keys[i];
			if (conf.get(i).getState() == 1
					&& global.get(key).getUri().equals(uri))
				res.add(new Duo(global.get(key).getName(), 1));
			if (conf.get(i).getState() == 0
					&& global.get(key).getUri().equals(uri))
				res.add(new Duo(global.get(key).getName(), 0));
		}
		return res;
	}

	public boolean isConsistent() throws DRAONRuntimeException 
	{
		for (String uri : systemData.getLocalUris()) 
		{
			remoteResults.put(uri, new HashSet<Result>());
		}
		// Rethink the uniqueness of "GlobalOntoUri"
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());

		Map<String, StateOfConf> statesOfConf = new HashMap<String, StateOfConf>();

		Set<String> ents = systemData.getGlobalEntityMap().keySet();
		String[] arrayEnts = new String[ents.size()];
		int q = 0;
		for (String s : ents)
			arrayEnts[q++] = s;

		Vector<Duo> conf = new Vector<Duo>(ents.size());
		Vector<OWLAxiom> axioms = new Vector<OWLAxiom>(ents.size());
		for (String ent : ents) {
			conf.add(new Duo(ent, 0));
			axioms.add(null);
			statesOfConf.put(ent, new StateOfConf(0, 0, 0));
		}

		int c = -1;
		// if c==-1 then conf is empty and key="null", we check consistencies
		// if c==0 then conf is empty and "key" is the first;
		while (c < ents.size()) 
		{
			String key = null;
			if (c == -1)
				key = null;
			else
				key = arrayEnts[c];

			Set<OWLAxiom> newAxioms = new HashSet<OWLAxiom>();
			for (int i = 0; i < c; i++)
				newAxioms.add(axioms.get(i));

			OWLAxiom nonemptyAx = null;
			if (key != null)
				nonemptyAx = entityToAxiom(key,
						systemData.getGlobalEntityMap(), 1);
			OWLAxiom emptyAx = null;
			if (key != null)
				emptyAx = entityToAxiom(key, systemData.getGlobalEntityMap(), 0);

			boolean emptyIn = false, nonemptyIn = false;
			boolean empty = true, nonempty = true;

			boolean empty1 = localReasoner.isEntailed(
					systemData.getOWLAlignOnto(), newAxioms, emptyAx);
			boolean nonempty1 = localReasoner.isEntailed(
					systemData.getOWLAlignOnto(), newAxioms, nonemptyAx);
			if (emptyAx == null)
				empty1 = !empty1;

			if (nonemptyAx == null)
				nonempty1 = !nonempty1;

			Result res = new Result(empty1, nonempty1, key, conf);

			remoteResults.get(systemData.getGlobalOntoUri()).add(res);

			if (empty1)
				nonemptyIn = true;
			if (nonempty1)
				emptyIn = true;
			if (empty && !empty1)
				empty = false;
			if (nonempty && !nonempty1)
				nonempty = false;
			// variables empty, nonempty, emptyIn, nonemptyIn can be computed
			// from empty1, nonempty1
			if (!(emptyIn && nonemptyIn))
				for (String uri : systemData.getLocalUris()) {

					// we put here code for distributed reasoning
					// askReasoner( String uri, Vector<Integer> compressedConf,
					// int newPosition, boolean flag);

					Vector<Duo> localConf = globalToLocalConf(uri,
							systemData.getGlobalEntityMap(), conf, c);

					String tmpKey = null;
					if (conceptProjections.get(uri).contains(key)
							|| roleProjections.get(uri).contains(key))
						tmpKey = key;

					Result isRes = checkLocalResult(uri, localConf, tmpKey);

					if (isRes != null) {
						res = isRes;
						empty1 = res.getEmptyEntailed();
						nonempty1 = res.getNonemptyEntailed();
					} else {
						 
						empty1 = localReasoner.isEntailed(
								systemData.getLocationFromUri(uri), newAxioms,
								emptyAx);
						nonempty1 = localReasoner.isEntailed(
								systemData.getLocationFromUri(uri), newAxioms,
								nonemptyAx);

						if (emptyAx == null)
							empty1 = !empty1;// converting consistency to
												// entailement

						if (nonemptyAx == null)
							nonempty1 = !nonempty1;// converting consistency to
													// entailement

						res = new Result(empty1, nonempty1, key, localConf);
					}
					if (empty1)
						nonemptyIn = true;
					if (nonempty1)
						emptyIn = true;
					if (empty && !empty1)
						empty = false;
					if (nonempty && !nonempty1)
						nonempty = false;

					remoteResults.get(uri).add(res);

					if (emptyIn && nonemptyIn)
						break;
				} // for

			if (c == -1) {
				c++;
				continue;
			}

			if (emptyIn && nonemptyIn) {
				boolean found = false;
				int zeroIn = 0;
				for (int j = c - 1; j >= 0; j--) {
					if (statesOfConf.get(arrayEnts[j]).getModel() == 0) {
						found = true;
						zeroIn = j;
						break;
					}
				}
				if (!found) {

					return false;
				} else {
					statesOfConf.get(arrayEnts[zeroIn]).setModel(2);
					for (int k = zeroIn + 1; k < systemData
							.getGlobalEntityMap().size(); k++) {
						statesOfConf.get(arrayEnts[k]).setEmpty(0);
						statesOfConf.get(arrayEnts[k]).setNonempty(0);
						statesOfConf.get(arrayEnts[k]).setModel(0);
						conf.set(k, new Duo(arrayEnts[k], 0));
					}
					// reset "conf"

					if (statesOfConf.get(arrayEnts[zeroIn]).getEmpty() == 0) {
						// newConf.add(zeroIn, new Integer(0));
						conf.set(zeroIn, new Duo(arrayEnts[zeroIn], 0));
					} else {
						// newConf.add(zeroIn, new Integer(1));
						conf.set(zeroIn, new Duo(arrayEnts[zeroIn], 1));
					}
					 

					if (statesOfConf.get(arrayEnts[zeroIn]).getEmpty() == 0) {
						axioms.set(
								zeroIn,
								entityToAxiom((String) arrayEnts[zeroIn],
										systemData.getGlobalEntityMap(), 0));
						// newAxioms.add(entityToAxiom(
						// (String)arrayAnts[zeroIn],
						// systemData.getGlobalEntityMap(), 0));
					} else {
						axioms.set(
								zeroIn,
								entityToAxiom((String) arrayEnts[zeroIn],
										systemData.getGlobalEntityMap(), 1));

					}

					c = zeroIn + 1;

					continue; // to while with c
				}
			}

			if (empty) {

				statesOfConf.get(arrayEnts[c]).setModel(1);
				statesOfConf.get(arrayEnts[c]).setEmpty(1);
				conf.set(c, new Duo(arrayEnts[c], 0));
				axioms.set(c,entityToAxiom(arrayEnts[c], systemData.getGlobalEntityMap(), 0));
				c++;

			} else if (nonempty) {

				statesOfConf.get(arrayEnts[c]).setModel(1);
				statesOfConf.get(arrayEnts[c]).setNonempty(1);
				// conf.add(new Integer(1));
				conf.set(c, new Duo(arrayEnts[c], 1));
				axioms.set(c, entityToAxiom(arrayEnts[c], systemData.getGlobalEntityMap(), 1));

				c++;
			} else {

				int w = 0;
				if (statesOfConf.get(arrayEnts[c]).getEmpty() == 0
						&& statesOfConf.get(arrayEnts[c]).getNonempty() == 0) {
					w = 0; // we choose empty , we can define a strategy in
							// Configurarion
					// statesOfConf.get(arrayAnts[c]).setNonempty(1);
					statesOfConf.get(arrayEnts[c]).setEmpty(1);
				} else {
					if (statesOfConf.get(arrayEnts[c]).getNonempty() == 0) {
						w = 1;
						statesOfConf.get(arrayEnts[c]).setNonempty(1);
					} else {
						w = 0;
						statesOfConf.get(arrayEnts[c]).setEmpty(1);
					}
					statesOfConf.get(arrayEnts[c]).setModel(2); // not important
				}
				// conf.add(new Integer(w));
				conf.set(c, new Duo(arrayEnts[c], w));
				axioms.set(c, entityToAxiom(arrayEnts[c], systemData.getGlobalEntityMap(), w));
				c++;
			}
		}// while for c
		return true;
	}

	// It sets "localMap", "globalConceptSet", "globalIndSet" and
	// "globalEntityMap" in DataSystem.
	// It returns a new align onto
	public OWLOntology setDataForEntailment(OWLAxiom initAxiom)
			throws DRAONRuntimeException 
	{
		OWLOntology aOnto = systemData.getOWLAlignOnto();
		manager = OWLManager.createOWLOntologyManager();
		facto = manager.getOWLDataFactory();
		OWLOntology tOnto = null;
		File tmpFile = null;
		try {
			tmpFile = File.createTempFile("tmpOntologyForEntailed", "owl");
			tmpFile.deleteOnExit();
		} catch (IOException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data :" + ex);
		}

		try {
			tOnto = manager.createOntology(IRI.create("file:"
					+ tmpFile.getAbsolutePath()));

		} catch (OWLOntologyCreationException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data :" + ex);
		}

		Set<OWLAxiom> tAxioms = (Set<OWLAxiom>) aOnto.getAxioms();
		for (OWLAxiom ax : tAxioms) {
			AddAxiom addaxiom = new AddAxiom(tOnto, ax);
			manager.applyChange(addaxiom);
		}

		// creating B(b) and a<>b where axiom = B(a)
		if (initAxiom instanceof OWLClassAssertionAxiom) {
			OWLClassAssertionAxiom axiom = (OWLClassAssertionAxiom) initAxiom;
			OWLIndividual aInd = axiom.getIndividual();
			String aIndName = aInd.asOWLNamedIndividual().getIRI().toString();
			String indUri = null;
			int indexNum = 0;
			for (int i = 0; i < aIndName.length(); i++)
				if (aIndName.charAt(i) == '#') {
					indexNum = i;
					break;
				}
			indUri = aIndName.substring(0, indexNum);

			OWLClassExpression bClass = axiom.getClassExpression();
			String bClsName = bClass.asOWLClass().getIRI().toString();
			for (int i = 0; i < bClsName.length(); i++)
				if (bClsName.charAt(i) == '#') {
					indexNum = i;
					break;
				}
			String clsUri = bClsName.substring(0, indexNum);

			systemData.getLocalMap().addConcept(clsUri, bClsName);
			systemData.getLocalMap().addInd(indUri, aIndName);

			if (!systemData.getGlobalIndSet().contains(aIndName)) {
				systemData.getGlobalIndSet().add(aIndName);
				GlobalEntity ge = new GlobalEntity(aIndName, indUri, 3);
				systemData.getGlobalEntityMap().put(aIndName, ge);
			}

			if (!systemData.getGlobalConceptSet().contains(bClsName)) {
				systemData.getGlobalConceptSet().add(bClsName);
				GlobalEntity ge = new GlobalEntity(bClsName, clsUri, 0);
				systemData.getGlobalEntityMap().put(bClsName, ge);
			}

			String bIndName = DRAON.uriRoot + getGlobalOntoName() + ".owl"
					+   Integer.toString(DRAONServices.getGlobalEntityId());
			OWLIndividual bInd = facto.getOWLNamedIndividual(IRI
					.create(bIndName));
			OWLClassAssertionAxiom classBb = facto.getOWLClassAssertionAxiom(
					bClass, bInd);
			OWLDifferentIndividualsAxiom aDb = facto
					.getOWLDifferentIndividualsAxiom(aInd, bInd);
			AddAxiom addaxiom = new AddAxiom(tOnto, classBb);
			manager.applyChange(addaxiom);
			addaxiom = new AddAxiom(tOnto, aDb);
			manager.applyChange(addaxiom);

			return tOnto;
			// creating A(b), b<>c and B(c) where axiom = A < B
		} else if (initAxiom instanceof OWLSubClassOfAxiom) {
			OWLSubClassOfAxiom axiom = (OWLSubClassOfAxiom) initAxiom;
			OWLClassExpression cls1 = axiom.getSuperClass();
			OWLClassExpression cls2 = axiom.getSubClass();
			String cls1Name = cls1.asOWLClass().getIRI().toString();
			String cls1Uri = null;
			int indexNum = 0;
			for (int i = 0; i < cls1Name.length(); i++)
				if (cls1Name.charAt(i) == '#') {
					indexNum = i;
					break;
				}
			cls1Uri = cls1Name.substring(0, indexNum);

			String cls2Name = cls2.asOWLClass().getIRI().toString();
			for (int i = 0; i < cls2Name.length(); i++)
				if (cls2Name.charAt(i) == '#') {
					indexNum = i;
					break;
				}
			String cls2Uri = cls2Name.substring(0, indexNum);

			systemData.getLocalMap().addConcept(cls1Uri, cls1Name);
			systemData.getLocalMap().addConcept(cls2Uri, cls2Name);
			if (!systemData.getGlobalConceptSet().contains(cls1Name)) {
				systemData.getGlobalConceptSet().add(cls1Name);
				GlobalEntity ge = new GlobalEntity(cls1Name, cls1Uri, 0);
				systemData.getGlobalEntityMap().put(cls1Name, ge);
			}
			if (!systemData.getGlobalConceptSet().contains(cls2Name)) {
				systemData.getGlobalConceptSet().add(cls2Name);
				GlobalEntity ge = new GlobalEntity(cls2Name, cls2Uri, 0);
				systemData.getGlobalEntityMap().put(cls2Name, ge);
			}

			String bIndName = DRAON.uriRoot + getGlobalOntoName() + ".owl"
					+   Integer.toString(DRAONServices.getGlobalEntityId());
			String cIndName = DRAON.uriRoot + getGlobalOntoName() + ".owl"
					+   Integer.toString(DRAONServices.getGlobalEntityId());
			OWLIndividual bInd = facto.getOWLNamedIndividual(IRI
					.create(bIndName));
			OWLIndividual cInd = facto.getOWLNamedIndividual(IRI
					.create(cIndName));
			// cls1 : superClass, cls2 : subClass
			OWLClassAssertionAxiom classAb = facto.getOWLClassAssertionAxiom(
					cls2, bInd);
			OWLClassAssertionAxiom classBc = facto.getOWLClassAssertionAxiom(
					cls1, cInd);
			OWLDifferentIndividualsAxiom bDc = facto
					.getOWLDifferentIndividualsAxiom(bInd, cInd);

			AddAxiom addaxiom = new AddAxiom(tOnto, classAb);
			manager.applyChange(addaxiom);
			addaxiom = new AddAxiom(tOnto, classBc);
			manager.applyChange(addaxiom);

			addaxiom = new AddAxiom(tOnto, bDc);
			manager.applyChange(addaxiom);
			return tOnto;

		} else
			throw new DRAONRuntimeException("DRAON: unsupported axiom :"
					+ initAxiom);
	}

	public boolean isEntailed(OWLAxiom axiom) throws DRAONRuntimeException {
		boolean result = false;
		OWLOntology aOnto = systemData.getOWLAlignOnto();
		OWLOntology tOnto = setDataForEntailment(axiom);
		systemData.setOWLAlignOnto(tOnto);

		result = isConsistent();
		systemData.setOWLAlignOnto(aOnto);
		return !result;
	}
	
	/* 
	 * CLD: new version, not optimized
	 * It corresponds to what a centralized version does
	 * Normaly, there are 2 nodes
	 */
	public boolean isConsistent(Vector<LocalNode> nodes) throws DRAONRuntimeException 
	{
		Vector<DistributedPropagate> reasoners =  new Vector<DistributedPropagate>();
		for(int i=0; i < nodes.size(); i++)
		{
			//System.out.println("uri="+nodes.get(i).getUri());
			//System.out.println("IP="+nodes.get(i).getIP());
			reasoners.add( new DistributedPropagate(nodes.get(i).getUri(), nodes.get(i).getIP(), nodes.get(i).getPort() ));
		}
		for (String uri : systemData.getLocalUris()) 
		{
			remoteResults.put(uri, new HashSet<Result>());
		}
		// Rethink the uniqueness of "GlobalOntoUri"
		//System.out.println("SystemData:GlobalOntoUri="+ systemData.getGlobalOntoUri());
		
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());

		Map<String, StateOfConf> statesOfConf = new HashMap<String, StateOfConf>();

		Set<String> ents = systemData.getGlobalEntityMap().keySet();
		String[] arrayEnts = new String[ents.size()];
		int q = 0;
		for (String s : ents)
			arrayEnts[q++] = s;

		Vector<Duo> conf = new Vector<Duo>(ents.size());
		Vector<OWLAxiom> axioms = new Vector<OWLAxiom>(ents.size());
		for (String ent : ents) {
			conf.add(new Duo(ent, 0));
			axioms.add(null);
			statesOfConf.put(ent, new StateOfConf(0, 0, 0));
		}

		int c = -1;
		while (c < ents.size()) 
		{
			String key = null;
			if (c == -1)
				key = null;
			else
				key = arrayEnts[c];

			Set<OWLAxiom> newAxioms = new HashSet<OWLAxiom>();
			for (int i = 0; i < c; i++)
				newAxioms.add(axioms.get(i));

			OWLAxiom nonemptyAx = null;
			if (key != null)
				nonemptyAx = entityToAxiom(key,
						systemData.getGlobalEntityMap(), 1);
			OWLAxiom emptyAx = null;
			if (key != null)
				emptyAx = entityToAxiom(key, systemData.getGlobalEntityMap(), 0);

			boolean emptyIn = false, nonemptyIn = false;
			boolean empty = true, nonempty = true;

			boolean empty1 = localReasoner.isEntailed(
					systemData.getOWLAlignOnto(), newAxioms, emptyAx);
			boolean nonempty1 = localReasoner.isEntailed(
					systemData.getOWLAlignOnto(), newAxioms, nonemptyAx);
			if (emptyAx == null)
				empty1 = !empty1;

			if (nonemptyAx == null)
				nonempty1 = !nonempty1;

			Result res = new Result(empty1, nonempty1, key, conf);

			remoteResults.get(systemData.getGlobalOntoUri()).add(res);

			if (empty1)
				nonemptyIn = true;
			if (nonempty1)
				emptyIn = true;
			if (empty && !empty1)
				empty = false;
			if (nonempty && !nonempty1)
				nonempty = false;
			// variables empty, nonempty, emptyIn, nonemptyIn can be computed
			// from empty1, nonempty1
			if (!(emptyIn && nonemptyIn))
				for (String uri : systemData.getLocalUris()) {

					// we put here code for distributed reasoning
					// askReasoner( String uri, Vector<Integer> compressedConf,
					// int newPosition, boolean flag);
					Vector<Duo> localConf = globalToLocalConf(uri,
							systemData.getGlobalEntityMap(), conf, c);

					String tmpKey = null;
					if (conceptProjections.get(uri).contains(key)
							|| roleProjections.get(uri).contains(key))
						tmpKey = key;

					Result isRes = checkLocalResult(uri, localConf, tmpKey);

					if (isRes != null) {
						res = isRes;
						empty1 = res.getEmptyEntailed();
						nonempty1 = res.getNonemptyEntailed();
					} else {
						//Distributed calls
						empty1 = reasoners.get(0).isEntailed(newAxioms,emptyAx);
						nonempty1 = reasoners.get(1).isEntailed(newAxioms,nonemptyAx);

						if (emptyAx == null)
							empty1 = !empty1;// converting consistency to
												// entailement

						if (nonemptyAx == null)
							nonempty1 = !nonempty1;// converting consistency to
													// entailement

						res = new Result(empty1, nonempty1, key, localConf);
					}
					if (empty1)
						nonemptyIn = true;
					if (nonempty1)
						emptyIn = true;
					if (empty && !empty1)
						empty = false;
					if (nonempty && !nonempty1)
						nonempty = false;

					remoteResults.get(uri).add(res);

					if (emptyIn && nonemptyIn)
						break;
				} // for

			if (c == -1) {
				c++;
				continue;
			}

			if (emptyIn && nonemptyIn) {
				boolean found = false;
				int zeroIn = 0;
				for (int j = c - 1; j >= 0; j--) {
					if (statesOfConf.get(arrayEnts[j]).getModel() == 0) {
						found = true;
						zeroIn = j;
						break;
					}
				}
				if (!found) {

					return false;
				} else {
					statesOfConf.get(arrayEnts[zeroIn]).setModel(2);
					for (int k = zeroIn + 1; k < systemData
							.getGlobalEntityMap().size(); k++) {
						statesOfConf.get(arrayEnts[k]).setEmpty(0);
						statesOfConf.get(arrayEnts[k]).setNonempty(0);
						statesOfConf.get(arrayEnts[k]).setModel(0);
						conf.set(k, new Duo(arrayEnts[k], 0));
					}
					// reset "conf"

					if (statesOfConf.get(arrayEnts[zeroIn]).getEmpty() == 0) {
						// newConf.add(zeroIn, new Integer(0));
						conf.set(zeroIn, new Duo(arrayEnts[zeroIn], 0));
					} else {
						// newConf.add(zeroIn, new Integer(1));
						conf.set(zeroIn, new Duo(arrayEnts[zeroIn], 1));
					}
					 

					if (statesOfConf.get(arrayEnts[zeroIn]).getEmpty() == 0) {
						axioms.set(
								zeroIn,
								entityToAxiom((String) arrayEnts[zeroIn],
										systemData.getGlobalEntityMap(), 0));
 
					} else {
						axioms.set(
								zeroIn,
								entityToAxiom((String) arrayEnts[zeroIn],
										systemData.getGlobalEntityMap(), 1));

					}

					c = zeroIn + 1;

					continue; // to while with c
				}
			}

			if (empty) {

				statesOfConf.get(arrayEnts[c]).setModel(1);
				statesOfConf.get(arrayEnts[c]).setEmpty(1);

				// conf.add(new Integer(0));
				conf.set(c, new Duo(arrayEnts[c], 0));

				axioms.set(
						c,
						entityToAxiom(arrayEnts[c],
								systemData.getGlobalEntityMap(), 0));

				c++;

			} else if (nonempty) {

				statesOfConf.get(arrayEnts[c]).setModel(1);
				statesOfConf.get(arrayEnts[c]).setNonempty(1);
				// conf.add(new Integer(1));
				conf.set(c, new Duo(arrayEnts[c], 1));
				axioms.set(
						c,
						entityToAxiom(arrayEnts[c],
								systemData.getGlobalEntityMap(), 1));

				c++;
			} else {

				int w = 0;
				if (statesOfConf.get(arrayEnts[c]).getEmpty() == 0
						&& statesOfConf.get(arrayEnts[c]).getNonempty() == 0) {
					w = 0; // we choose empty , we can define a strategy in
							// Configurarion
					// statesOfConf.get(arrayAnts[c]).setNonempty(1);
					statesOfConf.get(arrayEnts[c]).setEmpty(1);
				} else {
					if (statesOfConf.get(arrayEnts[c]).getNonempty() == 0) {
						w = 1;
						statesOfConf.get(arrayEnts[c]).setNonempty(1);
					} else {
						w = 0;
						statesOfConf.get(arrayEnts[c]).setEmpty(1);
					}
					statesOfConf.get(arrayEnts[c]).setModel(2); // not important
				}
				// conf.add(new Integer(w));
				conf.set(c, new Duo(arrayEnts[c], w));
				axioms.set(
						c,
						entityToAxiom(arrayEnts[c],
								systemData.getGlobalEntityMap(), w));

				c++;
			}
		}// while for c
		return true;
	}

	/* 
	 * CLD: Optimized and distributed algorithm
	 * 
	 */
	/*public boolean isConsistent(Set<LocalNode> nodes)
			throws DRAONRuntimeException 
    {
		for (String uri : systemData.getLocalUris()) 
		{
			remoteResults.put(uri, new HashSet<Result>());
		}
		// Rethink the uniqueness of "GlobalOntoUri"
		remoteResults.put(systemData.getGlobalOntoUri(), new HashSet<Result>());

		Map<String, StateOfConf> statesOfConf = new HashMap<String, StateOfConf>();

		Set<String> ents = systemData.getGlobalEntityMap().keySet();

		String[] arrayEnts = new String[ents.size()];
		int q = 0;
		for (String s : ents)
			arrayEnts[q++] = s;
		globalEnts = arrayEnts;

		// conf in binary
		Vector<Duo> conf = new Vector<Duo>(ents.size());
		Vector<OWLAxiom> axioms = new Vector<OWLAxiom>(ents.size());
		for (String ent : ents) 
		{
			conf.add(new Duo(ent, 0));
			axioms.add(null);
			statesOfConf.put(ent, new StateOfConf(0, 0, 0));
		}

		//create remote reasoners which share structures defined in systemData
		//Connector[] conns = new Connector[nodes.size()];
		for (LocalNode node : nodes) 
		{
			Connector conn = new Connector(node.getHttp(), node.getPort(), node.getUri(), ents, this);
			//remoteReasoners.put(node.getUri(), conn );
			//conns[i++] = conn;
		}

		setTerminated(new Integer(0));
		int c = -1;

		// If c==0 then conf is empty; "c" is the index of new position
		while (c < ents.size()) 
		{
			String key = null;
			if (c == -1)
				key = null;
			else
				key = arrayEnts[c];

			Set<OWLAxiom> newAxioms = new HashSet<OWLAxiom>();
			for (int i = 0; i < c; i++)
				newAxioms.add(axioms.get(i));

			OWLAxiom nonemptyAx = null;
			if (key != null)
				nonemptyAx = entityToAxiom(key,
						systemData.getGlobalEntityMap(), 1);
			OWLAxiom emptyAx = null;
			if (key != null)
				emptyAx = entityToAxiom(key, systemData.getGlobalEntityMap(), 0);

			boolean emptyIn = false, nonemptyIn = false;
			boolean empty = true, nonempty = true;
			
			//Determines emptiness of "emptyAx" with respect to "newAxioms" from the alignment
			boolean empty1 = localReasoner.isEntailed(
					         systemData.getOWLAlignOnto(), newAxioms, emptyAx);
			if (emptyAx == null)
				empty1 = !empty1;
			boolean nonempty1 = localReasoner.isEntailed(
					            systemData.getOWLAlignOnto(), newAxioms, nonemptyAx);
			if (nonemptyAx == null)
				nonempty1 = !nonempty1;

			Result res = new Result(empty1, nonempty1, key, conf);

			remoteResults.get(systemData.getGlobalOntoUri()).add(res);

			if (empty1)
				nonemptyIn = true;
			if (nonempty1)
				emptyIn = true;
			if (empty && !empty1)
				empty = false;
			if (nonempty && !nonempty1)
				nonempty = false;

			String tmpKey = null;

			// register conf for local reasoners
			Vector<Duo> localConf = null;
			if (!(emptyIn && nonemptyIn))
				for (String uri : systemData.getLocalUris()) 
				{
					tmpKey = null;
					if (conceptProjections.get(uri).contains(key) || roleProjections.get(uri).contains(key))
						tmpKey = key;
					localConf = globalToLocalConf(uri, systemData.getGlobalEntityMap(), conf, c);
					res = new Result(false, false, tmpKey, localConf);
					synchronized (confsFromGlobal) {
						confsFromGlobal.get(uri).add(res);
					}
				}

			// fetch remoteResult
			if (!(emptyIn && nonemptyIn))
				for (String uri : systemData.getLocalUris()) 
				{
					tmpKey = null;
					if (conceptProjections.get(uri).contains(key)
							|| roleProjections.get(uri).contains(key))
						tmpKey = key;
					localConf = globalToLocalConf(uri,
							systemData.getGlobalEntityMap(), conf, c);
					res = null;
					//System.out.println(remoteResults);
					do {
						res = checkLocalResult(uri, localConf, tmpKey);
					} while (res == null);
					 
					empty1 = res.getEmptyEntailed();
					nonempty1 = res.getNonemptyEntailed();					
					
					if (empty1)
						nonemptyIn = true;
					if (nonempty1)
						emptyIn = true;
					if (empty && !empty1)
						empty = false;
					if (nonempty && !nonempty1)
						nonempty = false;

					synchronized (remoteResults) {
						remoteResults.get(uri).add(res);
					}
					if (emptyIn && nonemptyIn)
						break;
				}  
			if (c == -1) {
				c++;
				continue;
			}

			if (emptyIn && nonemptyIn) {
				boolean found = false;
				int zeroIn = 0;
				for (int j = c - 1; j >= 0; j--) {
					if (statesOfConf.get(arrayEnts[j]).getModel() == 0) {
						found = true;
						zeroIn = j;
						break;
					}
				}

				if (!found) {
					setTerminated(new Integer(1));
					return false;
				} else {

					statesOfConf.get(arrayEnts[zeroIn]).setModel(2);
					for (int k = zeroIn + 1; k < systemData
							.getGlobalEntityMap().size(); k++) 
					{
						statesOfConf.get(arrayEnts[k]).setEmpty(0);
						statesOfConf.get(arrayEnts[k]).setNonempty(0);
						statesOfConf.get(arrayEnts[k]).setModel(0);
						conf.set(k, new Duo(arrayEnts[k], 0));
					}

					if (statesOfConf.get(arrayEnts[zeroIn]).getEmpty() == 0) {
						// newConf.add(zeroIn, new Integer(0));
						conf.set(zeroIn, new Duo(arrayEnts[zeroIn], 0));
					} else {
						// newConf.add(zeroIn, new Integer(1));
						conf.set(zeroIn, new Duo(arrayEnts[zeroIn], 1));
					}

					if (statesOfConf.get(arrayEnts[zeroIn]).getEmpty() == 0) {
						axioms.set(
								zeroIn,
								entityToAxiom(arrayEnts[zeroIn],
										systemData.getGlobalEntityMap(), 0));
						// newAxioms.add(entityToAxiom( ents.toArray()[zeroIn],
						// systemData.getGlobalEntityMap(), 0));
					} else {
						axioms.set(
								zeroIn,
								entityToAxiom(arrayEnts[zeroIn],
										systemData.getGlobalEntityMap(), 1));
						// newAxioms.add(entityToAxiom( ents.toArray()[zeroIn],
						// systemData.getGlobalEntityMap(), 1));
					}

					c = zeroIn + 1;
					continue; // to while with c
				}
			}

			if (empty) 
			{
				statesOfConf.get(arrayEnts[c]).setModel(1);
				statesOfConf.get(arrayEnts[c]).setEmpty(1);

				// conf.add(new Integer(0));
				conf.set(c, new Duo(arrayEnts[c], 0));

				axioms.set(
						c,
						entityToAxiom(arrayEnts[c],
								systemData.getGlobalEntityMap(), 0));
				c++;

			} else if (nonempty) {
				statesOfConf.get(arrayEnts[c]).setModel(1);
				statesOfConf.get(arrayEnts[c]).setNonempty(1);
				// conf.add(new Integer(1));
				conf.set(c, new Duo(arrayEnts[c], 1));
				axioms.set(
						c,
						entityToAxiom(arrayEnts[c],
								systemData.getGlobalEntityMap(), 1));
				c++;
			} else {

				int w = 0;
				if (statesOfConf.get(arrayEnts[c]).getEmpty() == 0
						&& statesOfConf.get(arrayEnts[c]).getNonempty() == 0) {
					w = 0; // flag: we choose empty
					// statesOfConf.get(ents.toArray()[c]).setNonempty(1);
					statesOfConf.get(arrayEnts[c]).setEmpty(1);
				} else {
					if (statesOfConf.get(arrayEnts[c]).getNonempty() == 0) {
						w = 1;
						statesOfConf.get(arrayEnts[c]).setNonempty(1);
					} else {
						w = 0;
						statesOfConf.get(arrayEnts[c]).setEmpty(1);
					}
					statesOfConf.get(arrayEnts[c]).setModel(2); // not important
				}
				// conf.add(new Integer(w));
				conf.set(c, new Duo(arrayEnts[c], w));
				axioms.set( c,
						entityToAxiom(arrayEnts[c],
								systemData.getGlobalEntityMap(), w));
				c++;
			}
		}// while for c
		setTerminated(new Integer(1));
		return true;
	}*/

	public boolean isEntailed(OWLAxiom axiom, Vector<LocalNode> nodes)
			throws DRAONRuntimeException {
		boolean result = false;
		OWLOntology aOnto = systemData.getOWLAlignOnto();
		OWLOntology tOnto = setDataForEntailment(axiom);
		systemData.setOWLAlignOnto(tOnto);

		result = isConsistent(nodes);
		systemData.setOWLAlignOnto(aOnto);
		return !result;
	}

	public void buildLocalConceptProj(Set<String> uris) {
		for (String uri : uris) {
			Set<String> localC = systemData.getLocalMap().getConceptsByUri(uri);
			if (localC != null)
				conceptProjections.put(uri, localC);
		}
	}

	// Restore all concepts that belong to the equi classes with representatives
	// "representatives" and project them on "uri"
	public Set<String> buildLocalConceptProj(String uri, Set<String> conceptConf) {
		Set<String> resSet = new HashSet<String>();

		Set<String> expandedC = new HashSet<String>();

		if (conceptConf == null)
			return resSet;
		for (String c : conceptConf)
			if (!expandedC.contains(c))
				expandedC.add(c);
		// expandedC.addAll( conceptConf );

		for (String c : systemData.getLocalMap().getConceptsByUri(uri)) {
			if (expandedC.contains(c))
				resSet.add(c);
		}

		return resSet;
	}

	public void buildLocalObjectRoleProj(Set<String> uris) {
		for (String uri : uris) {
			Set<String> localR = systemData.getLocalMap().getObjectRolesByUri(
					uri);
			if (localR != null) {
				objectRoleProjections.put(uri, localR);
			}
		}
	}

	public void buildLocalDataRoleProj(Set<String> uris) {
		for (String uri : uris) {

			Set<String> localR = systemData.getLocalMap()
					.getDataRolesByUri(uri);
			if (localR != null) {
				dataRoleProjections.put(uri, localR);
			}
		}
	}

	public void buildLocalRoleProj(Set<String> uris) {
		for (String uri : uris) {

			Set<String> localR = systemData.getLocalMap().getRolesByUri(uri);
			if (localR != null) {
				roleProjections.put(uri, localR);
			}
		}
	}

	public void buildLocalIndProj(Set<String> uris) {
		for (String uri : uris) {

			Set<String> localR = systemData.getLocalMap().getIndsByUri(uri);
			if (localR != null) {
				individualProjections.put(uri, localR);
			}
		}
	}

	public Set<String> buildLocalObjectRoleProj(String uri,
			Set<String> objectRoleConf) 
    {
		Set<String> resSet = new HashSet<String>();
		Set<String> expandedR = new HashSet<String>();
		if (objectRoleConf == null)
			return resSet;

		for (String c : objectRoleConf)
			if (!expandedR.contains(c))
				expandedR.add(c);

		// expandedR.addAll( objectRoleConf );

		if (systemData.getLocalMap().getObjectRolesByUri(uri) == null)
			return resSet;
		for (String r : systemData.getLocalMap().getObjectRolesByUri(uri)) {
			if (expandedR.contains(r)) {
				resSet.add(r);
			}
		}

		return resSet;
	}

	public Set<String> buildLocalDataRoleProj(String uri, Set<String> dataRoleConf) 
	{
		Set<String> resSet = new HashSet<String>();
		Set<String> expandedR = new HashSet<String>();
		if (dataRoleConf == null)
			return resSet;
		for (String c : dataRoleConf)
			if (!expandedR.contains(c))
				expandedR.add(c);
		// expandedR.addAll( dataRoleConf );

		if (systemData.getLocalMap().getDataRolesByUri(uri) == null)
			return resSet;
		for (String r : systemData.getLocalMap().getDataRolesByUri(uri)) {
			if (expandedR.contains(r)) {
				resSet.add(r);
			}
		}

		return resSet;
	}

	public Set<String> buildLocalRoleProj(String uri,
			Set<String> representatives) 
	{
		Set<String> resSet = new HashSet<String>();
		for (String c : buildLocalDataRoleProj(uri, representatives))
			if (!resSet.contains(c))
				resSet.add(c);
		for (String c : buildLocalObjectRoleProj(uri, representatives))
			if (!resSet.contains(c))
				resSet.add(c);

		// resSet.addAll( buildLocalDataRoleProj(uri, representatives) );
		// resSet.addAll( buildLocalObjectRoleProj(uri, representatives) );
		return resSet;
	}

	public Set<String> buildLocalIndProj(String uri, Set<String> indConf) {
		Set<String> resSet = new HashSet<String>();
		Set<String> expandedR = new HashSet<String>();
		if (indConf == null)
			return resSet;
		for (String c : indConf)
			if (!expandedR.contains(c))
				expandedR.add(c);
		// expandedR.addAll( dataRoleConf );

		if (systemData.getLocalMap().getIndsByUri(uri) == null)
			return resSet;
		for (String r : systemData.getLocalMap().getDataRolesByUri(uri)) {
			if (expandedR.contains(r)) {
				resSet.add(r);
			}
		}

		return resSet;
	}

	public OWLOntology createExAlignOntology(String ontoName,
			Set<OWLAxiom> axsFromConceptConf, Set<OWLAxiom> axsFromRoleConf)
			throws DRAONRuntimeException {
		// JE2010: in spite of its name, the same manager is always returned
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology initOnto = systemData.getOWLAlignOnto();
		Set<OWLAxiom> axiomSet = initOnto.getAxioms();
		// OWLOntology onto = new OWLOntology( manager, new OWLOntologyID(
		// IRI.create(ontoName) ) );
		OWLOntology onto = null;
		try {
			onto = manager.createOntology(IRI.create(ontoName));
		} catch (OWLOntologyCreationException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data", ex);
		}

		for (OWLAxiom ax : axiomSet) {
			AddAxiom addaxiom = new AddAxiom(initOnto, ax);
			// addaxiom.accept( changeVisitor );
			manager.applyChange(addaxiom);
		}

		localReasoner.loadOntology(onto);
		// localReasoner.reasoner.unloadOntology( onto );

		for (OWLAxiom ax : axsFromConceptConf) {
			AddAxiom addaxiom = new AddAxiom(onto, ax);
			// addaxiom.accept( changeVisitor );
			manager.applyChange(addaxiom);
		}

		for (OWLAxiom ax : axsFromRoleConf) {
			AddAxiom addaxiom = new AddAxiom(onto, ax);
			// addaxiom.accept( changeVisitor );
			manager.applyChange(addaxiom);
		}

		return onto;
	}

	// Add C(a_C) for each C \in cConf, or C \sqsubseteq \bot for each C \notin
	// cConf
	// Add R(a_1, a_2) for each R\in rConf, or \top \sqsubseteq \forall R.\bot
	// for each R\notin rConf
	public Set<OWLAxiom> createLocalAxiomsFromConf(String uri, Set<String> cConf, Set<String> rConf) {
		Set<OWLAxiom> exAxioms = new HashSet<OWLAxiom>();
		Set<String> compConcepts = new HashSet<String>();

		//System.out.println("uri = "+ uri);
		// System.out.println("c proj = "+conceptProjections.get(uri).toString());
		if (conceptProjections.get(uri) != null)
			if (!conceptProjections.get(uri).isEmpty()) {
				for (String st : conceptProjections.get(uri))
					compConcepts.add(st);
				for (String c1 : cConf) {

					compConcepts.remove(c1);
					OWLIndividual ind = facto.getOWLNamedIndividual(IRI
							.create(uri + "#" + getLocalEntityId()));
					OWLClass cls = facto.getOWLClass(IRI.create(c1));
					OWLClassAssertionAxiom ax = facto
							.getOWLClassAssertionAxiom(cls, ind);
					exAxioms.add(ax);
					
					//System.out.println("ID="+ind);
				}

				// creating axioms C \sqsubseteq \forall R . \neg C'
				for (String c2 : compConcepts) {

					OWLClass noth = facto.getOWLNothing();
					OWLClass cls = facto.getOWLClass(IRI.create(c2));
					OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(cls,
							noth);
					exAxioms.add(ax);
				}
			}

		if (roleProjections.get(uri) != null)
			if (!roleProjections.get(uri).isEmpty()) {
				Set<String> compObjectRoles = new HashSet<String>();
				Set<String> compDataRoles = new HashSet<String>();
				for (String st : objectRoleProjections.get(uri))
					compObjectRoles.add(st);
				for (String st : dataRoleProjections.get(uri))
					compDataRoles.add(st);

				// Set<String> compRoles = roleProjections.get(uri);
				// creating r1(ind1,ind2)
				for (String r1 : rConf) {

					OWLProperty r = null;
					if (compObjectRoles.contains(r1)) {
						compObjectRoles.remove(r1);
						r = facto.getOWLObjectProperty(IRI.create(r1));
						OWLIndividual ind1 = facto.getOWLNamedIndividual(IRI
								.create(uri + "#" + getLocalEntityId()));
						OWLIndividual ind2 = facto.getOWLNamedIndividual(IRI
								.create(uri + "#" + getLocalEntityId()));
						OWLObjectPropertyAssertionAxiom ax = facto
								.getOWLObjectPropertyAssertionAxiom(
										(OWLObjectProperty) r, ind1, ind2);
						exAxioms.add(ax);
						
						
						//System.out.println("ID2="+ind1);
					}

					if (compDataRoles.contains(r1)) {
						compDataRoles.remove(r1);
						r = facto.getOWLDataProperty(IRI.create(r1));
						OWLIndividual ind1 = facto.getOWLNamedIndividual(IRI
								.create(uri + "#" + getLocalEntityId()));
						OWLLiteral ind2 = facto.getOWLLiteral(uri + "#"
								+ getLocalEntityId());
						OWLDataPropertyAssertionAxiom ax = facto
								.getOWLDataPropertyAssertionAxiom(
										(OWLDataProperty) r, ind1, ind2);
						exAxioms.add(ax);
						
						//System.out.println("ID3="+ind1);
					}

				}

				// creating axioms C \sqsubseteq \forall R . \neg C'
				for (String r2 : compObjectRoles) {
					OWLClass th = facto.getOWLThing();
					OWLClass noth = facto.getOWLNothing();

					OWLProperty r = facto.getOWLObjectProperty(IRI.create(r2));
					OWLObjectAllValuesFrom cls = facto
							.getOWLObjectAllValuesFrom((OWLObjectProperty) r,
									noth);
					OWLSubClassOfAxiom ax = facto
							.getOWLSubClassOfAxiom(th, cls);
					exAxioms.add(ax);
				}
				// creating axioms C \sqsubseteq \forall R . \neg C'
				for (String r2 : compDataRoles) {
					OWLClass th = facto.getOWLThing();
					OWLDataRange noth = facto.getOWLDataComplementOf(facto
							.getTopDatatype());

					OWLProperty r = facto.getOWLDataProperty(IRI.create(r2));
					OWLDataAllValuesFrom cls = facto.getOWLDataAllValuesFrom(
							(OWLDataProperty) r, noth);
					OWLSubClassOfAxiom ax = facto
							.getOWLSubClassOfAxiom(th, cls);
					exAxioms.add(ax);
				}
			}

		return exAxioms;
	}

	public Set<OWLAxiom> createLocalAxiomsForOntoEx(String uri, Set<String> cConf) 
	{
		Set<OWLAxiom> exAxioms = new HashSet<OWLAxiom>();
		Set<String> compConcepts = new HashSet<String>();
 
		if (conceptProjections.get(uri) != null)
			if (!conceptProjections.get(uri).isEmpty()) {
				for (String st : conceptProjections.get(uri))
					compConcepts.add(st);
				//Set<String> compConcepts = conceptProjections.get(uri);
				for (String c1 : cConf) {

					compConcepts.remove(c1);
					OWLIndividual ind = facto.getOWLNamedIndividual(IRI
							.create(uri + "#" + getLocalEntityId()));
					OWLClass cls = facto.getOWLClass(IRI.create(c1));
					OWLClassAssertionAxiom ax = facto
							.getOWLClassAssertionAxiom(cls, ind);
					exAxioms.add(ax);
				}

				// creating axioms C \sqsubseteq \forall R . \neg C'
				for (String c2 : compConcepts) {

					OWLClass noth = facto.getOWLNothing();
					OWLClass cls = facto.getOWLClass(IRI.create(c2));
					OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(cls,
							noth);
					exAxioms.add(ax);
				}
			}
		return exAxioms;
	}

	// \top \sqsubseteq \exists R .\top
	// \top \sqsubseteq \forall R .\bot
	public Set<OWLAxiom> createGlobalAxiomsFromObjectRoleConf(String nameOnto,
			Set<String> roleConf, Set<String> allRoles) {
		Set<OWLAxiom> axs = new HashSet<OWLAxiom>();
		Set<String> compRoles = new HashSet<String>();
		for (String st : allRoles)
			compRoles.add(st);

		// \top \sqsubseteq \exists R .\top
		for (String r1 : roleConf) {
			compRoles.remove(r1);
			OWLClass th = facto.getOWLThing();
			OWLProperty r = facto.getOWLObjectProperty(IRI.create(r1));
			OWLObjectSomeValuesFrom cl1 = facto.getOWLObjectSomeValuesFrom(
					(OWLObjectProperty) r, th);
			OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl1);
			axs.add(ax);
		}

		// \top \sqsubseteq \forall R .\bot
		for (String r1 : compRoles) {
			OWLClass th = facto.getOWLThing();
			OWLClass noth = facto.getOWLNothing();
			OWLProperty r = facto.getOWLObjectProperty(IRI.create(r1));
			OWLObjectAllValuesFrom cl2 = facto.getOWLObjectAllValuesFrom(
					(OWLObjectProperty) r, noth);
			OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl2);
			axs.add(ax);
		}

		return axs;
	}

	// \top \sqsubseteq \exists R .\top
	// \top \sqsubseteq \forall R .\bot
	public Set<OWLAxiom> createGlobalAxiomsFromDataRoleConf(
			Set<String> roleConf, Set<String> allRoles) {
		Set<OWLAxiom> axs = new HashSet<OWLAxiom>();
		Set<String> compRoles = new HashSet<String>();
		for (String st : allRoles)
			compRoles.add(st);

		// \top \sqsubseteq \exists R .\top
		for (String r1 : roleConf) {
			compRoles.remove(r1);
			OWLDataRange th2 = facto.getTopDatatype();
			OWLClass th = facto.getOWLThing();
			OWLProperty r = facto.getOWLDataProperty(IRI.create(r1));
			OWLDataSomeValuesFrom cl1 = facto.getOWLDataSomeValuesFrom(
					(OWLDataProperty) r, th2);
			OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl1);
			axs.add(ax);
		}

		// \top \sqsubseteq \forall R .\bot
		for (String r1 : compRoles) {
			OWLClass th = facto.getOWLThing();
			OWLDataRange th2 = facto.getTopDatatype();
			OWLProperty r = facto.getOWLDataProperty(IRI.create(r1));
			OWLDataAllValuesFrom cl2 = facto.getOWLDataAllValuesFrom(
					(OWLDataProperty) r, facto.getOWLDataComplementOf(th2));
			OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl2);
			axs.add(ax);
		}

		return axs;
	}

	public Set<OWLAxiom> createGlobalAxiomsFromRoleConf(Set<String> roleConf,
			Set<String> allRoles) {
		Set<OWLAxiom> axs = new HashSet<OWLAxiom>();
		Set<String> compRoles = new HashSet<String>();
		for (String st : allRoles)
			compRoles.add(st);
		Set<String> globalObjectRoles = systemData.getGlobalObjectRoleSet();
		Set<String> globalDataRoles = systemData.getGlobalDataRoleSet();

		// \top \sqsubseteq \exists R .\top
		for (String r1 : roleConf) {
			compRoles.remove(r1);
			OWLClass th = facto.getOWLThing();
			if (globalObjectRoles.contains(r1)) {
				OWLProperty r = facto.getOWLObjectProperty(IRI.create(r1));
				OWLObjectSomeValuesFrom cl1 = facto.getOWLObjectSomeValuesFrom(
						(OWLObjectProperty) r, th);
				OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl1);
				axs.add(ax);
			}
			if (globalDataRoles.contains(r1)) {
				OWLProperty r = facto.getOWLDataProperty(IRI.create(r1));
				OWLDataSomeValuesFrom cl1 = facto.getOWLDataSomeValuesFrom(
						(OWLDataProperty) r, facto.getTopDatatype());
				OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl1);
				axs.add(ax);
			}

		}

		// \top \sqsubseteq \forall R .\bot
		for (String r1 : compRoles) {
			OWLClass th = facto.getOWLThing();
			OWLClass noth = facto.getOWLNothing();
			if (globalObjectRoles.contains(r1)) {
				OWLProperty r = facto.getOWLObjectProperty(IRI.create(r1));
				OWLObjectAllValuesFrom cl2 = facto.getOWLObjectAllValuesFrom(
						(OWLObjectProperty) r, noth);
				OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl2);
				axs.add(ax);
			}

			if (globalDataRoles.contains(r1)) {
				OWLProperty r = facto.getOWLDataProperty(IRI.create(r1));
				OWLDataAllValuesFrom cl2 = facto.getOWLDataAllValuesFrom(
						(OWLDataProperty) r,
						facto.getOWLDataComplementOf(facto.getTopDatatype()));
				OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(th, cl2);
				axs.add(ax);
			}
		}

		return axs;
	}

	// for each "C \in conf", adding C(a) and for each  "C \notin conf", adding
	// C \sqsubseteq \bot
	public Set<OWLAxiom> createGlobalAxiomsFromConceptConf(Set<String> conf,
			Set<String> allConcepts) {
		Set<OWLAxiom> axs = new HashSet<OWLAxiom>();
		Set<String> compConf = new HashSet<String>();
		for (String st : allConcepts)
			compConf.add(st);

		for (String concept : conf) {
			compConf.remove(concept);
			OWLClass cls = facto.getOWLClass(IRI.create(concept));
			String indName = Integer.toString(getGlobalEntityId());
			OWLIndividual ind = facto
					.getOWLNamedIndividual(IRI.create(indName));
			OWLClassAssertionAxiom ax = facto.getOWLClassAssertionAxiom(cls,
					ind);
			axs.add(ax);
		}

		for (String concept : compConf) {
			OWLClass cls1 = facto.getOWLClass(IRI.create(concept));
			OWLClass cls2 = facto.getOWLNothing();
			OWLSubClassOfAxiom ax = facto.getOWLSubClassOfAxiom(cls1, cls2);
			axs.add(ax);
		}

		return axs;
	}

	// This function fills equivalence classes of concepts i.e.
	// equivalent concepts are grouped together.
	public Set<String> buildEquivCls(OWLOntology onto, Set<String> names)
			throws InconsistentOntologyException {
		if (names == null || names.size() == 0)
			return new HashSet<String>();
		localReasoner.loadOntology(onto);
		Set<String> newNames = new HashSet<String>();
		for (String name : names)
			newNames.add(new String(name));

		Set<String> equiv = new HashSet<String>();

		while (newNames.size() > 0) {
			String str = (String) newNames.toArray()[0];
			equiv.add(str);
			newNames.remove(str);
			Set<String> tmp = new HashSet<String>();

			if (systemData.getGlobalEntityMap().get(str).getAttr() == 0) {
				OWLClass cls1 = facto.getOWLClass(IRI.create(str));
				Node<OWLClass> ns = localReasoner.reasoner
						.getEquivalentClasses(cls1);
				for (String key : newNames) {
					if (systemData.getGlobalEntityMap().get(key).getAttr() == 0) {
						OWLClass cls2 = facto.getOWLClass(IRI.create(key));
						if (ns.contains(cls2)) {
							tmp.add(key); // System.err.println("Equiv concept");
						}
					}
				}

			} else if (systemData.getGlobalEntityMap().get(str).getAttr() == 1) {
				OWLObjectProperty pro1 = facto.getOWLObjectProperty(IRI
						.create(str));
				Node<OWLObjectPropertyExpression> ns = localReasoner.reasoner
						.getEquivalentObjectProperties(pro1);
				for (String key : newNames) {
					if (systemData.getGlobalEntityMap().get(key).getAttr() == 1) {
						OWLObjectProperty pro2 = facto.getOWLObjectProperty(IRI
								.create(key));
						if (ns.contains(pro2)) {
							tmp.add(key);// System.err.println("Equiv Object");
						}
					}
				}

			} else if (systemData.getGlobalEntityMap().get(str).getAttr() == 2) {
				OWLDataProperty pro1 = facto
						.getOWLDataProperty(IRI.create(str));
				Node<OWLDataProperty> ns = (Node<OWLDataProperty>) localReasoner.reasoner
						.getEquivalentDataProperties(pro1);
				for (String key : newNames) {
					if (systemData.getGlobalEntityMap().get(key).getAttr() == 2) {
						OWLDataProperty pro2 = facto.getOWLDataProperty(IRI
								.create(key));
						if (ns.contains(pro2)) {
							tmp.add(key);// System.err.println("Equiv Data");
						}
					}
				}
			}
			newNames.removeAll(tmp);
		}// while newNames
		localReasoner.dispose();
		return equiv;
	}

	// This class generates all subsets of "size" of "array"
	// If size > array.length then an exeception raises.
	public class PowerSetBySize implements Iterator<Object> {
		private int[] membership;
		private Object[] array;
		private int size;

		public PowerSetBySize(Object[] array, int s) {
			if (s <= 0 || array.length < s) {
				this.array = new Object[0];
				this.size = 0;
				this.membership = new int[0];
			} else {
				this.array = array;
				this.size = s;
				this.membership = new int[this.size];
				for (int i = 0; i < this.size - 1; i++)
					this.membership[i] = i;
				this.membership[this.size - 1] = this.size - 2;
			}
		}

		public Object next() {
			boolean ok = false;

			for (int i = 0; i < this.size; i++)
				if (!(this.membership[i] == this.array.length - this.size + i)) {
					ok = true;
					break;
				}
			if (!ok)
				throw (new NoSuchElementException(
						"The next method was called when no more objects remained."));
			else {
				for (int n = this.size - 1; n >= 0; n--) {
					if (this.membership[n] < this.array.length + n - this.size) {
						this.membership[n]++;

						for (int j = n + 1; j < this.size; j++)
							this.membership[j] = this.membership[j - 1] + 1;
						break;
					}
				}

				// Vector vec=new Vector();
				HashSet<Object> vec = new HashSet<Object>();
				for (int i = 0; i < this.size; i++)
					vec.add(this.array[this.membership[i]]);
				return vec;
			}
		}

		public void remove() {
			throw new UnsupportedOperationException(
					"The PowerSet class does not support the remove method.");
		}

		public boolean hasNext() {
			for (int i = 0; i < this.size; i++)
				if (!(this.membership[i] == this.array.length + i - this.size))
					return true;
			return false;
		}
	}

	public class PowerSet implements Iterator<Object> {
		private boolean[] membership;
		private Object[] array;

		public PowerSet(Object[] array) {
			this.array = array;
			this.membership = new boolean[this.array.length];
		}

		public PowerSet(Collection<Object> c) {
			this(c.toArray());
		}

		public PowerSet(Vector<Object> v) {
			this(v.toArray());
		}

		public PowerSet(ArrayList<Object> a) {
			this(a.toArray());
		}

		public Object next() {
			boolean ok = false;

			for (int i = 0; i < this.membership.length; i++)
				if (!this.membership[i]) {
					ok = true;
					break;
				}

			if (!ok)
				throw (new NoSuchElementException(
						"The next method was called when no more objects remained."));
			else {
				int n = 0;
				this.membership[0] = !this.membership[0];
				boolean carry = !this.membership[0];

				while (n + 1 < this.membership.length) {
					n++;
					if (carry) {
						this.membership[n] = !this.membership[n];
						carry = !this.membership[n];
					} else
						break;
				}

				// Vector vec=new Vector();
				HashSet<Object> vec = new HashSet<Object>();
				for (int i = 0; i < this.membership.length; i++)
					if (this.membership[i])
						vec.add(this.array[i]);
				return vec;
			}
		}

		public void remove() {

			throw new UnsupportedOperationException(
					"The PowerSet class does not support the remove method.");
		}

		public boolean hasNext() {

			for (int i = 0; i < this.membership.length; i++)

				if (!this.membership[i])

					return true;

			return false;
		}
	}

}
