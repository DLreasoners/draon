package fr.paris8.iut.info.draon;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.coode.owlapi.rdf.model.RDFNode;
import org.coode.owlapi.rdf.model.RDFResourceNode;
import org.coode.owlapi.rdf.model.RDFTranslator;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.parser.AlignmentParser;

public class LinkkeyGenerator {
	private static final String XMLNS_URI = "http://ns.inria.org/edoal/1.0/#";
	private static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	private static final String ALIGN_URI = "http://knowledgeweb.semanticweb.org/heterogeneity/alignment#";
	private static final String DC_URI = "http://purl.org/dc/elements/1.1/";
	private static final String ALEXT_URI = "http://exmo.inrialpes.fr/align/ext/1.0/";

	private Document document;
	private Element root;
	private String onto1, onto2;
	private OWLReasoner r1, r2;
	private OWLOntology owlOntology1, owlOntology2;
	private int lknb;

	/**
	 * Default constructor.
	 * 
	 * @param o1 First ontology of the Alignment.
	 * @param o2 Second ontology of the alignment.
	 */
	public LinkkeyGenerator(String o1, String o2) {
		try {
			this.lknb = 1;
			// ontologies
			try {
				OWLOntologyManager owlManager = OWLManager.createOWLOntologyManager();
				ReasonerFactory factory = new ReasonerFactory();
				Configuration configuration = new Configuration();
				configuration.throwInconsistentOntologyException = false;
				// O1
				File f1 = new File(o1);
				this.onto1 = f1.getAbsolutePath();
				owlOntology1 = owlManager.loadOntologyFromOntologyDocument(f1);
				r1 = factory.createReasoner(owlOntology1, configuration);
				// O2
				File f2 = new File(o2);
				this.onto2 = f2.getAbsolutePath();
				owlOntology2 = owlManager.loadOntologyFromOntologyDocument(f2);
				r2 = factory.createReasoner(owlOntology2, configuration);
			} catch (OWLOntologyCreationException e) {
				e.printStackTrace();
			}
			// xml
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			documentFactory.setNamespaceAware(true);
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			document = documentBuilder.newDocument();
			// namespaces
			Element rdf = document.createElement("rdf:RDF");
			rdf.setAttribute("xmlns", XMLNS_URI);
			rdf.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:rdf", RDF_URI);
			rdf.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:align", ALIGN_URI);
			rdf.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:dc", DC_URI);
			rdf.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:alext", ALEXT_URI);
			document.appendChild(rdf);
			// root
			root = document.createElement("align:Alignment");
			rdf.appendChild(root);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds the header to the RDF file.
	 * 
	 * @param p   The purpose of the file.
	 * @param lvl The level of the alignment.
	 */
	private void addHeader(String p, String lvl) {
		Element xml = document.createElement("xml");
		xml.appendChild(document.createTextNode("yes"));
		root.appendChild(xml);
		Element date = document.createElement("dc:date");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate d = LocalDate.now();
		date.appendChild(document.createTextNode(formatter.format(d)));
		root.appendChild(date);
		Element method = document.createElement("alex:method");
		method.appendChild(document.createTextNode("manual"));
		root.appendChild(method);
		Element purpose = document.createElement("alext:purpose");
		purpose.appendChild(document.createTextNode(p));
		root.appendChild(purpose);
		Element level = document.createElement("align:level");
		level.appendChild(document.createTextNode(lvl));
		root.appendChild(level);
		Element type = document.createElement("align:type");
		type.appendChild(document.createTextNode("**"));
		root.appendChild(type);
	}

	/**
	 * Adds the ontologies to the rdf file.
	 */
	private void addOntologies() {
		// onto1
		Element onto1 = document.createElement("align:onto1");
		root.appendChild(onto1);
		Element ontology1 = document.createElement("align:ontology");
		onto1.appendChild(ontology1);
		Element location1 = document.createElement("align:location");
		location1.appendChild(document.createTextNode(this.onto1));
		ontology1.appendChild(location1);
		Element formalism = document.createElement("align:formalism");
		ontology1.appendChild(formalism);
		Element f1 = document.createElement("align:Formalism");
		f1.setAttribute("align:name", "OWL1.0");
		f1.setAttribute("align:uri", "http://www.w3.org/2002/07/owl#");
		formalism.appendChild(f1);
		// onto2
		Element onto2 = document.createElement("align:onto2");
		root.appendChild(onto2);
		Element ontology2 = document.createElement("align:ontology");
		onto2.appendChild(ontology2);
		Element location2 = document.createElement("align:location");
		location2.appendChild(document.createTextNode(this.onto2));
		ontology2.appendChild(location2);
		Element formalism2 = document.createElement("align:formalism");
		ontology2.appendChild(formalism2);
		Element f2 = document.createElement("align:Formalism");
		f2.setAttribute("align:name", "OWL1.0");
		f2.setAttribute("align:uri", "http://www.w3.org/2002/07/owl#");
		formalism2.appendChild(f2);
	}

	/**
	 * Adds a link key to the rdf file.
	 * 
	 * @param c1 The first concept in the link key.
	 * @param c2 The second concept in the link key.
	 * @param p1 The list of properties linked to the first concept (sorted).
	 * @param p2 The list of properties linked to the second concept (sorted). Both
	 *           list have the same length, properties with the same identifier
	 *           should be matched.
	 */
	private void addLinkkey(String c1, String c2, List<String> p1, List<String> p2) {
		Element map = document.createElement("align:map");
		root.appendChild(map);
		Element cell = document.createElement("align:Cell");
		cell.setAttribute("rdf:about", "#linkkey" + lknb);
		lknb += 1;
		map.appendChild(cell);
		// class 1
		Element e1 = document.createElement("align:entity1");
		cell.appendChild(e1);
		Element class1 = document.createElement("Class");
		class1.setAttribute("rdf:about", c1);
		e1.appendChild(class1);
		// class 2
		Element e2 = document.createElement("align:entity2");
		cell.appendChild(e2);
		Element class2 = document.createElement("Class");
		class2.setAttribute("rdf:about", c2);
		e2.appendChild(class2);
		// relation
		Element relation = document.createElement("align:relation");
		relation.appendChild(document.createTextNode("="));
		cell.appendChild(relation);
		Element measure = document.createElement("align:measure");
		measure.setAttribute("rdf:datatype", "http://www.w3.org/2001/XMLSchema#float");
		measure.appendChild(document.createTextNode("1.0"));
		cell.appendChild(measure);
		Element linkkey = document.createElement("linkkey");
		cell.appendChild(linkkey);
		Element LK = document.createElement("Linkkey");
		linkkey.appendChild(LK);
		// properties
		for (int i = 0; i < p1.size(); i++) {
			Element binding = document.createElement("binding");
			LK.appendChild(binding);
			Element eq = document.createElement("Equals");
			binding.appendChild(eq);
			// p1
			Element prop1 = document.createElement("property1");
			eq.appendChild(prop1);
			Element rel1 = document.createElement("Relation");
			rel1.setAttribute("rdf:about", p1.get(i));
			prop1.appendChild(rel1);
			// p2
			Element prop2 = document.createElement("property2");
			eq.appendChild(prop2);
			Element rel2 = document.createElement("Relation");
			rel2.setAttribute("rdf:about", p2.get(i));
			prop2.appendChild(rel2);
		}
	}

	/**
	 * Generates a single link key naively. It takes two properties, some class in
	 * their domains, and builds the link key without any verification.
	 */
	private void naiveLKGeneration() {
		String class1 = null, class2 = null;
		List<String> props1 = new ArrayList<String>();
		List<String> props2 = new ArrayList<String>();
		Iterator<OWLObjectProperty> it1 = owlOntology1.getObjectPropertiesInSignature().iterator();
		Iterator<OWLObjectProperty> it2 = owlOntology2.getObjectPropertiesInSignature().iterator();

		// choose p1 and c1
		while (it1.hasNext()) {
			OWLObjectProperty p1 = it1.next();
			// get a valid class from the domain of p1
			Iterator<OWLClassExpression> itc1 = p1.getDomains(owlOntology1).iterator();
			while (itc1.hasNext()) {
				OWLClassExpression c1 = itc1.next();
				if (c1.isOWLNothing())
					break;
				if (!c1.isOWLThing() && !c1.isAnonymous()) {
					class1 = c1.asOWLClass().toStringID();
					props1.add(p1.toStringID());
					break;
				}
			}
			if (class1 != null)
				break;
		}
		// all domain classes are anonymous, owl:Thing or owl:Nothing
		if (class1 == null) {
			System.out.println(onto1 + " is invalid for linkkey generation.");
			System.exit(0);
		}
		// choose p2 and c2
		while (it2.hasNext()) {
			OWLObjectProperty p2 = it2.next();
			// get a valid class from the domain of p2
			Iterator<OWLClassExpression> itc2 = p2.getDomains(owlOntology2).iterator();
			while (itc2.hasNext()) {
				OWLClassExpression c2 = itc2.next();
				if (c2.isOWLNothing())
					break;
				if (!c2.isOWLThing() && !c2.isAnonymous()) {
					class2 = c2.asOWLClass().toStringID();
					props2.add(p2.toStringID());
					break;
				}
			}
			if (class2 != null)
				break;
		}
		// all domain classes are anonymous, owl:Thing or owl:Nothing
		if (class2 == null) {
			System.out.println(onto2 + " is invalid for linkkey generation.");
			System.exit(0);
		}

		addLinkkey(class1, class2, props1, props2);
	}

	/**
	 * Generates several link key using an alignment file containing equalities
	 * between individual. The method identifies the properties for which the
	 * individuals are object, then the property's subject's classes, and builds the
	 * link key from the result. Several link key can be generated, but each one
	 * will contain only one property.
	 * 
	 * @param align The path to the alignment file containing the individuals'
	 *              correspondences.
	 */
	private void validLKGeneration(String align) {
		try {
			LinkedHashMap<String, List<String>> lkO1 = new LinkedHashMap<String, List<String>>();
			LinkedHashMap<String, List<String>> lkO2 = new LinkedHashMap<String, List<String>>();
			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
			OWLDataFactory factory = manager.getOWLDataFactory();
			AlignmentParser ap = new AlignmentParser();
			Alignment alignment = ap.parse(align);
			Enumeration<Cell> em = alignment.getElements();

			// browse the alignment rules
			while (em.hasMoreElements()) {
				Cell cell = em.nextElement();
				IRI iri1 = IRI.create(cell.getObject1AsURI());
				IRI iri2 = IRI.create(cell.getObject2AsURI());
				Relation r = cell.getRelation();

				if (owlOntology1.containsIndividualInSignature(iri1)) {
					if (r instanceof EquivRelation) {
						// extract individuals equalities

						// get the classes and properties associated to th individual in O1
						RDFTranslator translator1 = new RDFTranslator(manager, owlOntology1, true) {
							protected void addTriple(RDFResourceNode subject, RDFResourceNode pred, RDFNode object) {
								if (!object.isAnonymous() && object.getIRI() != null) {
									if (object.getIRI().equals(iri1)) {
										String class1 = null;
										for (Node<OWLClass> c : r1
												.getTypes(factory.getOWLNamedIndividual(subject.getIRI()), true))
											for (OWLClass c1 : c.getEntities()) {
												class1 = c1.getIRI().toString();
												break;
											}
										if (lkO1.containsKey(class1))
											lkO1.get(class1).add(pred.getIRI().toString());
										else {
											List<String> props = new ArrayList<String>();
											props.add(pred.getIRI().toString());
											lkO1.put(class1, props);
										}
									}
								}
							}
						};
						// get the classes and properties associated to th individual in O2
						RDFTranslator translator2 = new RDFTranslator(manager, owlOntology2, false) {
							protected void addTriple(RDFResourceNode subject, RDFResourceNode pred, RDFNode object) {
								if (!object.isAnonymous() && object.getIRI() != null)
									if (object.getIRI().equals(iri2)) {
										String class2 = null;
										for (Node<OWLClass> c : r2
												.getTypes(factory.getOWLNamedIndividual(subject.getIRI()), true))
											for (OWLClass c2 : c.getEntities()) {
												class2 = c2.getIRI().toString();
												break;
											}
										if (lkO2.containsKey(class2))
											lkO2.get(class2).add(pred.getIRI().toString());
										else {
											List<String> props = new ArrayList<String>();
											props.add(pred.getIRI().toString());
											lkO2.put(class2, props);
										}
									}
							}
						};

						// process the triples
						for (OWLAxiom ax : owlOntology1.getAxioms())
							ax.accept(translator1);
						for (OWLAxiom ax : owlOntology2.getAxioms())
							ax.accept(translator2);

					}
				}
			}

			// iterate over the results to generate the link keys
			Iterator<String> it1 = lkO1.keySet().iterator();
			Iterator<String> it2 = lkO2.keySet().iterator();
			while (it1.hasNext() && it2.hasNext()) {
				String key1 = it1.next();
				String key2 = it2.next();
				addLinkkey(key1, key2, lkO1.get(key1), lkO2.get(key2));
			}
		} catch (AlignmentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generates link keys using the validLKGeneration method.
	 * 
	 * @param output The path to the output file to be generated.
	 * @param align  The path to the alignment file containing the individuals'
	 *               correspondences.
	 */
	public void generateAlignmentWithLK(String output, String align) {
		addHeader("example", "2EDOAL");
		addOntologies();
		validLKGeneration(new File(align).toURI().toString());
		writeAlignment(new File(output));
	}

	/**
	 * Generates the output rdf file containing the link keys.
	 * 
	 * @param output The path to the rdf file.
	 */
	private void writeAlignment(File output) {
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(output);
			// StreamResult result = new StreamResult(System.out)
			transformer.transform(domSource, streamResult);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generates link keys using the naive method.
	 * 
	 * @param output The path to the rdf file.
	 */
	public void generateAlignmentWithLK(String output) {
		addHeader("example", "2EDOAL");
		addOntologies();
		naiveLKGeneration();
		writeAlignment(new File(output));
	}

	// arg[0]: ontology 1, arg[1]: ontology 2, arg[2]: output file, arg[3]:
	// alignment
	public static void main(String[] args) {
		String filePath1, filePath2, filePath3, filePath4;
		if (args.length != 4) {
			filePath1 = "ISWC2019/dataset/FMA.owl";
			filePath2 = "ISWC2019/dataset/NCI.owl";
			filePath3 = "ISWC2019/dataset/testLK.rdf";
			filePath4 = "ISWC2019/dataset/FMA-NCI.rdf";
		} else {
			filePath1 = args[0];
			filePath2 = args[1];
			filePath3 = args[2];
			filePath4 = args[3];
		}
		LinkkeyGenerator generator = new LinkkeyGenerator(filePath1, filePath2);
		// generator.generateAlignmentWithLK(filePath3);
		generator.generateAlignmentWithLK(filePath3, filePath4);
		System.out.println("Done creating XML File");
	}

}
