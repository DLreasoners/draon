package fr.paris8.iut.info.draon;

import java.net.InetAddress;
import java.net.UnknownHostException;

import fr.paris8.iut.info.draon.localnode.LocalReasonerNode;

public class LocalMain {
	public static void main(String[] args) throws UnknownHostException {
		String hostName = InetAddress.getLocalHost().getHostName();
		String myIp = null;
		InetAddress addrs[] = InetAddress.getAllByName(hostName);
 
		for (InetAddress addr : addrs) {
			if (!addr.isLoopbackAddress() && addr.isSiteLocalAddress()) {
				myIp = addr.getHostAddress();
			}
		}

		System.out.println("IP:" + myIp);
		LocalReasonerNode iddlServeur = new LocalReasonerNode("http://" + myIp, 8081);
		iddlServeur.run();
	}

}
