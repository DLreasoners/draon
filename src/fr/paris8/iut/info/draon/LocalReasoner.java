/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2009-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package fr.paris8.iut.info.draon;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.util.AbstractSet;
import java.util.Enumeration;
import java.util.Collections;

//import org.semanticweb.kaon2.api.formatting.OntologyFileFormat;
//import org.semanticweb.owl.align.Alignment;
//import fr.inrialpes.exmo.align.impl.BasicAlignment;
//import org.semanticweb.owl.align.AlignmentProcess;
//import org.semanticweb.owl.align.AlignmentVisitor;
//import org.semanticweb.owl.align.Parameters;

//import fr.inrialpes.exmo.align.impl.BasicParameters;
//import fr.inrialpes.exmo.ontowrap.OntologyCache;
//import fr.inrialpes.exmo.align.parser.AlignmentParser;
//import fr.inrialpes.exmo.align.impl.renderer.HTMLRendererVisitor;
//import fr.inrialpes.exmo.align.impl.renderer.OWLAxiomsRendererVisitor;
//import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;
//import fr.inrialpes.exmo.align.impl.OWLAPIAlignment;


//import org.mindswap.pellet.owlapi.Reasoner;
//import org.semanticweb.owlapi.reasoner.Reasoner;
//import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLPropertyAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.util.OWLOntologyChangeVisitorAdapter;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.paris8.iut.info.draon.DRAONException;
import fr.paris8.iut.info.draon.AlignServer;
import fr.paris8.iut.info.draon.DRAON;
import fr.paris8.iut.info.draon.DRAONServices;
import fr.paris8.iut.info.draon.DRAONRuntimeException;

public class LocalReasoner {
	public static OWLOntologyManager manager = null;
	// public static PelletReasoner reasoner = null; //JE2010 This should be
	// Reasoner
	public static Reasoner reasoner = null; // Added for HermiT
	public String uriRoot = "http://alignapi.gforge.inria.fr/tutorial/myOnto.owl";

	public LocalReasoner() {
		manager = OWLManager.createOWLOntologyManager();
	}

	public LocalReasoner(OWLOntology onto) {
		manager = OWLManager.createOWLOntologyManager();
		loadOntology(onto);
	}

	public void loadOntology(OWLOntology onto) {
		reasoner = new Reasoner(onto); // HermiT
		manager = OWLManager.createOWLOntologyManager();
	}

	public void dispose() {
		if (reasoner != null) {
			reasoner.dispose();
			reasoner = null;
		}
	}

	public boolean isConsistent() {
		return reasoner.isConsistent();
	}

	public boolean isEntailed(OWLAxiom ax) {
		return reasoner.isEntailed(ax);
	}

	public OWLOntology createOntologyWithAxioms(String ontoUri,
			Set<OWLAxiom> axioms) throws DRAONRuntimeException {
		manager = OWLManager.createOWLOntologyManager();

		OWLOntology onto = null;

		try {
			String url = new File(ontoUri).getAbsolutePath();
			url ="file:"+url;
			onto = manager.loadOntology(IRI.create(url));
		} catch (OWLOntologyCreationException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException("DRAON: cannot load ontology : "
					+ ontoUri);
		}
		if (axioms == null || axioms.size() == 0)
			return onto;
 
		for (OWLAxiom ax : axioms) {
			if (ax != null) {
				AddAxiom addaxiom = new AddAxiom(onto, ax);
				manager.applyChange(addaxiom);
			}
		}
		return onto;
	}

	public boolean isConsistent(String ontoUri, Set<OWLAxiom> axioms)
			throws DRAONRuntimeException {
		boolean response = false;
		OWLOntology onto = createOntologyWithAxioms(ontoUri, axioms);
		loadOntology(onto);
		response = reasoner.isConsistent();
		dispose();
		return response;
	}

	// when ontologies oi are merged, oi:Thing -> global:Thing, oi:Nothing ->
	// global:Nothing
	public boolean isConsistent(Set<String> ontoUris, OWLOntology onto)
			throws DRAONRuntimeException {
		boolean response = false;

		// rethink if there is a unloadOntology
		// If it were not there is a OWLOntologyAlreadyExistsException
		/*manager = OWLManager.createOWLOntologyManager();

		OWLOntology tOnto = null;
		File tmpOnto = null;
		try {
			tmpOnto = File.createTempFile(
					"tmpOntology" + DRAONServices.getGlobalOntoName(),
					"owl");
			tmpOnto.deleteOnExit();
		} catch (IOException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data :" + ex);
		}

		try {
			tOnto = manager.createOntology(IRI.create("file:"
					+ tmpOnto.getAbsolutePath()));

		} catch (OWLOntologyCreationException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data :" + ex);
		}

		Set<OWLAxiom> alignAxioms = new HashSet<OWLAxiom>();
		if (alignOnto != null) {
			alignAxioms = (Set<OWLAxiom>) alignOnto.getAxioms();
			for (OWLAxiom ax : alignAxioms) {
				AddAxiom addaxiom = new AddAxiom(tOnto, ax);
				manager.applyChange(addaxiom);
			}
		}

		for (int i = 0; i < ontoUris.size(); i++) 
		{
			OWLOntology onto = null;
			try {
				String url = new File(ontoUris.toArray()[i].toString()).getAbsolutePath();
				url ="file:"+url;
				onto = manager.loadOntology(IRI.create(url));
			} catch (OWLOntologyCreationException ex) {
				// ex.printStackTrace();
				throw new DRAONRuntimeException(
						"DRAON: cannot temporary data :" + ex);
			}

			Set<OWLAxiom> axioms = (Set<OWLAxiom>) onto.getAxioms();

			for (OWLAxiom ax : axioms) {
					AddAxiom addaxiom = new AddAxiom(tOnto, ax);
					manager.applyChange(addaxiom);
			}
		}*/
		loadOntology(onto);
		System.out.println("Reasoning ... on size = "+onto.getAxiomCount());
		response = isConsistent();
		dispose();
		return response;
	}

	// if axiom==null then isEntailed => isConsistent
	public boolean isEntailed(String ontoUri, Set<OWLAxiom> axioms,
			OWLAxiom axiom) throws DRAONRuntimeException {
		boolean response = false;

		OWLOntology onto = createOntologyWithAxioms(ontoUri, axioms);

		loadOntology(onto);
		if (axiom == null)
			response = reasoner.isConsistent();
		else if (reasoner.isConsistent())
			response = reasoner.isEntailed(axiom);
		dispose();

		return response;
	}

	// it must create a temporary ontology
	// if axiom==null then isEntailed => isConsistent
	public boolean isEntailed(OWLOntology onto, Set<OWLAxiom> axioms,
			OWLAxiom axiom) throws DRAONRuntimeException {
		boolean response = false;

		OWLOntology tOnto = null;
		File tmpOnto = null;
		try {
			tmpOnto = File.createTempFile(
					"tmpOntology" + DRAONServices.getGlobalOntoName(),
					"owl");
			tmpOnto.deleteOnExit();
		} catch (IOException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data :" + ex);
		}

		try {
			tOnto = manager.createOntology(IRI.create("file:"
					+ tmpOnto.getAbsolutePath()));

		} catch (OWLOntologyCreationException ex) {
			// ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data :" + ex);
		}

		Set<OWLAxiom> tAxioms = (Set<OWLAxiom>) onto.getAxioms();
		for (OWLAxiom ax : tAxioms) {
			AddAxiom addaxiom = new AddAxiom(tOnto, ax);
			manager.applyChange(addaxiom);
		}

		for (OWLAxiom ax : axioms) {
			if (ax != null) {
				AddAxiom addaxiom = new AddAxiom(tOnto, ax);
				manager.applyChange(addaxiom);
			}
		}

		loadOntology(tOnto);
		if (axiom == null)
			response = reasoner.isConsistent();
		else if (reasoner.isConsistent())
			response = reasoner.isEntailed(axiom);

		dispose();
		return response;
	}

	/* check if each axiom of entailedAlign is entailed from */
	// This method can be reduced
	public boolean isEntailed(Set<String> ontoUris, OWLOntology alignOnto,
			OWLOntology entailedAlign) throws DRAONRuntimeException {

		// rethink if there is a unloadOntology
		// If it were not there is a OWLOntologyAlreadyExistsException
		manager = OWLManager.createOWLOntologyManager();
		boolean response = false;

		Set<OWLAxiom> axiomSet = new HashSet<OWLAxiom>();
		if (alignOnto != null) {
			axiomSet = alignOnto.getAxioms();

		} else {
			String aUriStr = DRAON.uriRoot
					+ DRAONServices.getGlobalOntoName() + ".owl";
			// String aUriStr = "file:"+tmpOnto.getAbsolutePath() +
			// DRAONReasonerImpl.getGlobalOntoName() +".owl";
			IRI aUri = IRI.create(aUriStr);

			try {
				alignOnto = manager.createOntology(aUri);
			} catch (OWLOntologyCreationException ex) {
				ex.printStackTrace();
				throw new DRAONRuntimeException(
						"DRAON: cannot create temporary data.");
			}
		}

		// try {

		File tmpOnto = null;
		try {
			tmpOnto = File.createTempFile(
					"tmpOntology" + DRAONServices.getGlobalOntoName(),
					"owl");
			tmpOnto.deleteOnExit();
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data.");
		}

		OWLOntology onto1 = null;
		try {
			onto1 = manager.createOntology(IRI.create("file:"
					+ tmpOnto.getAbsolutePath()));
		} catch (OWLOntologyCreationException ex) {
			ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data.");
		}

		// System.out.println("Number of axioms of align : "+ axiomSet.size());
		// add the axioms of the alignment to the global ontology
		for (OWLAxiom ax : axiomSet) {
			AddAxiom addaxiom = new AddAxiom(onto1, ax);
			manager.applyChange(addaxiom);
		}

		// add all axioms of ontologies to the global ontology
		for (int i = 0; i < ontoUris.size(); i++) {
			// System.out.println( " uris=" + uris.get(i) );
			OWLOntology onto = null;
			try {
				onto = manager.loadOntology(IRI.create((String) ontoUris
						.toArray()[i]));
			} catch (OWLOntologyCreationException ex) {
				ex.printStackTrace();
				throw new DRAONRuntimeException("DRAON: cannot load ontology");
			}
			Set<OWLAxiom> axioms = (Set<OWLAxiom>) onto.getAxioms();

			for (OWLAxiom ax : axioms) {
				AddAxiom addaxiom = new AddAxiom(onto1, ax);
				manager.applyChange(addaxiom);
			}

		}

		loadOntology(onto1);

		Set<OWLAxiom> entailedAxs = entailedAlign.getAxioms();
		response = true;
		// it answers "true" if all "ax" are entailed
		// it answers "false" if all "ax" are supported and there is axiom "ax"
		// that is not entailed
		for (OWLAxiom ax : entailedAxs) {
			if (!reasoner.isEntailed(ax)) {
				response = false;
			}
		}
		dispose();
		return response;
	}

	// This method can be reduced
	public boolean isEntailed(Set<String> ontoUris, OWLOntology alignOnto,
			OWLAxiom cons) throws DRAONRuntimeException {
		// rethink if there is a unloadOntology
		// If it were not there is a OWLOntologyAlreadyExistsException
		manager = OWLManager.createOWLOntologyManager();
		boolean response = false;

		Set<OWLAxiom> axiomSet = new HashSet<OWLAxiom>();
		if (alignOnto != null) {
			axiomSet = alignOnto.getAxioms();

		} else {
			// OWLDataFactoryImpl facto = new OWLDataFactoryImpl();
			String aUriStr = DRAON.uriRoot
					+ DRAONServices.getGlobalOntoName() + ".owl";
			IRI aUri = IRI.create(aUriStr);
			try {
				alignOnto = manager.createOntology(aUri);
			} catch (OWLOntologyCreationException ex) {
				ex.printStackTrace();
				throw new DRAONRuntimeException(
						"DRAON: cannot create temporary data.");
			}

		}
		File tmpOnto = null;
		try {
			tmpOnto = File.createTempFile(
					"tmpOntology" + DRAONServices.getGlobalOntoName(),
					"owl");
			tmpOnto.deleteOnExit();
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data.");
		}

		OWLOntology onto1 = null;
		try {
			onto1 = manager.createOntology(IRI.create("file:"
					+ tmpOnto.getAbsolutePath()));
		} catch (OWLOntologyCreationException ex) {
			ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data.");
		}

		for (OWLAxiom ax : axiomSet) {
			AddAxiom addaxiom = new AddAxiom(onto1, ax);
			manager.applyChange(addaxiom);
		}

		for (int i = 0; i < ontoUris.size(); i++) {
			// System.out.println( " uris=" + uris.get(i) );
			OWLOntology onto = null;
			try {
				onto = manager.loadOntology(IRI.create((String) ontoUris
						.toArray()[i]));
			} catch (OWLOntologyCreationException ex) {
				ex.printStackTrace();
				throw new DRAONRuntimeException(
						"DRAON: cannot create temporary data.");
			}
			Set<OWLAxiom> axioms = (Set<OWLAxiom>) onto.getAxioms();

			for (OWLAxiom ax : axioms) {
				AddAxiom addaxiom = new AddAxiom(onto1, ax);
				manager.applyChange(addaxiom);
			}
		}

		loadOntology(onto1);
		response = reasoner.isEntailed(cons);
		dispose();
		return response;
	}

	public boolean isConsistent(Set<String> ontoUris, Set<String> alignUris)
			throws DRAONRuntimeException {
		// rethink if there is a unloadOntology
		// If it were not there is a OWLOntologyAlreadyExistsException
		manager = OWLManager.createOWLOntologyManager();
		boolean response = false;
		String defaultHost = "aserv.inrialpes.fr";
		String defaultPort = "80";
		int idOwl = 0;

		AlignServer onlineAlign = new AlignServer(defaultPort, defaultHost);
		Set<OWLAxiom> axiomSet = new HashSet<OWLAxiom>();
		OWLOntology ontox = null;
		Set<OWLAxiom> axs = new HashSet<OWLAxiom>();

		for (int i = 0; i < alignUris.size(); i++) {

			String owl = onlineAlign.getOWLAlignment((String) alignUris
					.toArray()[i]);

			String owlPath = new Integer(idOwl++) + ".owl";
			try {
				File owlFile = new File(owlPath);

				FileWriter out = new FileWriter(owlFile);

				out.write(owl);
				out.flush();
				out.close();

				ontox = manager.loadOntologyFromOntologyDocument(IRI
						.create("file:" + owlFile.getAbsolutePath()));
				axs = ontox.getAxioms();
				for (OWLAxiom ax : axs)
					if (!axiomSet.contains(ax))
						axiomSet.add(ax);
				// axiomSet.addAll( axs );

				// for ( OWLAxiom ax : axs ) System.out.println(
				// " axioms from aligns =" + ax.toString() );

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			;
		}

		// try
		// {
		File tmpOnto = null;
		try {
			tmpOnto = File.createTempFile(
					"tmpOntology" + DRAONServices.getGlobalOntoName(),
					"owl");
			tmpOnto.deleteOnExit();
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data.");
		}

		OWLOntology onto1 = null;
		try {
			onto1 = manager.createOntology(IRI.create("file:"
					+ tmpOnto.getAbsolutePath()));

		} catch (OWLOntologyCreationException ex) {
			ex.printStackTrace();
			throw new DRAONRuntimeException(
					"DRAON: cannot create temporary data.");
		}

		for (OWLAxiom ax : axiomSet) {

			AddAxiom addaxiom = new AddAxiom(onto1, ax);
			// addaxiom.accept(xc);
			manager.applyChange(addaxiom);
		}

		for (int i = 0; i < ontoUris.size(); i++) {
			OWLOntology onto = null;
			try {
				onto = manager.loadOntology(IRI.create((String) ontoUris
						.toArray()[i]));
			} catch (OWLOntologyCreationException ex) {
				ex.printStackTrace();
				throw new DRAONRuntimeException("DRAON: cannot load ontology");
			}

			Set<OWLAxiom> axioms = (Set<OWLAxiom>) onto.getAxioms();

			for (OWLAxiom ax : axioms) {
				AddAxiom addaxiom = new AddAxiom(onto1, ax);

				manager.applyChange(addaxiom);
			}

		}

		// java.util.Set<OWLAxiom> axiomSet1 = onto1.getAxioms();
		// System.out.println("Number of axioms of the system : "+
		// axiomSet1.size());

		// reasoner.loadOntology( onto1 );
		loadOntology(onto1);

		response = reasoner.isConsistent();
		dispose();
		return response;

		// } catch ( Exception ex ) { ex.printStackTrace();};

		// return response;
	}

}
