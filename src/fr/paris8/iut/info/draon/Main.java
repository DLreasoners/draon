package fr.paris8.iut.info.draon;

import org.semanticweb.HermiT.Reasoner;
import fr.paris8.iut.info.draon.conf.Semantics;
import fr.paris8.iut.info.draon.DRAONException;
import fr.paris8.iut.info.draon.DRAONRuntimeException;
import fr.paris8.iut.info.draon.DRAON;
import fr.paris8.iut.info.draon.localnode.LocalNode;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.Map;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;
import java.util.Iterator;
import java.util.Vector;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import java.util.Enumeration;

import java.io.File;
import java.io.IOException;

import org.semanticweb.owl.align.AlignmentVisitor;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.align.impl.renderer.OWLAxiomsRendererVisitor;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.Ontology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

import fr.inrialpes.exmo.align.parser.AlignmentParser;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.URIAlignment;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.HasInstanceRelation;
import fr.inrialpes.exmo.align.impl.rel.InstanceOfRelation;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.AddOntologyAnnotation;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLAnonymousIndividual;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLInverseFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.OWLOntologyChangeListener;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.RemoveAxiom;
import org.semanticweb.owlapi.model.RemoveOntologyAnnotation;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.SWRLRule;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import org.semanticweb.owlapi.reasoner.FreshEntitiesException;
import org.semanticweb.owlapi.reasoner.FreshEntityPolicy;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.IndividualNodeSetPolicy;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.ReasonerInterruptedException;
import org.semanticweb.owlapi.reasoner.ReasonerProgressMonitor;
import org.semanticweb.owlapi.reasoner.TimeOutException;
import org.semanticweb.owlapi.reasoner.impl.OWLClassNode;
import org.semanticweb.owlapi.reasoner.impl.OWLClassNodeSet;
import org.semanticweb.owlapi.reasoner.impl.OWLDataPropertyNode;
import org.semanticweb.owlapi.reasoner.impl.OWLDataPropertyNodeSet;
import org.semanticweb.owlapi.reasoner.impl.OWLNamedIndividualNode;
import org.semanticweb.owlapi.reasoner.impl.OWLNamedIndividualNodeSet;
import org.semanticweb.owlapi.reasoner.impl.OWLObjectPropertyNode;
import org.semanticweb.owlapi.reasoner.impl.OWLObjectPropertyNodeSet;
import org.semanticweb.owlapi.util.Version;
import org.semanticweb.owlapi.vocab.PrefixOWLOntologyFormat;

import org.semanticweb.owlapi.reasoner.UnsupportedEntailmentTypeException;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.FreshEntitiesException;
import org.semanticweb.owlapi.reasoner.ReasonerInterruptedException;
import org.semanticweb.owlapi.reasoner.TimeOutException;
import org.semanticweb.owlapi.reasoner.ClassExpressionNotInProfileException;
import org.semanticweb.owlapi.reasoner.AxiomNotInProfileException;

import org.semanticweb.owlapi.reasoner.OWLReasoner;

public class Main {

	public static OWLDataFactory owlfactory = null;
	public static OntologyFactory ontoFact = null;
	private static boolean LOGS = true;

	public static void main(String[] args) {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

		owlfactory = manager.getOWLDataFactory();
		ontoFact = OntologyFactory.getFactory();
		//DRAON reasoner = null;
		/*
		 * //check DL alignment entailment reasoner = new DRAON();
		 * reasoner.addOntology(URI.create(
		 * "http://www.inrialpes.fr/exmo/people/cleduc/geography.owl"));
		 * reasoner.addOntology(URI.create(
		 * "http://www.inrialpes.fr/exmo/people/cleduc/geopolitics.owl"));
		 * 
		 * reasoner.setSemantics(Semantics.DL); Alignment al = null; try {
		 * AlignmentParser ap = new AlignmentParser( 0 ); ap.setEmbedded(true);
		 * al = ap.parse("http://aserv.inrialpes.fr/alid/1232381048317/996"); }
		 * catch ( Exception ex ) { ex.printStackTrace(); };
		 * 
		 * reasoner.addAlignment(al);
		 * 
		 * if (reasoner.isEntailed(al)) {
		 * 
		 * System.out.println(
		 * "Is http://aserv.inrialpes.fr/alid/1232381048317/996 implied from the system  ? YES"
		 * ); } else System.out.println(
		 * "Is http://aserv.inrialpes.fr/alid/1232381048317/996 implied from the system  ? NO"
		 * );
		 * 
		 * //check axiom entailment
		 */

		/*OWLClass cls1 = owlfactory
				.getOWLClass(IRI
						.create("http://www.inrialpes.fr/exmo/people/cleduc/geography.owl#SouthAmericanRegion"));
		OWLClass cls2 = owlfactory
				.getOWLClass(IRI
						.create("http://www.inrialpes.fr/exmo/people/cleduc/geography.owl#Region"));
		OWLSubClassOfAxiom ax = owlfactory.getOWLSubClassOfAxiom(cls1, cls2);*/
		
		/*
		 * reasoner = new DRAON(); reasoner.addOntology(URI.create(
		 * "http://www.inrialpes.fr/exmo/people/cleduc/geography.owl"));
		 * reasoner.addOntology(URI.create(
		 * "http://www.inrialpes.fr/exmo/people/cleduc/geopolitics.owl"));
		 * reasoner.setSemantics(Semantics.DL); if (reasoner.isEntailed( ax )) {
		 * 
		 * System.out.println(
		 * "Is SouthAmericanRegion subsumed by Region implied from the system  ? YES"
		 * ); } else System.out.println(
		 * "Is SouthAmericanRegion subsumed by Region implied from the system  ? NO"
		 * );
		 */

		// check DRAON consistency

		/*OWLOntology onto1 = null;
		String ontologyFilePath = "file:"
				+ new File("distrib/edu.mit.visus.bibtex-1.owl")
						.getAbsolutePath();
		try {
			if (LOGS)
				System.out.println("Checking consistency of "
						+ ontologyFilePath);
			onto1 = manager.loadOntology(IRI.create(ontologyFilePath));
		} catch (OWLOntologyCreationException ex) {
			ex.printStackTrace();
		}*/

		/*DRAON rea = new DRAON(onto1);

		if (rea.localReasoner.isConsistent())
			if (LOGS)
				System.err.println("Is Consistent bibtex? YES");
			else if (LOGS)
				System.err.println("Is Consistent bibtex? NO");
		*/
		
		/*
		 * long startgalen = java.lang.System.currentTimeMillis();
		 * OWLNamedIndividual ind = owlfactory.getOWLNamedIndividual(
		 * IRI.create(
		 * "http://alignapi.gforge.inria.fr/tutorial/edu.mit.visus.bibtex.owl#x1"
		 * ) ); OWLClass cl1 = owlfactory.getOWLClass ( IRI.create(
		 * "http://alignapi.gforge.inria.fr/tutorial/edu.mit.visus.bibtex.owl#Book"
		 * ) ); OWLClass cl2 = owlfactory.getOWLClass ( IRI.create(
		 * "http://alignapi.gforge.inria.fr/tutorial/edu.mit.visus.bibtex.owl#Nothing"
		 * ) ); OWLClassAssertionAxiom ax1 =
		 * owlfactory.getOWLClassAssertionAxiom (cl1, ind); Set<OWLAxiom> se =
		 * new HashSet<OWLAxiom>(); se.add(ax1); if
		 * (rea.localReasoner.isEntailed( owlfactory.getOWLSubClassOfAxiom (cl1,
		 * cl2) ) ) { //if (rea.localReasoner.isEntailed( ax1 ) ) {
		 * System.err.println("Is ENtailed ? YES"); } else
		 * System.err.println("Is Entailed ? NO");
		 * 
		 * if (rea.localReasoner.isConsistent(
		 * "http://alignapi.gforge.inria.fr/tutorial/edu.mit.visus.bibtex.owl",
		 * se ) ) { System.err.println("Is Consistent ? YES"); } else
		 * System.err.println("Is Consistent ? NO");
		 * 
		 * System.out.println("Consummed VA = " +
		 * (java.lang.System.currentTimeMillis() - startgalen) ) ;
		 */
		long startdl = java.lang.System.currentTimeMillis();
		Alignment al1 = null;
		//ObjectAlignment al = null;
		String alignmentFilePath = "file:"+ new File(args[0]).getAbsolutePath();
		try 
		{
			AlignmentParser ap = new AlignmentParser();
			if (LOGS)
				System.out.println("Parsing alignment file :" + alignmentFilePath);
			al1 = ap.parse(alignmentFilePath);
			//al = ObjectAlignment.toObjectAlignment((URIAlignment) al1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		//long startdl = java.lang.System.currentTimeMillis();
		DRAON reasonerDL = null;
		Set<URI> als = new HashSet<URI>();
		try {
			if (LOGS)
			System.out.println("Checking consistency of file "+ alignmentFilePath);
			als.add(URI.create(alignmentFilePath));
			reasonerDL = new DRAON(als);
		} catch (DRAONException ex) {
			ex.printStackTrace();
		}
		
		if (reasonerDL.isConsistent()) {
			if (LOGS)
				System.out.println("Is the DL consistent ? YES");
		} else if (LOGS)
			System.out.println("Is the DL consistent ? NO");
		if (LOGS)
			System.out.println("Consummed DL = "
					+ (java.lang.System.currentTimeMillis() - startdl));
 
		 
		/*DRAON reasonerDRAON = null;
		List<Alignment> aligns = new ArrayList<Alignment>();
		aligns.add(al1);

		try {
			reasonerDRAON = new DRAON(als, Semantics.IDDL);
		} catch (DRAONException ex) {
			ex.printStackTrace();
		}

		long startdraon = java.lang.System.currentTimeMillis();

		if (reasonerDRAON.isConsistent())
			if (LOGS)
				System.err.println("Is the IDDL consistent ? YES");
			else if (LOGS)
				System.err.println("Is the IDDL consistent ? NO");
		if (LOGS)
			System.out.println("Consummed IDDL = "
					+ (java.lang.System.currentTimeMillis() - startdraon));

		DRAON reasonerDRAONd = null;
		//reasoner url, ontology url, port
		LocalNode node1 = new LocalNode(args[0], args[1], 8080);
		LocalNode node2 = new LocalNode(args[2], args[3], 8081);
		Vector<LocalNode> nodes = new Vector<LocalNode>();
		nodes.add(node1);
		nodes.add(node2);
		long startdraondistri = java.lang.System.currentTimeMillis();
		try {
			reasonerDRAONd = new DRAON(als, nodes, Semantics.IDDL);
		} catch (DRAONException ex) {
			ex.printStackTrace();
		}

		if (reasonerDRAONd.isConsistent()) {
			System.err.println("Is the distributed IDDL consistent ? YES");
		} else
			System.err.println("Is the distributed IDDL consistent ? NO");
		System.out.println("\nConsummed distributed IDDL = "
				+ (java.lang.System.currentTimeMillis() - startdraondistri));*/

		/*try {
			if (reasonerDRAONd.isEntailed(al1)) {
				System.err.println("Is the al1 is entailed ? YES");
			} else
				System.err.println("Is the al1 is entailed ? NO");
		} catch (DRAONException ex) {
			ex.printStackTrace();
		}*/
	}

	// JE : In fact, it should be possible to do all EDOAL
	public static OWLAxiom correspToAxiom(Alignment align, Cell corresp)
			throws DRAONException 
	{
		// LoadedOntology onto1 = al.ontology1();
		// LoadedOntology onto2 = al.ontology2();
		Object e1 = corresp.getObject1();
		Object e2 = corresp.getObject2();
		Relation r = corresp.getRelation();
		BasicAlignment al = (BasicAlignment) align;
		// retrieve entity1 and entity2
		// create the axiom in function of their labels

		try {
			LoadedOntology onto1 = ontoFact.loadOntology(al
					.getOntologyObject1().getFile());
			LoadedOntology onto2 = ontoFact.loadOntology(al
					.getOntologyObject2().getFile());
			// if ( onto1.isClass( e1 ) && onto2.isClass( e2 ))

			// System.out.println("class, class" + corresp );

			if (onto1.isClass(e1)) {
				if (onto2.isClass(e2)) {
					OWLClass entity1 = owlfactory.getOWLClass(IRI.create(onto1
							.getEntityURI(e1)));
					OWLClass entity2 = owlfactory.getOWLClass(IRI.create(onto2
							.getEntityURI(e2)));
					if (r instanceof EquivRelation) {
						return owlfactory.getOWLEquivalentClassesAxiom(entity1,
								entity2);
					} else if (r instanceof SubsumeRelation) {
						return owlfactory.getOWLSubClassOfAxiom(entity2,
								entity1);
					} else if (r instanceof SubsumedRelation) {
						return owlfactory.getOWLSubClassOfAxiom(entity1,
								entity2);
					} else if (r instanceof IncompatRelation) {

						return owlfactory.getOWLDisjointClassesAxiom(entity1,
								entity2);
					}
				} else if (onto2.isIndividual(e2)
						&& (r instanceof HasInstanceRelation)) {
					return owlfactory.getOWLClassAssertionAxiom(owlfactory
							.getOWLClass(IRI.create(onto1.getEntityURI(e1))),
							owlfactory.getOWLNamedIndividual(IRI.create(onto2
									.getEntityURI(e2))));
				}
			} else if (onto1.isDataProperty(e1) && onto2.isDataProperty(e2)) {
				OWLDataProperty entity1 = owlfactory.getOWLDataProperty(IRI
						.create(onto1.getEntityURI(e1)));
				OWLDataProperty entity2 = owlfactory.getOWLDataProperty(IRI
						.create(onto2.getEntityURI(e2)));
				if (r instanceof EquivRelation) {
					return owlfactory.getOWLEquivalentDataPropertiesAxiom(
							entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					return owlfactory.getOWLSubDataPropertyOfAxiom(entity2,
							entity1);
				} else if (r instanceof SubsumedRelation) {
					return owlfactory.getOWLSubDataPropertyOfAxiom(entity1,
							entity2);
				} else if (r instanceof IncompatRelation) {
					return owlfactory.getOWLDisjointDataPropertiesAxiom(
							entity1, entity2);
				}
			} else if (onto1.isObjectProperty(e1) && onto2.isObjectProperty(e2)) {
				OWLObjectProperty entity1 = owlfactory.getOWLObjectProperty(IRI
						.create(onto1.getEntityURI(e1)));
				OWLObjectProperty entity2 = owlfactory.getOWLObjectProperty(IRI
						.create(onto2.getEntityURI(e2)));
				if (r instanceof EquivRelation) {
					return owlfactory.getOWLEquivalentObjectPropertiesAxiom(
							entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					return owlfactory.getOWLSubObjectPropertyOfAxiom(entity2,
							entity1);
				} else if (r instanceof SubsumedRelation) {
					return owlfactory.getOWLSubObjectPropertyOfAxiom(entity1,
							entity2);
				} else if (r instanceof IncompatRelation) {
					return owlfactory.getOWLDisjointObjectPropertiesAxiom(
							entity1, entity2);
				}
			} else if (onto1.isIndividual(e1)) {
				if (onto2.isIndividual(e2)) {
					OWLIndividual entity1 = owlfactory
							.getOWLNamedIndividual(IRI.create(onto1
									.getEntityURI(e1)));
					OWLIndividual entity2 = owlfactory
							.getOWLNamedIndividual(IRI.create(onto2
									.getEntityURI(e2)));
					if (r instanceof EquivRelation) {
						return owlfactory.getOWLSameIndividualAxiom(entity1,
								entity2);
					} else if (r instanceof IncompatRelation) {
						return owlfactory.getOWLDifferentIndividualsAxiom(
								entity1, entity2);
					}
				} else if (onto2.isClass(e2)
						&& (r instanceof InstanceOfRelation)) {
					return owlfactory.getOWLClassAssertionAxiom(owlfactory
							.getOWLClass(IRI.create(onto2.getEntityURI(e2))),
							owlfactory.getOWLNamedIndividual(IRI.create(onto1
									.getEntityURI(e1))));
				}
			}
		} catch (OntowrapException owex) {
			throw new DRAONException("Error interpreting URI " + owex);
		}
		throw new DRAONException("Cannot convert correspondence " + corresp);
	}

}
