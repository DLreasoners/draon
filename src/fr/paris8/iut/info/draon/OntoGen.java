package fr.paris8.iut.info.draon;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.coode.owlapi.rdf.model.RDFNode;
import org.coode.owlapi.rdf.model.RDFResourceNode;
import org.coode.owlapi.rdf.model.RDFTranslator;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.parser.AlignmentParser;
import uk.ac.manchester.cs.owl.owlapi.OWLDataFactoryImpl;

public class OntoGen {
	 
	
	private OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	private OWLDataFactory fact = new OWLDataFactoryImpl(); 
	 
	public OntoGen(String inOnto, String data, String outOnto, int flag) 
	{
	   switch(flag)
	   {
	   case  0 :
		   AddAssertions(inOnto, data, outOnto);
		   break;
	   case  1 :
		   //AddAxioms(inOnto, data, outOnto);
		   break;
	   case  2 : 
		   extract(inOnto, outOnto);
		   break;
		   
	   }
	}
	/*
	 * Extracts "name" from predicat(#WWW, name)
	 */
	public void extract(String inf, String outf)
	{
	   Set<String> ss = new HashSet<String>();
	   File file=new File(inf);    
	   File of=new File(outf);    
		try {
		  FileReader fr=new FileReader(file);   //reads the file  
		  BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream  
		  PrintWriter bw=new PrintWriter(of); 
		  String line=null;
		  while((line=br.readLine())!=null)
		  {
			String[] inpar = line.split(",");
			if( !ss.contains(inpar[2].split("\\)")[0].toUpperCase().replace(" ", "_")) )
			{
				 bw.println(inpar[2].split("\\)")[0].toUpperCase().replace(" ", "_") );
			     ss.add(inpar[2].split("\\)")[0].toUpperCase().replace(" ", "_"));
			}
		  }
		  br.close();
		  bw.close();
		}catch(IOException e) 
		{
		  e.printStackTrace();
		}
	}
    /*
     * dataFile is a set of lines of the form 
     */
	public void AddAssertions(String inOnto, String dataFile, String outOnto)
	{
		OWLOntology onto=null;
		try 
		  {		 	 
			onto = manager.loadOntologyFromOntologyDocument(new File(inOnto));
		  } catch (Exception e) 
		  {
			e.printStackTrace();
		  }
		File file=new File(dataFile);    
		try {
		  FileReader fr=new FileReader(file);   //reads the file  
		  BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream  
		  System.out.println("file=" + dataFile);
		  String line=null;
		  while((line=br.readLine())!=null)
		  {
			String[] inpar = line.split(",");
			System.out.println("assert=" + inpar[0]);
			System.out.println("pred=" + inpar[0].split("\\(")[0] + ", rep=" +inpar[0].split("\\(")[1] );
			System.out.println("id=" + inpar[1]);
			
			System.out.println("Name=" + inpar[2].split("\\)")[0]);
            OWLObjectProperty pred = fact.getOWLObjectProperty(IRI.create(onto.getOntologyID().getOntologyIRI() +"/" + inpar[0].split("\\(")[0]));
            OWLIndividual op1 = fact.getOWLNamedIndividual(IRI.create(onto.getOntologyID().getOntologyIRI() +"/" + inpar[0].split("\\(")[1]+inpar[1].replace(" ", "_")));
            OWLClass cls = fact.getOWLClass(IRI.create(onto.getOntologyID().getOntologyIRI() +"/" + 
                    inpar[2].split("\\)")[0])); //.toUpperCase().replace(" ", "_")
            OWLClassExpression op2 = fact.getOWLObjectSomeValuesFrom(pred, cls);
            OWLClassAssertionAxiom ax =  fact.getOWLClassAssertionAxiom(op2, op1);
            
			AddAxiom addaxiom = new AddAxiom(onto,ax); 
			manager.applyChange(addaxiom);	
			OWLClassExpression c1 = null;
			if(inpar[0].split("\\(")[0].equals("medicatedWith") || inpar[0].split("\\(")[0].equals("allergicTo"))
            {
				c1 = fact.getOWLClass(IRI.create(onto.getOntologyID().getOntologyIRI() +"/Drug"));      
            	//pred = fact.getOWLObjectProperty(IRI.create(onto.getOntologyID().getOntologyIRI() +"/" + "medicatedWith"));
            	//op2 = fact.getOWLObjectAllValuesFrom(pred, fact.getOWLObjectComplementOf(cls));
            		
            } else
            	c1 = fact.getOWLClass(IRI.create(onto.getOntologyID().getOntologyIRI() +"/Disease"));
            	
			OWLSubClassOfAxiom ax1 =  fact.getOWLSubClassOfAxiom(cls, c1);
        	addaxiom = new AddAxiom(onto,ax1); 
        	manager.applyChange(addaxiom);
			/*Scanner scan = new Scanner(System.in);
	        String text= scan.nextLine();
	        */
		  }
		  br.close();
		  
		  try {
				manager.saveOntology(onto);
		  } catch (OWLOntologyStorageException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		  }
		  
		}catch(IOException e) 
		{
		  e.printStackTrace();
		}
	}
	 
	// arg[0]: input ontology, arg[1] : input data, arg[2]: output ontology
	public static void main(String[] args) 
	{
	  //0 : add assertions;  1: add axioms
	  //OntoGen generator = new OntoGen(args[0], args[1], args[2], 0);	 
	  //System.out.println("Done!");
	  OWLOntologyManager manager= OWLManager.createOWLOntologyManager();
	   
	  OWLOntology onto1 = null;
	  try {
		   onto1= manager.loadOntologyFromOntologyDocument(new File("ontology-2020-08-11.owl"));
		   //onto1 = manager.loadOntology( new File("ontology-2020-08-11.owl"));
		   
	  } catch (OWLException ex) { 
				ex.printStackTrace();
	  }
	  Set<OWLClass> cls = onto1.getClassesInSignature();
	  System.out.println ("Size="+ cls.size());
	  
	}

}
