package fr.paris8.iut.info.draon;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.semanticweb.owlapi.model.OWLSameIndividualAxiom;
import org.semanticweb.owlapi.owllink.OWLlinkHTTPXMLReasonerFactory;
import org.semanticweb.owlapi.owllink.OWLlinkReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import com.google.common.collect.SetMultimap;

import fr.inrialpes.exmo.align.impl.edoal.ClassConstruction;
import fr.inrialpes.exmo.align.impl.edoal.ClassDomainRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassExpression;
import fr.inrialpes.exmo.align.impl.edoal.ClassOccurenceRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassTypeRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassValueRestriction;
import fr.inrialpes.exmo.align.impl.edoal.Comparator;
import fr.inrialpes.exmo.align.impl.edoal.EDOALCell;
import fr.inrialpes.exmo.align.impl.edoal.Id;
import fr.inrialpes.exmo.align.impl.edoal.InstanceExpression;
import fr.inrialpes.exmo.align.impl.edoal.InstanceId;
import fr.inrialpes.exmo.align.impl.edoal.Linkkey;
import fr.inrialpes.exmo.align.impl.edoal.LinkkeyBinding;
import fr.inrialpes.exmo.align.impl.edoal.PathExpression;
import fr.inrialpes.exmo.align.impl.edoal.PropertyConstruction;
import fr.inrialpes.exmo.align.impl.edoal.PropertyRestriction;
import fr.inrialpes.exmo.align.impl.edoal.RelationConstruction;
import fr.inrialpes.exmo.align.impl.edoal.RelationRestriction;
import fr.inrialpes.exmo.align.impl.edoal.Value;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.align.parser.AlignmentParser;
import fr.inrialpes.exmo.align.parser.SyntaxElement.Constructor;
import uk.ac.manchester.cs.owl.owlapi.OWLClassAssertionImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLClassImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataAllValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataExactCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataHasValueImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataMaxCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataMinCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataSomeValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDatatypeImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLLiteralImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectAllValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectComplementOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectExactCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectHasValueImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectIntersectionOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectInverseOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectMaxCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectMinCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectSomeValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectUnionOfImpl;

/*
 * All methods to distribute the propagation, and gather the results. Must NOT be distributed, but
 * deals with the distribution instead.
 */
public class Propagate {
	private DistributedPropagate prop1, prop2;
	private OWLOntology o1, o2;
	private OWLOntology o12;
	private Alignment alignment;
	private OWLDataFactory owlfactory;
	// A reasoner to deal with alignment ontology
	private OWLReasoner reasoner,reasoner1,reasoner2;
	// Unsatisfiable concepts in onto1
	private Set<OWLClassExpression> unsats_1;
	// Unsatisfiable concepts in onto2
	private Set<OWLClassExpression> unsats_2;
	// equalities
	private Set<OWLAxiom> equalities;
	// link keys
	private Set<EDOALCell> linkkeyCells;
	private Vector<Worker> workers_1;
	private Vector<Worker> workers_2;
	private String url_1 = null;
	private String url_2 = null;
	//Rethink of dynamic ports
	private int port_1 = 8080;
	private int port_2 = 8081;
	//private URL urlReasoner1=null;
	//private URL urlReasoner2=null;
	//private OWLlinkReasonerConfiguration reasonerConfiguration1;
	//private OWLlinkReasonerConfiguration reasonerConfiguration2;
	//private OWLlinkHTTPXMLReasonerFactory factory;
	private final int THREAD_MAX = 1000;
	public Propagate(String url1, String url2, String onto1, String onto2, String align) 
	{
		try {
			linkkeyCells = new HashSet<EDOALCell>();
			// alignment
			AlignmentParser ap = new AlignmentParser();
			alignment = ap.parse(align);
			// alignment = rdfparser.parse(align);
			OWLOntologyManager m = OWLManager.createOWLOntologyManager();
			owlfactory = m.getOWLDataFactory();
			// This ontology contains non-key correspondences
			o12 = m.createOntology(IRI.create("http://liasd.example"));
			reasoner = new Reasoner(o12); // HermiT
			unsats_1 = new HashSet<OWLClassExpression>();
			unsats_2 = new HashSet<OWLClassExpression>();
			equalities = new HashSet<OWLAxiom>();
			// ontology 1
			o1 = m.loadOntologyFromOntologyDocument(new File(onto1));
			if( url1.equals("localhost") )
				url_1 =  "127.0.0.1";
			else
				url_1 = url1;
			
			// replace localhost by ip address of a server
			prop1 = new DistributedPropagate(onto1, "http://" + url1, port_1);
			// ontology 2
			o2 = m.loadOntologyFromOntologyDocument(new File(onto2));
			if( url2.equals("localhost") )
				url_2 =  "127.0.0.1";
			else
				url_2 = url2;
			
			// replace localhost by ip address of another server
			prop2 = new DistributedPropagate(onto2, "http://" + url2, port_2);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		} catch (AlignmentException e) {
			e.printStackTrace();
		}
		/*try {
			   urlReasoner1 =  new URL(url_1 + ":" + port_1);
		} catch (MalformedURLException e) {
			  e.printStackTrace();
		}
		try {
			   urlReasoner2 =  new URL(url_2 + ":" + port_2);
		} catch (MalformedURLException e) {
			  e.printStackTrace();
		}*/
	    //factory = new OWLlinkHTTPXMLReasonerFactory();
		//reasonerConfiguration1 = new OWLlinkReasonerConfiguration(urlReasoner1);
		//reasoner1 = factory.createNonBufferingReasoner(o1, reasonerConfiguration1);
		//reasonerConfiguration2 = new OWLlinkReasonerConfiguration(urlReasoner2);
		//reasoner2 = factory.createNonBufferingReasoner(o2, reasonerConfiguration2);
		workers_1 = new Vector<Worker>();
		//Worker1.init(o1);
		workers_2 = new Vector<Worker>();
		//Worker2.init(o2);
	}

	public Set<OWLClassExpression> getUnsatClasses_1() {
		return unsats_1;
	}

	public Set<OWLClassExpression> getUnsatClasses_2() {
		return unsats_2;
	}

	public OWLOntology getOntology_1() {
		return o1;
	}

	public OWLOntology getOntology_2() {
		return o2;
	}

	public OWLOntology getOntology_12() {
		return o12;
	}

	public Set<OWLAxiom> getEqualities() {
		return equalities;
	}

	public Set<EDOALCell> getLinkkeyCells() {
		return linkkeyCells;
	}

	public Vector<Worker> getWorkers_1()
	{
		return workers_1;
	}
	public Vector<Worker> getWorkers_2()
	{
		return workers_2;
	}
	/**
	 * Processes the alignment. according to each alignment rule, extracts
	 * information from both ontologies in order to produce an approximated
	 * consistency evaluation.
	 */
	public void processAlignment() {
		int x=0;
		try {
			Enumeration<Cell> em = alignment.getElements();
			while (em.hasMoreElements()) {
				System.out.println("while ..."+(x++));
				Cell cell = em.nextElement();
				if ((cell instanceof EDOALCell) && ((EDOALCell) cell).linkkeys() != null) {
					// System.out.println("one link key extracted");
					linkkeyCells.add((EDOALCell) cell);
					OWLClassExpression owlClass1 = getCellClass1AsOWL(((EDOALCell) cell));
					OWLClassExpression owlClass2 = getCellClass2AsOWL(((EDOALCell) cell));
					for (Linkkey lk : ((EDOALCell) cell).linkkeys()) {
						Set<OWLPropertyExpression> roles1 = new HashSet<OWLPropertyExpression>();
						Set<OWLPropertyExpression> roles2 = new HashSet<OWLPropertyExpression>();
						for (LinkkeyBinding binding : lk.bindings()) {
							roles1.add(getOWLPathExpression1(binding));
							roles2.add(getOWLPathExpression2(binding));
						}
						prop1.processLinkKey(roles1, owlClass1);
						prop2.processLinkKey(roles2, owlClass2);
					}
				} else {
					//System.out.println("an ordinary cell extracted");
					if(cell.getObject1AsURI()==null || cell.getObject2AsURI()==null)
						continue;
					IRI iri1 = IRI.create(cell.getObject1AsURI());
					IRI iri2 = IRI.create(cell.getObject2AsURI());
					Relation r = cell.getRelation();
					if (o1.containsClassInSignature(iri1)) {
						// iri1 is class of O1
						OWLClass entity1 = owlfactory.getOWLClass(iri1);
						OWLClass entity2 = owlfactory.getOWLClass(iri2);
						if (r instanceof EquivRelation) {
							o12.getOWLOntologyManager().addAxiom(o12,
									owlfactory.getOWLEquivalentClassesAxiom(entity1, entity2));
							//System.out.println("OK X2 ..."); System.out.flush();
							if( !unsats_1.contains(entity1) )
							{
								//System.out.println("OK 2 ..."); 
								//OWLlinkReasonerConfiguration rc1 =  new OWLlinkReasonerConfiguration(urlReasoner1);
								//OWLReasoner r1 = factory.createNonBufferingReasoner(o1, rc1);
							    Worker w1 = new Worker(o1, entity1, null, url_1, port_1);
							    workers_1.add(w1);
							    //System.out.println("OK 22 ="+workers_1.size()); 
						    }
							if( !unsats_2.contains(entity2) )
							{
								//OWLlinkReasonerConfiguration rc2 =  new OWLlinkReasonerConfiguration(urlReasoner2);
								//OWLReasoner r2 = factory.createNonBufferingReasoner(o2, rc2);
								Worker w2 = new Worker(o2, entity2, null, url_2, port_2);
							    workers_2.add(w2);   
							    //System.out.println("OK 22 ="+workers_2.size()); 
						    }
							
							/*if (!prop1.checkSatisfiability(entity1))
								unsats_1.add(entity1);
							if (!prop2.checkSatisfiability(entity2))
								unsats_2.add(entity2);*/
						} else if (r instanceof SubsumeRelation) {
							o12.getOWLOntologyManager().addAxiom(o12,
									owlfactory.getOWLSubClassOfAxiom(entity2, entity1));
							if( !unsats_1.contains(entity1) )
							{
								//System.out.println("OK 3 ...");
								//OWLlinkReasonerConfiguration rc1 =  new OWLlinkReasonerConfiguration(urlReasoner1);
								//OWLReasoner r1 = factory.createNonBufferingReasoner(o1, rc1);
							    Worker w1 = new Worker(o1,  entity1, null, url_1, port_1);
							    workers_1.add(w1);   
							    //System.out.println("OK 33 ="+workers_1.size()); 
						    }
							
							/*if (!prop1.checkSatisfiability(entity1))
								unsats_1.add(entity1);*/
						} else if (r instanceof SubsumedRelation) {
							System.out.println("OK 4 ...");
							o12.getOWLOntologyManager().addAxiom(o12,
									owlfactory.getOWLSubClassOfAxiom(entity1, entity2));
							if( !unsats_2.contains(entity2) )
							{
								//OWLlinkReasonerConfiguration rc2 =  new OWLlinkReasonerConfiguration(urlReasoner2);
								//OWLReasoner r2 = factory.createNonBufferingReasoner(o2, rc2);
								Worker w2 = new Worker(o2, entity2, null, url_2, port_2);
							    workers_2.add(w2);   
							    //System.out.println("OK 44 ="+ workers_2.size());
						    }
							
							/*if (!prop2.checkSatisfiability(entity2))
								unsats_2.add(entity2);*/
						}
					}
					if (o1.containsIndividualInSignature(iri1)) {
						// both are individuals
						if (r instanceof EquivRelation) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(owlfactory.getOWLNamedIndividual(iri1),
									owlfactory.getOWLNamedIndividual(iri2));
							o12.getOWLOntologyManager().addAxiom(o12, ax);
							equalities.add(ax);
						}
					}
				}
				//collect results from threads
				int j1=0;
				//if(workers_1.size() >= THREAD_MAX)
				for(Worker  w : workers_1) 
				{
					//System.out.print("join 1"+(j1++));
					try {
					   w.join();
					}catch (InterruptedException e){
						e.printStackTrace();
					}
					if( w.getOWLClass()!=null && !w.getResult() )
						unsats_1.add(w.getOWLClass());
				}
				workers_1.clear();
				int j2=0;
				//if(workers_2.size() >= THREAD_MAX)
				for(Worker  w : workers_2) 
				{
					//System.out.print("join 2"+(j2++));
					try {
						   w.join();
					}catch (InterruptedException e){
							e.printStackTrace();
					}
					if( w.getOWLClass()!=null && !w.getResult()  )
						unsats_2.add(w.getOWLClass());
				}
				workers_2.clear();
			}//while
		} catch (AlignmentException  e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Checks the consistency of the network in the current situation.
	 * 
	 * @return true if the network is consistent, false otherwise.
	 */
	public boolean checkLocalConsistencies() 
	{
		Worker w1 = new Worker(o1, null, null, url_1, port_1);
		Worker w2 = new Worker(o2, null, null, url_2, port_2);
		try {
			   w1.join();
			   w2.join();
		}catch (InterruptedException e){
				e.printStackTrace();
		}
		reasoner.flush();
		if ( w1.getResult() && w2.getResult()  && reasoner.isConsistent() )
			return true;
		else
			return false;
	}

	/**
	 * @author Jerome Euzenat
	 */
	private OWLClassExpression getCellClass1AsOWL(EDOALCell c) throws AlignmentException {
		return getClassExpressionAsOWL(c.getObject1(), o1);
	}

	/**
	 * @author Jerome Euzenat
	 */
	private OWLClassExpression getCellClass2AsOWL(EDOALCell c) throws AlignmentException {
		return getClassExpressionAsOWL(c.getObject2(), o2);
	}

	/**
	 * @author Jerome Euzenat
	 */
	private OWLClassExpression getClassExpressionAsOWL(Object expr, OWLOntology o) throws AlignmentException {
		if (expr instanceof Id) {
			return (OWLClass) getEntity(o, ((Id) expr).getURI());
		} else if (expr instanceof ClassConstruction) {
			Set<OWLClassExpression> components = new HashSet<OWLClassExpression>();
			for (final ClassExpression ce : ((ClassConstruction) expr).getComponents()) {
				components.add(getClassExpressionAsOWL(ce, o));
			}
			switch (((ClassConstruction) expr).getOperator()) {
			case AND: // TODO: creating a new OWLClassImpl may not be the best
				if (components.size() == 0)
					return new OWLClassImpl(IRI.create("http://www.w3.org/2002/07/owl#Thing"));
				else
					return new OWLObjectIntersectionOfImpl(components);
			case OR:
				if (components.size() == 0)
					return new OWLClassImpl(IRI.create("http://www.w3.org/2002/07/owl#Nohing"));
				else
					return new OWLObjectUnionOfImpl(components);
			case NOT:
				if (components.size() == 0)
					throw new AlignmentException("Complement constructor cannot be empty");
				else
					return new OWLObjectComplementOfImpl(components.iterator().next());
			default:
				throw new AlignmentException("Unknown class constructor : " + ((ClassConstruction) expr).getOperator());
			}
			/// Below depend on object/attribute...
		} else {
			OWLPropertyExpression pe = getPathExpressionAsOWL(((ClassRestriction) expr).getRestrictionPath(), o);
			if (expr instanceof ClassValueRestriction) {
				if (pe instanceof OWLObjectPropertyExpression) {
					OWLIndividual ind = getInstanceExpressionAsOWL(
							(InstanceExpression) ((ClassValueRestriction) expr).getValue(), o);
					return new OWLObjectHasValueImpl((OWLObjectPropertyExpression) pe, ind);
				} else {
					Value val = (Value) ((ClassValueRestriction) expr).getValue();
					OWLDatatype type = null;
					if (val.getType() != null)
						type = new OWLDatatypeImpl(IRI.create(val.getType()));
					OWLLiteral lit = new OWLLiteralImpl(val.getValue(), val.getLang(), type);
					return new OWLDataHasValueImpl((OWLDataPropertyExpression) pe, lit);
				}
			} else if (expr instanceof ClassTypeRestriction) {
				// In principle, it only works for ALL
				// TODO: Only works if the result of getType is a URI/IRI
				OWLDataRange type = new OWLDatatypeImpl(IRI.create(((ClassTypeRestriction) expr).getType().getType()));
				if (((ClassTypeRestriction) expr).isUniversal()) {
					return new OWLDataAllValuesFromImpl((OWLDataPropertyExpression) pe, type);
				} else {
					return new OWLDataSomeValuesFromImpl((OWLDataPropertyExpression) pe, type);
				}
			} else if (expr instanceof ClassDomainRestriction) {
				OWLClassExpression domain = getClassExpressionAsOWL(((ClassDomainRestriction) expr).getDomain(), o);
				if (((ClassDomainRestriction) expr).isUniversal()) {
					return new OWLObjectAllValuesFromImpl((OWLObjectPropertyExpression) pe, domain);
				} else {
					return new OWLObjectSomeValuesFromImpl((OWLObjectPropertyExpression) pe, domain);
				}
			} else if (expr instanceof ClassOccurenceRestriction) {
				// TODO: the interface needs a OWLClassExpression filler @nonnull
				int card = ((ClassOccurenceRestriction) expr).getOccurence();
				if (pe instanceof OWLObjectPropertyExpression) {
					Comparator comp = ((ClassOccurenceRestriction) expr).getComparator();
					if (comp == Comparator.EQUAL)
						return new OWLObjectExactCardinalityImpl((OWLObjectPropertyExpression) pe, card, null);
					else if (comp == Comparator.LOWER)
						return new OWLObjectMinCardinalityImpl((OWLObjectPropertyExpression) pe, card, null);
					else if (comp == Comparator.GREATER)
						return new OWLObjectMaxCardinalityImpl((OWLObjectPropertyExpression) pe, card, null);
				} else {
					Comparator comp = ((ClassOccurenceRestriction) expr).getComparator();
					if (comp == Comparator.EQUAL)
						return new OWLDataExactCardinalityImpl((OWLDataPropertyExpression) pe, card, null);
					else if (comp == Comparator.LOWER)
						return new OWLDataMinCardinalityImpl((OWLDataPropertyExpression) pe, card, null);
					else if (comp == Comparator.GREATER)
						return new OWLDataMaxCardinalityImpl((OWLDataPropertyExpression) pe, card, null);
				}
			}
			throw new AlignmentException("Not yet implemented: dereferencing non URI classes");
		}
	}

	/**
	 * OWL, and in particular OWL 2, does not allow for more Relation
	 * (ObjectProperty) and Property (DataProperty) constructor than owl:inverseOf
	 * 
	 * It is thus imposible to transcribe our and, or and not constructors. As well
	 * as most of our restrictions...
	 *
	 * Moreover, they have no constructor for the symmetric, transitive and
	 * reflexive closure and the compositional closure (or composition) can only be
	 * obtained by defining a property subsumed by this closure through an axiom. It
	 * is also possible to rewrite the reflexive closures as axioms as well. But the
	 * transitive closure can only be obtained through subsumption.
	 * 
	 * We will thus log the non transcription
	 * 
	 * @author Jerome Euzenat
	 */
	private OWLPropertyExpression getPathExpressionAsOWL(PathExpression expr, OWLOntology o) throws AlignmentException {
		if (expr instanceof Id) {
			return (OWLProperty) getEntity(o, ((Id) expr).getURI());
		} else if (expr instanceof PropertyConstruction) {
			// in the visitor I tried with propertyChainAxioms, but this is likely illegal
			// (and more useful for relation)
			throw new AlignmentException("Cannot transcribe property construction in OWL");
		} else if (expr instanceof RelationConstruction) {
			final Constructor op = ((RelationConstruction) expr).getOperator();
			if (op == Constructor.INVERSE) {
				return new OWLObjectInverseOfImpl((OWLObjectPropertyExpression) getPathExpressionAsOWL(
						((RelationConstruction) expr).getComponents().iterator().next(), o));
			} else {
				// in the visitor I tried with propertyChainAxioms, but this is likely illegal
				// (and more useful for relation)
				throw new AlignmentException("Cannot transcribe relation construction " + op + " in OWL");
			}
		} else if (expr instanceof PropertyRestriction) {
			// in the visitor I tried with propertyChainAxioms, but this is likely illegal
			throw new AlignmentException("Cannot transcribe property restrictions in OWL");
		} else if (expr instanceof RelationRestriction) {
			throw new AlignmentException("Cannot transcribe relation restrictions in OWL");
		} else
			throw new AlignmentException("Unknown property/relation expression " + expr);
	}

	/**
	 * @author Jerome Euzenat
	 */
	private OWLIndividual getInstanceExpressionAsOWL(InstanceExpression expr, OWLOntology o) throws AlignmentException {
		if (expr instanceof Id) {
			return (OWLIndividual) getEntity(o, ((Id) expr).getURI());
		} else
			throw new AlignmentException("Not yet implemented: dereferencing non URI paths");
	}

	/**
	 * @author Jerome Euzenat
	 */
	private OWLEntity getEntity(OWLOntology o, URI u) {

		Set<OWLEntity> entities = o.getEntitiesInSignature(IRI.create(u));
		if (entities.isEmpty()) {
			OWLClass c = owlfactory.getOWLClass(IRI.create(u));
			if (o1.isDeclared(c) || o2.isDeclared(c))
				return c;
			else
				return null;
		}
		return entities.iterator().next();
	}

	/**
	 * @author Jerome Euzenat
	 */
	private OWLPropertyExpression getOWLPathExpression1(LinkkeyBinding bd) throws AlignmentException {
		return getPathExpressionAsOWL(bd.getExpression1(), o1);
	}

	/**
	 * @author Jerome Euzenat
	 */
	private OWLPropertyExpression getOWLPathExpression2(LinkkeyBinding bd) throws AlignmentException {
		return getPathExpressionAsOWL(bd.getExpression2(), o2);
	}

	/**
	 * Propagates the unsatisfiability of concepts in both ontologies, and the
	 * subsumption of classes in the alignment. The propagation is processed until
	 * the network (both ontologies and their alignment) is stable.
	 * 
	 * @return true if the result after propagation is consistent, false otherwise.
	 */
	public boolean propagateUnsatisfiability() {
		if ((!prop1.isConsistent()) || (!prop2.isConsistent()) || (!reasoner.isConsistent()))
			return false;

		int size = prop1.ontoSize() + prop2.ontoSize() + o12.getAxiomCount();
		propagateSubsetEq();
		propagateUnsats();

		while (size != (prop1.ontoSize() + prop2.ontoSize() + o12.getAxiomCount())) {
			reasoner.flush();
			prop1.refreshReasoner();
			prop2.refreshReasoner();
			if ((!prop1.isConsistent()) || (!prop2.isConsistent()) || (!reasoner.isConsistent()))
				return false;
			size = prop1.ontoSize() + prop2.ontoSize() + o12.getAxiomCount();
			propagateSubsetEq();
			propagateUnsats();
		}
		return true;
	}

	/**
	 * Propagates the unsatisfiability of concepts in the ontologies.
	 */
	private void propagateUnsats() {
		for (OWLClassExpression c1 : unsats_1) {
			for (Node<OWLClass> node : reasoner.getSubClasses(c1, false)) {
				for (OWLClass c2 : node) {
					OWLAxiom ax = owlfactory.getOWLSubClassOfAxiom(c2, owlfactory.getOWLNothing());
					prop2.addNewAxiom(ax);
				}
			}
		}
		for (OWLClassExpression c2 : unsats_2) {
			for (Node<OWLClass> node : reasoner.getSubClasses(c2, false)) {
				for (OWLClass c1 : node) {
					OWLAxiom ax = owlfactory.getOWLSubClassOfAxiom(c1, owlfactory.getOWLNothing());
					prop1.addNewAxiom(ax);
				}
			}
		}
	}

	/**
	 * Propagates the subsumption of classes in the alignment.
	 */
	private void propagateSubsetEq() {
		for (OWLClass a : o12.getClassesInSignature()) {
			if (o1.containsClassInSignature(a.getIRI())) {
				for (Node<OWLClass> n : reasoner.getSuperClasses(a, false))
					for (OWLClass b : n) {
						for (Node<OWLClass> n2 : prop2.getSupClassesOf(b))
							for (OWLClass c : n2) {
								for (Node<OWLClass> n3 : reasoner.getSuperClasses(c, false))
									for (OWLClass d : n3) {
										if (!a.isBottomEntity() && !a.isTopEntity() && !c.isBottomEntity()
												&& !c.isTopEntity()) {
											o12.getOWLOntologyManager().addAxiom(o12,
													owlfactory.getOWLSubClassOfAxiom(a, c));
										}
										if (!b.isBottomEntity() && !b.isTopEntity() && !d.isBottomEntity()
												&& !d.isTopEntity()) {
											o12.getOWLOntologyManager().addAxiom(o12,
													owlfactory.getOWLSubClassOfAxiom(b, d));
										}
									}
							}
					}
				for (Node<OWLClass> n : reasoner.getSubClasses(a, false))
					for (OWLClass b : n) {
						for (Node<OWLClass> n2 : prop1.getSupClassesOf(a))
							for (OWLClass c : n2) {
								for (Node<OWLClass> n3 : reasoner.getSuperClasses(c, false))
									for (OWLClass d : n3) {
										if (!a.isBottomEntity() && !a.isTopEntity() && !d.isBottomEntity()
												&& !d.isTopEntity()) {
											o12.getOWLOntologyManager().addAxiom(o12,
													owlfactory.getOWLSubClassOfAxiom(a, d));
										}
										if (!b.isBottomEntity() && !b.isTopEntity() && !c.isBottomEntity()
												&& !c.isTopEntity()) {
											o12.getOWLOntologyManager().addAxiom(o12,
													owlfactory.getOWLSubClassOfAxiom(b, c));
										}
									}
							}
					}
			}
		}
	}

	/**
	 * Propagates the equalities of individuals and the axioms entailed by the
	 * alignment. The propagation is processed until the network (both ontologies
	 * and their alignment) is stable.
	 * 
	 * @return true if the result after propagation is consistent, false otherwise.
	 */
	public boolean propagateEqualities() {
		if ((!prop1.isConsistent()) || (!prop2.isConsistent()) || (!reasoner.isConsistent()))
			return false;

		int size = prop1.ontoSize() + prop2.ontoSize() + o12.getAxiomCount();
		propagateTransitivity();
		propagateLinkkeys();
		while (size != (prop1.ontoSize() + prop2.ontoSize() + o12.getAxiomCount())) {
			reasoner.flush();
			prop1.refreshReasoner();
			prop2.refreshReasoner();
			if ((!prop1.isConsistent()) || (!prop2.isConsistent()) || (!reasoner.isConsistent()))
				return false;
			size = prop1.ontoSize() + prop2.ontoSize() + o12.getAxiomCount();
			propagateTransitivity();
			propagateLinkkeys();
		}
		return true;
	}

	/**
	 * Propagates the individual equivalences obtained through transitivity.
	 */
	private void propagateTransitivity() {
		Set<OWLAxiom> newAxioms = new HashSet<OWLAxiom>();
		for (OWLAxiom axiom : equalities) {
			Set<OWLNamedIndividual> inds = ((OWLSameIndividualAxiom) axiom).getIndividualsInSignature();
			Iterator<OWLNamedIndividual> it = inds.iterator();
			OWLNamedIndividual a1i = it.next();
			OWLNamedIndividual a1j = it.next();

			if (o1.containsIndividualInSignature(a1i.getIRI())) {
				for (OWLNamedIndividual a2i : prop1.getSameIndividuals(a1i))
					for (OWLNamedIndividual a2j : reasoner.getSameIndividuals(a2i)) {
						if (!a1i.equals(a2j) && o2.containsIndividualInSignature(a2j.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a1i, a2j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a2i.equals(a1j) && o1.containsIndividualInSignature(a2i.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a2i, a1j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a1j.equals(a2j) && o2.containsIndividualInSignature(a2j.getIRI()))
							prop2.addNewAxiom(owlfactory.getOWLSameIndividualAxiom(a1j, a2j));
					}
				for (OWLNamedIndividual a2j : prop2.getSameIndividuals(a1j))
					for (OWLNamedIndividual a2i : reasoner.getSameIndividuals(a2j)) {
						if (!a1i.equals(a2j) && o2.containsIndividualInSignature(a2j.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a1i, a2j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a2i.equals(a1j) && o1.containsIndividualInSignature(a2i.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a2i, a1j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a1i.equals(a2i) && o1.containsIndividualInSignature(a2i.getIRI()))
							prop1.addNewAxiom(owlfactory.getOWLSameIndividualAxiom(a1i, a2i));
					}
			}
			if (o2.containsIndividualInSignature(a1i.getIRI())) {
				for (OWLNamedIndividual a2i : prop2.getSameIndividuals(a1i))
					for (OWLNamedIndividual a2j : reasoner.getSameIndividuals(a2i)) {
						if (!a1i.equals(a2j) && o1.containsIndividualInSignature(a2j.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a1i, a2j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a2i.equals(a1j) && o2.containsIndividualInSignature(a2i.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a2i, a1j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a1j.equals(a2j) && o1.containsIndividualInSignature(a2j.getIRI()))
							prop1.addNewAxiom(owlfactory.getOWLSameIndividualAxiom(a1j, a2j));
					}
				for (OWLNamedIndividual a2j : prop1.getSameIndividuals(a1j))
					for (OWLNamedIndividual a2i : reasoner.getSameIndividuals(a2j)) {
						if (!a1i.equals(a2j) && o1.containsIndividualInSignature(a2j.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a1i, a2j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a2i.equals(a1j) && o2.containsIndividualInSignature(a2i.getIRI())) {
							OWLAxiom ax = owlfactory.getOWLSameIndividualAxiom(a2i, a1j);
							if (!equalities.contains(ax)) {
								newAxioms.add(ax);
								o12.getOWLOntologyManager().addAxiom(o12, ax);
							}
						}
						if (!a1i.equals(a2i) && o2.containsIndividualInSignature(a2i.getIRI()))
							prop2.addNewAxiom(owlfactory.getOWLSameIndividualAxiom(a1i, a2i));
					}
			}
		}
		equalities.addAll(newAxioms);
	}

	/**
	 * Propagates the individual equivalences obtained through link key processing,
	 * as well as tautologies.
	 */
	private void propagateLinkkeys() {
		Set<OWLNamedIndividual> inds1 = o1.getIndividualsInSignature();
		Set<OWLNamedIndividual> inds2 = o2.getIndividualsInSignature();

		try {
			for (EDOALCell cell : linkkeyCells) {
				OWLClass owlClass1 = getCellClass1AsOWL(cell).asOWLClass();
				OWLClass owlClass2 = getCellClass2AsOWL(cell).asOWLClass();

				// for each individual i1 in o1
				for (OWLNamedIndividual i1 : inds1) {
					SetMultimap<OWLObjectProperty, OWLNamedIndividual> propsOfi1 = prop1.getLKPropertiesForInd(i1);
					// for each individual i2 in o2
					for (OWLNamedIndividual i2 : inds2) {
						SetMultimap<OWLObjectProperty, OWLNamedIndividual> propsOfi2 = prop2.getLKPropertiesForInd(i2);
						// browse the linkkeys
						for (Linkkey lk : ((EDOALCell) cell).linkkeys()) {
							boolean equalsInd = true;
							// browse the couples of properties
							for (LinkkeyBinding binding : lk.bindings()) {
								// we don't deal with data properties
								if (!getOWLPathExpression1(binding).isObjectPropertyExpression()) {
									equalsInd = false;
									break;
								}

								OWLObjectProperty p1 = (OWLObjectProperty) getOWLPathExpression1(binding);
								OWLObjectProperty p2 = (OWLObjectProperty) getOWLPathExpression2(binding);

								// if the individuals do not share the same object for one property, it's over
								if ((propsOfi1 == null) || (propsOfi2 == null)) {
									equalsInd = false;
									break;
								} else if ((propsOfi1.get(p1) == null) || (propsOfi2.get(p2) == null)) {
									equalsInd = false;
									break;
								}

								equalsInd = false;
								for (OWLNamedIndividual obj1 : propsOfi1.get(p1)) {
									for (OWLNamedIndividual obj2 : propsOfi2.get(p2)) {
										if (o12.containsAxiom(owlfactory.getOWLSameIndividualAxiom(obj1, obj2))) {
											equalsInd = true;
										}
									}
								}
								
								if (!equalsInd)
									break;								
							}
							if(equalsInd) {
								for(OWLNamedIndividual io1 : prop1.getSameIndividuals(i1)) {
									for(OWLNamedIndividual io2 : prop2.getSameIndividuals(i2)) {
										// add the equivalence to the alignment
										OWLAxiom axiom = owlfactory.getOWLSameIndividualAxiom(
												owlfactory.getOWLNamedIndividual(io1.getIRI()),
												owlfactory.getOWLNamedIndividual(io2.getIRI()));
										if (!o12.containsAxiom(axiom)) {
											// System.out.println("added into alignment: " + axiom);
											o12.getOWLOntologyManager().addAxiom(o12, axiom);
											alignment.addAlignCell(new InstanceId(io1.getIRI().toURI()),
													new InstanceId(io2.getIRI().toURI()), "=", 1.0);
										}
										
										// check to add a tautology to each ontology
										boolean side1 = true, side2 = true;
										Set<OWLAnnotation> annos = new HashSet<>();
										OWLClassExpression negClass1 = owlClass1.getObjectComplementOf();
										for (OWLClassAssertionAxiom a : o1.getClassAssertionAxioms(io1)) {
											if (a.getClassExpression() == owlClass1 || a.getClassExpression() == negClass1) {
												side1 = false;
												break;
											}
										}

										if (side1) {
											Set<OWLClassExpression> s1 = new HashSet<>();
											s1.add(owlClass1);
											s1.add(negClass1);
											OWLClassExpression classExp1 = new OWLObjectUnionOfImpl(s1);
											OWLClassAssertionAxiom classAxiom1 = new OWLClassAssertionImpl(io1, classExp1,
													annos);
											if (!o1.containsAxiom(classAxiom1)) {
												o1.getOWLOntologyManager().addAxiom(o1, classAxiom1);
											}
										}

										OWLClassExpression negClass2 = owlClass2.getObjectComplementOf();
										for (OWLClassAssertionAxiom a : o2.getClassAssertionAxioms(io2)) {
											if (a.getClassExpression() == owlClass2 || a.getClassExpression() == negClass2) {
												side2 = false;
												break;
											}
										}

										if (side2) {
											Set<OWLClassExpression> s2 = new HashSet<>();
											s2.add(owlClass2);
											s2.add(negClass2);
											OWLClassExpression classExp2 = new OWLObjectUnionOfImpl(s2);
											OWLClassAssertionAxiom classAxiom2 = new OWLClassAssertionImpl(io2, classExp2,
													annos);
											if (!o2.containsAxiom(classAxiom2)) {
												o2.getOWLOntologyManager().addAxiom(o2, classAxiom2);
											}
										}
									}
								}
							}

						}
					}
				}
			}
		} catch (AlignmentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Verifies the consistency of the network at a given moment.
	 * 
	 * @return true if both ontologies are consistent, false otherwise. Propagation
	 *         should be executed and completed before this method is used.
	 */
	public boolean alignmentIsConsistent() {
		// called when the alignment is stationary:
		// no need for flushing
		return prop1.isConsistent() && prop2.isConsistent() && reasoner.isConsistent();
	}

	/**
	 * Refreshes all reasoners, in order to take in account all the new axioms
	 * generated by the propagation methods.
	 */
	public void refreshPropagation() {
		prop1.refreshReasoner();
		prop2.refreshReasoner();
		reasoner.flush();
	}

}
