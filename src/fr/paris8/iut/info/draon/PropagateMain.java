package fr.paris8.iut.info.draon;

import java.io.File;

public class PropagateMain {
	// args[0] = url of server 1, args[1] = url of onto 1, args[2]= url of server 2,
	// args[3]= url of onto 2, args[4] = url of align
	public static void main(String[] args) {
		String ontoFile1 = new File(args[1]).getAbsolutePath();
		String ontoFile2 = new File(args[3]).getAbsolutePath();
		String alignmentFilePath = new File(args[4]).toURI().toString();

		//System.out.println("Checking " + alignmentFilePath);
		long startTime = java.lang.System.currentTimeMillis();
		//Propagate prop = new Propagate(args[0], args[2], ontoFile1, ontoFile2, alignmentFilePath);
		PropagateOWLLinkReasoner prop = new PropagateOWLLinkReasoner(args[0], args[2], ontoFile1, ontoFile2, alignmentFilePath);
		System.out.println("start ! " + (java.lang.System.currentTimeMillis() - startTime) / 1000);
		prop.processAlignment();
		System.out.println("Consummed  processAlignment  = " + (java.lang.System.currentTimeMillis() - startTime));
		if (prop.getUnsatClasses_1().size() == 0 && prop.getUnsatClasses_2().size() == 0
		    && prop.getEqualities().size() == 0 && prop.getLinkkeyCells().size() == 0) {
			System.out.println("Nothing to propagate !");
			if (prop.networkIsConsistent())
				System.out.println("The network is APPROX consistent! ");
			else
				System.out.println("The network is not APPROX consistent! ");
		} else {
			
			prop.propagateUnsatisfiability();
			//System.out.println("Consummed  propagateUnsatisfiability = " + (java.lang.System.currentTimeMillis() - startTime));
			//System.out.println("propagateUnsat done.");
			prop.propagateEqualities();
			//System.out.println("propagateLinkkey done.");
			//System.out.println("Consummed  propagateEqualities = " + (java.lang.System.currentTimeMillis() - startTime));
			if (prop.networkIsConsistent())
				System.out.println("The network is APPROX consistent after propagating.");
			else
				System.out.println("The network is APPROX inconsistent after propagating.");
		}
		System.out.println("Consummed APPROX = " + (java.lang.System.currentTimeMillis() - startTime));
	}

}

