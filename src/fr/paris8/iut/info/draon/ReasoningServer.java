/*
 * $Id$
 *
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.paris8.iut.info.draon;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLPropertyAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.util.OWLOntologyChangeVisitorAdapter;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntologyManager;


 
public class ReasoningServer 
{
	private ServerSocket serverSocket;	
	// Each thread may have its own ontology
	private static OWLOntology owlOntology;
	private static OWLOntologyManager owlManager;
	private static OWLReasoner reasoner;
	public ReasoningServer(String ontoFile, int port) throws IOException 
	{
		//ontoUri = ontoFile;
		serverSocket = new ServerSocket(port);
		owlManager = OWLManager.createOWLOntologyManager();
		try {
			   owlOntology = owlManager.loadOntologyFromOntologyDocument(new File(ontoFile));
		} catch (OWLOntologyCreationException e) {
			   e.printStackTrace();
		} 
		reasoner = new Reasoner(owlOntology); // HermiT
		while(true) 
		{
			System.out.println("Waiting a query ...");
			Socket comSocket = serverSocket.accept();
			System.out.println("A new query processed ...");
			ReasoningThread th = new ReasoningThread(owlOntology, reasoner, comSocket);
		    th.start();
		}
	}
}

class ReasoningThread extends Thread 
{	
	private Socket comSocket;
	ObjectOutputStream out = null;
	ObjectInputStream in = null;
	private static OWLOntology owlOntology;
	private static OWLReasoner reasoner;
	private static OWLOntologyManager owlManager;
	public ReasoningThread(OWLOntology o, OWLReasoner r, Socket communication ) 
	{
		owlManager = OWLManager.createOWLOntologyManager();
		owlOntology = o;
		reasoner = r;
		this.comSocket = communication;
		try {
			out = new ObjectOutputStream(communication.getOutputStream());
			out.flush();
			in = new ObjectInputStream(communication.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		try 
		{
			Query query = (Query)in.readObject();
			boolean r = false;
			Response  res = null;
			switch ( query.getCode() )
			{ 
			   case 0: //consistency
				   r = reasoner.isConsistent();
				   res = new Response(new Boolean(r), true);
				   out.writeObject(res);
				   out.flush();
				   break;
			   case 1: //satisfiability
				   r = reasoner.isSatisfiable( (OWLClassExpression)query.getQuery().iterator().next() ) ;
				   res = new Response(new Boolean(r), true);
				   out.writeObject(res);
				   out.flush();
				   break;
			   case 2: //entailment
				   r = reasoner.isEntailed( (OWLClassAxiom)query.getQuery().iterator().next() ) ;
				   res = new Response(new Boolean(r), true);
				   out.writeObject(res);
				   out.flush();
				   break;
			   case 3: //adding axioms
				   for(OWLObject ob : query.getQuery())
				   {
					   AddAxiom addaxiom = new AddAxiom(owlOntology, (OWLAxiom)ob);
					   owlManager.applyChange(addaxiom);
				   }
				   reasoner.flush();
				   res = new Response(new Boolean(true), true);
				   out.writeObject(res);
				   out.flush();
				   break;
			   default:
				   //Query unknown
				   res = new Response(new Boolean(false), false);
				   out.writeObject(res);
				   out.flush();
				   break;
			}	
			in.close();
			out.close();
			comSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}

