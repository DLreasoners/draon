package fr.paris8.iut.info.draon;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import fr.paris8.iut.info.draon.localnode.LocalReasonerNode;

public class ReasoningServerMain 
{
	public static void main(String[] args) throws NumberFormatException, IOException 
	{
		//args[0] : ontology, args[1] : port
		ReasoningServer server = new ReasoningServer( args[0], Integer.parseInt(args[1]) );	 
	}
}
