package fr.paris8.iut.info.draon;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.owllink.OWLlinkHTTPXMLReasonerFactory;
import org.semanticweb.owlapi.owllink.OWLlinkReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public class TestOWLLink {

	public static void main(String[] args) throws OWLOntologyCreationException, MalformedURLException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File("./ISWC2019/dataset/cmt.owl"));
		URL url = new URL("http://127.0.0.1:8080");
		OWLlinkReasonerConfiguration reasonerConfiguration = new OWLlinkReasonerConfiguration(url);
		OWLlinkHTTPXMLReasonerFactory factory = new OWLlinkHTTPXMLReasonerFactory();
		OWLReasoner reasoner = factory.createNonBufferingReasoner(ontology, reasonerConfiguration);

		for (OWLClass c : ontology.getClassesInSignature()) {
			System.out.println(c + " == ");
			System.out.println(reasoner.getInstances(c, false));
		}

	}

}
