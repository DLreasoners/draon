/*
 * $Id:  $
 *
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.paris8.iut.info.draon;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.net.UnknownHostException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.lang.Thread;
import java.lang.Exception;
import java.lang.ClassNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collections;
import java.net.ServerSocket;
import java.net.Socket;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.owllink.OWLlinkHTTPXMLReasonerFactory;
import org.semanticweb.owlapi.owllink.OWLlinkReasoner;
import org.semanticweb.owlapi.owllink.OWLlinkReasonerConfiguration;
import org.semanticweb.owlapi.owllink.builtin.requests.CreateKB;
import org.semanticweb.owlapi.owllink.builtin.requests.ReleaseKB;
import org.semanticweb.owlapi.owllink.builtin.requests.Tell;
import org.semanticweb.owlapi.owllink.builtin.response.KB;
import org.semanticweb.owlapi.owllink.builtin.response.OK;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import fr.paris8.iut.info.draon.DRAONServices;
import fr.paris8.iut.info.draon.DRAONRuntimeException;
import fr.paris8.iut.info.draon.conf.ConfFromGlobal;
import fr.paris8.iut.info.draon.conf.GlobalEntity;
import fr.paris8.iut.info.draon.conf.Result;
import fr.paris8.iut.info.draon.conf.SystemData;
/*
 * This class is similar to DistributedPropagate but multithreaded
 */
public class Worker extends Thread 
{
	//private OWLlinkReasonerConfiguration reasonerConfiguration;
	private OWLOntology owlOntology;
	//private OWLReasoner reasoner;
	//private URL url = null;
	private OWLAxiom axiom = null;
	private OWLClassExpression cls = null;
	//The result returned by this worker    
	private Boolean result =false;
	private Socket comSocket;
	ObjectOutputStream out = null;
	ObjectInputStream in = null;
	//If ax==null then this call is consistency/satisfiability checking
	public Worker(OWLOntology o, OWLClassExpression cl, OWLAxiom ax, String urlReasoner, int port) 
	{
		axiom = ax;
		cls = cl;
		owlOntology = o;
		try{
			comSocket = new Socket(urlReasoner, port);
		} catch(Exception exc){
			exc.printStackTrace();
		}
		try {
			out = new ObjectOutputStream(comSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(comSocket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		start();
	}

	public void run() 
	{
		try 
		{
			if( axiom != null )
			{
				Query q = new Query(new HashSet<OWLObject>(Collections.singleton(axiom)), 2);
			  	out.writeObject(q);
			  	out.flush();
			  	//while ( in.available() == 0 );
			  	Response r = (Response)in.readObject();
			  	//if(r==null) System.err.println("NO DATA READ===============================================");
			  	result =  r.getResult();
			} else if( cls != null )
			{
				Query q = new Query(new HashSet<OWLObject>(Collections.singleton(cls)), 1);
			  	out.writeObject(q);
			  	out.flush();
			  	//while ( in.available() == 0 );
			  	Response r = (Response)in.readObject();
			  	//if(r==null) System.err.println("NO DATA READ===============================================");
			    result =  r.getResult();
			} else
			{
				Query q = new Query(new HashSet<OWLObject>(new HashSet<OWLObject>()), 0);
				out.writeObject(q);
				out.flush();
				Response r = (Response)in.readObject();
				result =  r.getResult();
			}
			in.close();
			out.close();
			comSocket.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public OWLClassExpression getOWLClass()
	{
		return cls;
	}
	
	public OWLAxiom getAxiom()
	{
		return axiom;
	}
	
	public boolean getResult()
	{
		return result;
	}
}
