/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package fr.paris8.iut.info.draon.conf;

import java.io.Serializable;

public class Duo implements Serializable {
	String key;
	int state;

	public Duo(String k, int s) {
		key = k;
		state = s;

	}

	public String getKey() {
		return key;
	}

	public void setKey(String s) {
		key = s;
	}

	public int getState() {
		return state;
	}

	public void setState(int v) {
		state = v;
	}
}
