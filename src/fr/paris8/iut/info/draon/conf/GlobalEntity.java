/*
 * $Id$
 *
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package fr.paris8.iut.info.draon.conf;

import java.io.Serializable;

public class GlobalEntity implements Serializable {
	// "key" of maps are Local Ontology Uris, value lists contain URIs of local
	// concepts and their description
	private String name = null;
	private String uri = null;
	// if the equivalence class is unsatisfiable, this attribute is set to "1",
	// if non-empty with R (a,b) it is set to 0,
	// otherwise 2
	private int emptiness = 0; // O: unchecked, 1: checked, 2 : currently
								// checked
	private int nonemptiness = 0; // O: unchecked, 1: checked, 2 : currently
									// checked
	private int entailment = 0; // 0 : chosen, 1: dependant, 2 : entailed
	private int attr = 0; // 0: concept, 1: objectRole, 2: dataRole, 3 :
							// individual

	public GlobalEntity(String p, String u, int a) {
		name = p;
		uri = u;
		attr = a;
		emptiness = 0;
		nonemptiness = 0;
		entailment = 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		name = n;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String n) {
		uri = n;
	}

	public int getAttr() {
		return attr;
	}

	public void setAttr(int n) {
		attr = n;
	}

	public int getEmptiness() {
		return emptiness;
	}

	public void setEmptiness(int e) {
		emptiness = e;
	}

	public int getNonemptiness() {
		return nonemptiness;
	}

	public void setNonemptiness(int e) {
		nonemptiness = e;
	}

	public int getEntailment() {
		return entailment;
	}

	public void setEntailment(int e) {
		entailment = e;
	}

}
