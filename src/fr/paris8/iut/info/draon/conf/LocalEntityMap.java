/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package fr.paris8.iut.info.draon.conf;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class LocalEntityMap {
	// "key" of maps are Local Ontology Uris, value sets contain URIs of local
	// concepts and their description
	private HashMap<String, Set<String>> concepts = null;
	private HashMap<String, Set<String>> roles = null;
	private HashMap<String, Set<String>> dataRoles = null;
	private HashMap<String, Set<String>> objectRoles = null;
	private HashMap<String, Set<String>> inds = null;

	public LocalEntityMap() {
		concepts = new HashMap<String, Set<String>>(0);
		roles = new HashMap<String, Set<String>>(0);
		dataRoles = new HashMap<String, Set<String>>(0);
		objectRoles = new HashMap<String, Set<String>>(0);
		inds = new HashMap<String, Set<String>>(0);
	}

	// "ontoUri" : URI of local ontology in which the concept occurs,
	// "conceptUri" : URI of the concept,
	// "description" would be put here as a third parameter for complex concepts
	public void addConcept(String ontoUri, String conceptUri) {
		Set<String> cs = concepts.get(ontoUri);
		if (cs == null) {
			Set<String> ss = new HashSet();
			ss.add(conceptUri);
			concepts.put(ontoUri, ss);
		} else
			cs.add(conceptUri);
	}

	public void addRole(String ontoUri, String roleUri) {
		Set<String> rs = roles.get(ontoUri);

		if (rs == null) {
			Set<String> ss = new HashSet();
			ss.add(roleUri);
			roles.put(ontoUri, ss);
		} else
			rs.add(roleUri);

	}

	public void addObjectRole(String ontoUri, String roleUri) {
		Set<String> rs = objectRoles.get(ontoUri);

		if (rs == null) {
			Set<String> ss = new HashSet();
			ss.add(roleUri);
			objectRoles.put(ontoUri, ss);
		} else
			rs.add(roleUri);

	}

	public void addDataRole(String ontoUri, String roleUri) {
		Set<String> rs = dataRoles.get(ontoUri);

		if (rs == null) {
			Set<String> ss = new HashSet();
			ss.add(roleUri);
			dataRoles.put(ontoUri, ss);
		} else
			rs.add(roleUri);

	}

	public void addInd(String ontoUri, String indUri) {
		Set<String> is = inds.get(ontoUri);

		if (is == null) {
			Set<String> ss = new HashSet();
			ss.add(indUri);
			inds.put(ontoUri, ss);
		} else
			is.add(indUri);
	}

	public Set<String> getLocalUris() {
		Set<String> tmp = concepts.keySet();
		Set<String> tmp1 = roles.keySet();
		Set<String> res = tmp;
		for (String s : tmp1) {
			if (!res.contains(s))
				res.add(s);
		}
		return res;
	}

	public Set<String> getConceptsByUri(String uri) {
		Set<String> keys = concepts.keySet();
		if (!keys.contains(uri))
			return new HashSet<String>();
		else
			return concepts.get(uri);
	}

	public Set<String> getRolesByUri(String uri) {
		Set<String> keys = roles.keySet();
		if (!keys.contains(uri))
			return new HashSet<String>();
		else
			return roles.get(uri);
	}

	public Set<String> getObjectRolesByUri(String uri) {

		Set<String> keys = objectRoles.keySet();
		if (!keys.contains(uri))
			return new HashSet<String>();
		else
			return objectRoles.get(uri);
	}

	public Set<String> getDataRolesByUri(String uri) {
		Set<String> keys = dataRoles.keySet();
		if (!keys.contains(uri))
			return new HashSet<String>();
		else
			return dataRoles.get(uri);
	}

	public Set<String> getIndsByUri(String uri) {
		Set<String> keys = inds.keySet();
		if (!keys.contains(uri))
			return new HashSet<String>();
		else
			return inds.get(uri);
	}

	public boolean isObjectRole(String uri, String r) {

		Set<String> ors = objectRoles.get(uri);
		if (ors == null)
			return false;
		if (ors.contains(r))
			return true;
		else
			return false;

	}

	public boolean isDataRole(String uri, String r) {

		Set<String> ors = dataRoles.get(uri);
		if (ors == null)
			return false;
		if (ors.contains(r))
			return true;
		else
			return false;

	}

	public boolean isInd(String uri, String r) {

		Set<String> ors = inds.get(uri);
		if (ors == null)
			return false;
		if (ors.contains(r))
			return true;
		else
			return false;

	}
}
