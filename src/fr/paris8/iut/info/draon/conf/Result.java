/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package fr.paris8.iut.info.draon.conf;

import fr.paris8.iut.info.draon.conf.Duo;
import fr.paris8.iut.info.draon.conf.GlobalEntity;
import fr.paris8.iut.info.draon.conf.StateOfConf;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.Serializable;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.Collection;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Set;
import java.util.NoSuchElementException;
import java.util.Enumeration;
import javax.swing.JTextArea;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

//compressed representation of a configuration
public class Result implements Serializable {
	boolean emptyEntailed;
	boolean nonemptyEntailed;
	// boolean emptyIn;
	// boolean nonemptyIn;
	String newPosition;
	// in binary
	Vector<Duo> conf = null;

	// "binaryConf" is binary
	public Result(boolean em, boolean nonem, String newPos, Vector<Duo> c) {
		emptyEntailed = em;
		nonemptyEntailed = nonem;
		// emptyIn = emIn;
		// nonemptyIn = nonemIn;
		newPosition = newPos;
		conf = c;
		// conf = new Vector<Duo>();
		// for(int i=0; i < binaryConf.size(); i++ ) {
		// if(binaryConf.get(i).getState()==1)
		// conf.add(new Duo(binaryConf.get(i).getKey(), i));
		// }
	}

	/*
	 * //convert "comp" to global binary public static Vector<Duo> toBinary
	 * (Vector<Duo> comp) { if(comp==null || comp.size()==0) return new
	 * Vector<Duo>(0);
	 * 
	 * int max = comp.get(0).getState(); for(int i=0; i < comp.size(); i++ ) {
	 * if(comp.get(i).getState() > max) max = comp.get(i).getState(); }
	 * Vector<Integer> res = new Vector<Integer>(max+1); for(int i=0; i <
	 * max;i++) res.add(comp.get(i).intValue(), new Integer(0));
	 * 
	 * for(int i=0; i < comp.size(); i++ ) { res.add(comp.get(i).intValue(), new
	 * Integer(1)); } return res; }
	 */

	public void setEmptyEntailed(boolean v) {
		emptyEntailed = v;
	}

	public boolean getEmptyEntailed() {
		return emptyEntailed;
	}

	/*
	 * public void setEmptyIn (boolean v) { emptyIn = v; } public boolean
	 * getEmmpIn() { return emptyIn; }
	 * 
	 * public void setNonemptyIn (boolean v) { nonemptyIn = v; } public boolean
	 * getNonemptyIn() { return nonemptyIn; }
	 */
	public void setNonemptyEntailed(boolean v) {
		nonemptyEntailed = v;
	}

	public boolean getNonemptyEntailed() {
		return nonemptyEntailed;
	}

	public void setConf(Vector<Duo> v) {
		conf = v;
	}

	public Vector<Duo> getConf() {
		return conf;
	}

	public String getNewPosition() {
		return newPosition;
	}

	public void setNewPosition(String c) {
		newPosition = c;
	}

}
