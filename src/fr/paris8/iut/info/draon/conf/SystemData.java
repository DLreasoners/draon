/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
package fr.paris8.iut.info.draon.conf;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLPropertyAxiom;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.manchester.cs.owl.owlapi.OWLClassImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataAllValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataExactCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataHasValueImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataMaxCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataMinCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataSomeValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDatatypeImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLLiteralImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectAllValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectComplementOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectExactCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectHasValueImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectIntersectionOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectInverseOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectMaxCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectMinCardinalityImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectSomeValuesFromImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectUnionOfImpl;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.ObjectCell;
import fr.inrialpes.exmo.align.impl.edoal.ClassConstruction;
import fr.inrialpes.exmo.align.impl.edoal.ClassDomainRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassExpression;
import fr.inrialpes.exmo.align.impl.edoal.ClassOccurenceRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassTypeRestriction;
import fr.inrialpes.exmo.align.impl.edoal.ClassValueRestriction;
import fr.inrialpes.exmo.align.impl.edoal.Comparator;
import fr.inrialpes.exmo.align.impl.edoal.EDOALCell;
import fr.inrialpes.exmo.align.impl.edoal.Id;
import fr.inrialpes.exmo.align.impl.edoal.InstanceExpression;
import fr.inrialpes.exmo.align.impl.edoal.PathExpression;
import fr.inrialpes.exmo.align.impl.edoal.PropertyConstruction;
import fr.inrialpes.exmo.align.impl.edoal.PropertyRestriction;
import fr.inrialpes.exmo.align.impl.edoal.RelationConstruction;
import fr.inrialpes.exmo.align.impl.edoal.RelationRestriction;
import fr.inrialpes.exmo.align.impl.edoal.Value;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.HasInstanceRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.align.impl.rel.InstanceOfRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.align.parser.SyntaxElement.Constructor;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.paris8.iut.info.draon.DRAON;
import fr.paris8.iut.info.draon.DRAONException;
import fr.paris8.iut.info.draon.DRAONServices;

public class SystemData {
	final static Logger logger = LoggerFactory.getLogger(SystemData.class);

	// So, "oi:Thing" - > some global:conceptName; "oi:Nothing" - >
	// global:Nothing
	private HashSet<String> globalConceptSet = null;
	private HashSet<String> globalRoleSet = null;
	private HashSet<String> globalObjectRoleSet = null;
	private HashSet<String> globalDataRoleSet = null;
	private HashSet<String> globalIndSet = null;
	private Set<String> localUris = null;

	private Set<String> alignLocations = null;
	private Set<String> ontoLocations = null;
	private HashMap<String, String> uriToLocation = null;
	private String globalOntoUri = null;

	private LocalEntityMap localMap = null;
	private HashMap<String, GlobalEntity> globalEntityMap = null;
	private List<Alignment> network = null;
	// "Alignment Ontology" includes axioms corresponding to all correspondences
	// in alignments
	// private OWLOntology initialAlignOnto = null;
	private OWLOntology alignOnto = null;
	private OWLOntologyManager manager = null;
	private OWLDataFactory owlfactory = null;
	private OntologyFactory ontoFact = null;

	// rethink !!! that may be overflow
	int fileId = 0;
	public static String THING = "http://www.w3.org/2002/07/owl#Thing";
	public static String NOTHING = "http://www.w3.org/2002/07/owl#Nothing";

	public OWLOntology OWLAlign2OWLOnto(Alignment align) throws DRAONException {
		manager = OWLManager.createOWLOntologyManager();

		String alignUriStr = DRAON.uriRoot + DRAONServices.getGlobalOntoName() + ".owl";
		IRI alignUri = IRI.create(alignUriStr);

		OWLOntology OWLAlign = null;
		try {
			OWLAlign = manager.createOntology(alignUri);
		} catch (OWLOntologyCreationException ex) {
			// ex.printStackTrace();
			throw new DRAONException("DRAON : cannot create ontology", ex);
		}

		try {

			for (Cell cell : align) {
				OWLAxiom axiom = null;
				axiom = correspToAxiom(align, (ObjectCell) cell);
				AddAxiom addaxiom = new AddAxiom(OWLAlign, axiom);
				manager.applyChange(addaxiom);
			}

		} catch (DRAONException ex) {
			// ex.printStackTrace();
			throw new DRAONException("Alignment.");
		}

		return OWLAlign;
	}

	public List<Alignment> getNetwork() {
		return network;
	}

	// Extract data from alignments and store them into structures
	public SystemData(Hashtable<String, Alignment> alignTables, Semantics sem) throws DRAONException {

		if (alignTables.size() == 0 || alignTables == null ) 
		{
			throw new DRAONException("DRAON : empty system.");
		}
		manager = OWLManager.createOWLOntologyManager();
		owlfactory = manager.getOWLDataFactory();
		ontoFact = OntologyFactory.getFactory();

		network = new ArrayList<Alignment>();

		globalConceptSet = new HashSet<String>();
		globalRoleSet = new HashSet<String>();
		globalObjectRoleSet = new HashSet<String>();
		globalDataRoleSet = new HashSet<String>();
		globalIndSet = new HashSet<String>();

		localMap = new LocalEntityMap();
		globalEntityMap = new HashMap<String, GlobalEntity>();

		localUris = new HashSet<String>();
		alignLocations = new HashSet<String>();
		ontoLocations = new HashSet<String>();
		uriToLocation = new HashMap<String, String>();

		String alignOntoUriStr = DRAON.uriRoot + DRAONServices.getGlobalOntoName() + ".owl";
		IRI alignOntoUri = IRI.create(alignOntoUriStr);

		String alignUriStr = DRAON.uriRoot + DRAONServices.getGlobalOntoName() + ".owl";
		IRI alignUri = IRI.create(alignUriStr);
		// the uri of global alignment ontology
		globalOntoUri = alignUriStr;

		try {
			alignOnto = manager.createOntology(alignUri);
		} catch (OWLOntologyCreationException ex) {
			// ex.printStackTrace();
			throw new DRAONException("DRAON : cannot create temporary data.", ex);
		}

		globalConceptSet.add(THING);
		GlobalEntity ge = new GlobalEntity(THING, alignOntoUriStr, 0);
		globalEntityMap.put(THING, ge);

		OWLClass cls = owlfactory.getOWLClass(IRI.create(THING));

		OWLClassAxiom thAx = owlfactory.getOWLEquivalentClassesAxiom(cls, owlfactory.getOWLThing());

		AddAxiom thAddaxiom = new AddAxiom(alignOnto, thAx);

		manager.applyChange(thAddaxiom);

		Set<String> iterTable = alignTables.keySet();
		String localThing = alignOntoUriStr + "#" + Integer.toString(DRAONServices.getGlobalEntityId());
		// System.out.println("added # ... "+localThing2);

		// when alignments are merged, oi:Thing -> global:new concept name,
		// oi:Nothing -> global:Nothing.
		for (String iter : iterTable) {
			try {
				alignLocations.add(iter);
				BasicAlignment a = (BasicAlignment) alignTables.get(iter);
				network.add(a);
				String uri1 = a.getOntology1URI().toString();
				String uri2 = a.getOntology2URI().toString();
				localUris.add(uri1);
				localUris.add(uri2);

				if (!ontoLocations.contains(a.getFile1().toString())) {
					ontoLocations.add(a.getFile1().toString());
					uriToLocation.put(uri1, a.getFile1().toString());
				}

				if (!ontoLocations.contains(a.getFile2().toString())) {
					ontoLocations.add(a.getFile2().toString());
					uriToLocation.put(uri2, a.getFile2().toString());
				}
				String ontoFile1 = new File(a.getOntologyObject1().getFile().toString()).getAbsolutePath();
				// System.out.println("file="+ ontoFile1);
				// ontoFile1 = "file:/"+ ontoFile1;
				String ontoFile2 = new File(a.getOntologyObject2().getFile().toString()).getAbsolutePath();
				// ontoFile2 = "file:/"+ ontoFile2;

				OWLOntology onto1 = manager.loadOntologyFromOntologyDocument(new File(ontoFile1));
				OWLOntology onto2 = manager.loadOntologyFromOntologyDocument(new File(ontoFile2));
				String name1 = null;
				String name2 = null;
				// build vocabularies
				for (Cell cell : a) {
					if ((cell.getObject1AsURI(a) != null) && (cell.getObject2AsURI(a) != null)) {
						// if (!(cell instanceof BasicCell)) {
						IRI ur1 = IRI.create(cell.getObject1AsURI(a));
						String obUri1 = ur1.toString();
						// Object ob1 = onto1.getEntity(ur1);

						IRI ur2 = IRI.create(cell.getObject2AsURI(a));
						String obUri2 = ur2.toString();

						// local vocab. must include "#Thing"
						localMap.addConcept(uri1, THING);
						localMap.addConcept(uri2, THING);

						ge = new GlobalEntity(THING, uri1, 0);
						globalEntityMap.put(localThing, ge);

						ge = new GlobalEntity(THING, uri2, 0);
						globalEntityMap.put(localThing, ge);

						// The classes treated here include nominal concepts
						if (onto1.containsClassInSignature(ur1) && onto2.containsClassInSignature(ur2)) {
							localMap.addConcept(uri1, obUri1);
							localMap.addConcept(uri2, obUri2);
							// converting "http://www.w3.org/2002/07/owl#Thing" to a
							// normal concept in the global context
							if (obUri1.endsWith("#Thing")) {
								name1 = localThing;
							} else
								name1 = obUri1;
							// converting "http://www.w3.org/2002/07/owl#Thing" to a
							// normal concept in the global context
							if (obUri2.endsWith("#Thing")) {
								name2 = localThing;
							} else
								name2 = obUri2;

							if (!globalConceptSet.contains(name1)) {
								globalConceptSet.add(name1);
								ge = new GlobalEntity(name1, uri1, 0);
								globalEntityMap.put(name1, ge);
							}

							if (!globalConceptSet.contains(name2)) {
								globalConceptSet.add(name2);
								ge = new GlobalEntity(name2, uri2, 0);
								globalEntityMap.put(name2, ge);
							}
							// build Alignment Ontology
							OWLClass cls1 = owlfactory.getOWLClass(IRI.create(name1));
							OWLClass cls2 = owlfactory.getOWLClass(IRI.create(name2));

							OWLClassAxiom clsAx = null;

							if (cell.getRelation() instanceof EquivRelation) {
								clsAx = owlfactory.getOWLEquivalentClassesAxiom(cls1, cls2);

							} else if (cell.getRelation() instanceof SubsumedRelation) {
								clsAx = owlfactory.getOWLSubClassOfAxiom(cls1, cls2);

							} else if (cell.getRelation() instanceof SubsumeRelation) {
								clsAx = owlfactory.getOWLSubClassOfAxiom(cls2, cls1);

							} else if (cell.getRelation() instanceof IncompatRelation) {
								clsAx = owlfactory.getOWLDisjointClassesAxiom(cls1, cls2);
								if (sem == Semantics.IDDL)
									throw new DRAONException(
											"DRAON : OWLDisjointClassesAxiom is not supported : " + cell);
							} else {
								throw new DRAONException("Cannot convert correspondence " + cell);
							}

							AddAxiom addaxiom = new AddAxiom(alignOnto, clsAx);
							manager.applyChange(addaxiom);

						} else

						if (onto1.containsObjectPropertyInSignature(ur1)
								&& onto2.containsObjectPropertyInSignature(ur2)) {
							localMap.addRole(uri1, obUri1);
							localMap.addObjectRole(uri1, obUri1);

							if (!globalRoleSet.contains(obUri1)) {
								globalRoleSet.add(obUri1);
								ge = new GlobalEntity(obUri1, uri1, 1);
								globalEntityMap.put(obUri1, ge);
							}

							if (!globalRoleSet.contains(obUri2)) {
								globalRoleSet.add(obUri2);
								ge = new GlobalEntity(obUri2, uri2, 1);
								globalEntityMap.put(obUri2, ge);
							}

							if (!globalObjectRoleSet.contains(obUri1)) {
								globalObjectRoleSet.add(obUri1);
							}

							if (!globalObjectRoleSet.contains(obUri2)) {
								globalObjectRoleSet.add(obUri2);
							}

							localMap.addRole(uri2, obUri2);
							localMap.addObjectRole(uri2, obUri2);

							// OWLProperty prop1 = owlfactory.getOWLObjectProperty(
							// IRI.create(new URL( name1 )) );
							// OWLProperty prop2 = owlfactory.getOWLObjectProperty(
							// IRI.create(new URL( name2 ) ));
							// Set<OWLObjectPropertyExpression> propSet = new
							// HashSet<OWLObjectPropertyExpression>();
							// propSet.add( (OWLObjectPropertyExpression) prop1);
							// propSet.add( (OWLObjectPropertyExpression) prop2);
							// OWLPropertyAxiom propAx = null;

							OWLProperty aprop1 = owlfactory.getOWLObjectProperty(IRI.create(obUri1));
							OWLProperty aprop2 = owlfactory.getOWLObjectProperty(IRI.create(obUri2));
							Set<OWLObjectPropertyExpression> apropSet = new HashSet<OWLObjectPropertyExpression>();
							apropSet.add((OWLObjectPropertyExpression) aprop1);
							apropSet.add((OWLObjectPropertyExpression) aprop2);

							OWLPropertyAxiom apropAx = null;
							if (cell.getRelation() instanceof EquivRelation) {
								// propAx =
								// owlfactory.getOWLEquivalentObjectPropertiesAxiom(
								// propSet );
								apropAx = owlfactory.getOWLEquivalentObjectPropertiesAxiom(apropSet);
								// System.out.println( "equi p1 = " + obUri1);
								// System.out.println( "equi p2 = " + obUri2);

							} else if (cell.getRelation() instanceof SubsumedRelation) {
								// propAx =
								// owlfactory.getOWLSubObjectPropertyOfAxiom(
								// (OWLObjectPropertyExpression)prop1,
								// (OWLObjectPropertyExpression)prop2 );
								apropAx = owlfactory.getOWLSubObjectPropertyOfAxiom(
										(OWLObjectPropertyExpression) aprop1, (OWLObjectPropertyExpression) aprop2);
								// System.out.println( "sub p1 = " + obUri1);
								// System.out.println( "sub p2 = " + obUri2);
							} else if (cell.getRelation() instanceof SubsumeRelation) {

								// propAx =
								// owlfactory.getOWLSubObjectPropertyOfAxiom(
								// (OWLObjectPropertyExpression)prop2,
								// (OWLObjectPropertyExpression)prop1 );
								apropAx = owlfactory.getOWLSubObjectPropertyOfAxiom(
										(OWLObjectPropertyExpression) aprop2, (OWLObjectPropertyExpression) aprop1);

							} else if (cell.getRelation() instanceof IncompatRelation) {
								// propAx =
								// owlfactory.getOWLDisjointObjectPropertiesAxiom(
								// (OWLObjectPropertyExpression)prop1,
								// (OWLObjectPropertyExpression)prop2 );
								apropAx = owlfactory.getOWLDisjointObjectPropertiesAxiom(
										(OWLObjectPropertyExpression) aprop1, (OWLObjectPropertyExpression) aprop2);
								if (sem == Semantics.IDDL)
									throw new DRAONException(
											"DRAON : OWLDisjointObjectPropertiesAxiom is not supported : " + cell);
							} else {
								throw new DRAONException("Cannot convert correspondence " + obUri1 + "," + obUri2);
							}
							// AddAxiom addaxiom = new AddAxiom(initialAlignOnto,
							// propAx);

							// manager.applyChange( addaxiom );

							AddAxiom addaxiom = new AddAxiom(alignOnto, apropAx);
							manager.applyChange(addaxiom);
						} else

						if (onto1.containsDataPropertyInSignature(ur1) && onto2.containsDataPropertyInSignature(ur2)) {

							localMap.addRole(uri1, obUri1);
							localMap.addDataRole(uri1, obUri1);

							if (!globalRoleSet.contains(obUri1)) {
								globalRoleSet.add(obUri1);
								ge = new GlobalEntity(obUri1, uri1, 2);
								globalEntityMap.put(obUri1, ge);
							}

							if (!globalRoleSet.contains(obUri2)) {
								globalRoleSet.add(obUri2);
								ge = new GlobalEntity(obUri2, uri2, 1);
								globalEntityMap.put(obUri2, ge);
							}

							if (!globalDataRoleSet.contains(obUri1)) {
								globalDataRoleSet.add(obUri1);
							}

							if (!globalDataRoleSet.contains(obUri2)) {
								globalDataRoleSet.add(obUri2);
							}

							localMap.addRole(uri2, obUri2);
							localMap.addDataRole(uri2, obUri2);

							// OWLProperty prop1 = owlfactory.getOWLDataProperty(
							// IRI.create( name1 ) );
							// OWLProperty prop2 = owlfactory.getOWLDataProperty(
							// IRI.create( name2 ) );
							// Set<OWLDataPropertyExpression> propSet = new
							// HashSet<OWLDataPropertyExpression>();
							// propSet.add( (OWLDataPropertyExpression) prop1);
							// propSet.add( (OWLDataPropertyExpression) prop2);
							// OWLPropertyAxiom propAx = null;

							OWLProperty aprop1 = owlfactory.getOWLDataProperty(IRI.create(obUri1));
							OWLProperty aprop2 = owlfactory.getOWLDataProperty(IRI.create(obUri2));
							Set<OWLDataPropertyExpression> apropSet = new HashSet<OWLDataPropertyExpression>();
							apropSet.add((OWLDataPropertyExpression) aprop1);
							apropSet.add((OWLDataPropertyExpression) aprop2);
							OWLPropertyAxiom apropAx = null;
							if (cell.getRelation() instanceof EquivRelation) {
								// propAx =
								// owlfactory.getOWLEquivalentDataPropertiesAxiom(
								// propSet );
								apropAx = owlfactory.getOWLEquivalentDataPropertiesAxiom(apropSet);
								// System.out.println( "equi p1 = " + obUri1);
								// System.out.println( "equi p2 = " + obUri2);

							} else if (cell.getRelation() instanceof SubsumedRelation) {
								// propAx = owlfactory.getOWLSubDataPropertyOfAxiom(
								// (OWLDataPropertyExpression)prop1,
								// (OWLDataPropertyExpression)prop2 );
								apropAx = owlfactory.getOWLSubDataPropertyOfAxiom((OWLDataPropertyExpression) aprop1,
										(OWLDataPropertyExpression) aprop2);
								// System.out.println( "sub p1 = " + obUri1);
								// System.out.println( "sub p2 = " + obUri2);
							} else if (cell.getRelation() instanceof SubsumeRelation) {
								// propAx = owlfactory.getOWLSubDataPropertyOfAxiom(
								// (OWLDataPropertyExpression)prop2,
								// (OWLDataPropertyExpression)prop1 );
								apropAx = owlfactory.getOWLSubDataPropertyOfAxiom((OWLDataPropertyExpression) aprop2,
										(OWLDataPropertyExpression) aprop1);

							} else if (cell.getRelation() instanceof IncompatRelation) {
								// propAx =
								// owlfactory.getOWLDisjointDataPropertiesAxiom(
								// (OWLDataPropertyExpression)prop1,
								// (OWLDataPropertyExpression)prop2 );
								apropAx = owlfactory.getOWLDisjointDataPropertiesAxiom(
										(OWLDataPropertyExpression) aprop1, (OWLDataPropertyExpression) aprop2);
								if (sem == Semantics.IDDL)
									throw new DRAONException(
											"DRAON : OWLDisjointDataPropertiesAxiom is not supported : " + cell);
							} else {
								throw new DRAONException("Cannot convert correspondence " + obUri1 + "," + obUri2);
							}
							// AddAxiom addaxiom = new AddAxiom(initialAlignOnto,
							// propAx);
							// addaxiom.accept(xc);
							// manager.applyChange( addaxiom );

							AddAxiom addaxiom = new AddAxiom(alignOnto, apropAx);
							// addaxiom.accept(xc);
							manager.applyChange(addaxiom);

						} else

						// there may be two kinds of individual correspondences :
						// membership

						if ((onto1.containsIndividualInSignature(ur1) && onto2.containsClassInSignature(ur2))) {
							localMap.addInd(uri1, obUri1);

							if (!globalIndSet.contains(obUri1)) {
								globalIndSet.add(obUri1);
								ge = new GlobalEntity(obUri1, uri1, 3);
								globalEntityMap.put(obUri1, ge);
							}

							if (obUri2.endsWith("#Thing")) {
								name2 = alignOntoUriStr + "#" + Integer.toString(DRAONServices.getGlobalEntityId());
							} else
								name2 = obUri2;

							if (!globalConceptSet.contains(obUri2)) {
								globalConceptSet.add(name2);
							}

							localMap.addConcept(uri2, obUri2);

							// OWLIndividual ind1 =
							// owlfactory.getOWLNamedIndividual( IRI.create( name1 )
							// );
							// OWLClass cls1 = owlfactory.getOWLClass( IRI.create(
							// name2 ) );
							// OWLIndividualAxiom indAx = null;

							OWLIndividual aind1 = owlfactory.getOWLNamedIndividual(IRI.create(obUri1));
							OWLClass acls2 = owlfactory.getOWLClass(IRI.create(obUri2));
							OWLIndividualAxiom aindAx = null;
							if (cell.getRelation() instanceof InstanceOfRelation) {
								aindAx = owlfactory.getOWLClassAssertionAxiom(acls2, aind1);
							} else {
								throw new DRAONException("Cannot convert correspondence " + obUri1 + "," + obUri2);
							}

							AddAxiom addaxiom = new AddAxiom(alignOnto, aindAx);
							manager.applyChange(addaxiom);
						} else

						if ((onto1.containsClassInSignature(ur1) && onto2.containsIndividualInSignature(ur2))) {
							localMap.addInd(uri2, obUri2);

							if (!globalIndSet.contains(obUri2)) {
								globalIndSet.add(obUri2);
								ge = new GlobalEntity(obUri2, uri2, 3);
								globalEntityMap.put(obUri2, ge);
							}

							if (obUri1.endsWith("#Thing")) {
								name1 = alignOntoUriStr + "#" + Integer.toString(DRAONServices.getGlobalEntityId());
							} else
								name1 = obUri1;

							if (!globalConceptSet.contains(obUri1)) {
								globalConceptSet.add(name1);
							}

							localMap.addConcept(uri1, obUri1);

							OWLIndividual aind1 = owlfactory.getOWLNamedIndividual(IRI.create(obUri2));
							OWLClass acls1 = owlfactory.getOWLClass(IRI.create(obUri1));
							OWLIndividualAxiom aindAx = null;
							if (cell.getRelation() instanceof HasInstanceRelation) {

								aindAx = owlfactory.getOWLClassAssertionAxiom(acls1, aind1);

							} else {
								throw new DRAONException("Cannot convert correspondence " + obUri1 + "," + obUri2);
							}

							AddAxiom addaxiom = new AddAxiom(alignOnto, aindAx);
							manager.applyChange(addaxiom);
						} else {
							System.err.println("Correspondence not supported :" + obUri1 + "," + obUri2);
						}
						// process EDOAL cell
					} else {
						processEDOALCell((EDOALCell) cell, localThing, uri1, uri2, alignOntoUriStr, onto1, onto2, sem);
					}
				} // for Cells
			} catch (AlignmentException ex) {
				throw new DRAONException("Cannot convert correspondence.");
			} catch (OWLOntologyCreationException e) {
				e.printStackTrace();
			}
		} // for iterTable
	}

	public void processEDOALCell(EDOALCell cell, String thing, String uri1, String uri2, String alignOntoUriStr,
			OWLOntology onto1, OWLOntology onto2, Semantics sem) throws DRAONException {
		GlobalEntity ge = new GlobalEntity(THING, alignOntoUriStr, 0);

		// local vocab. must include "#Thing"
		localMap.addConcept(uri1, THING);
		localMap.addConcept(uri2, THING);

		ge = new GlobalEntity(THING, uri1, 0);
		globalEntityMap.put(thing, ge);

		ge = new GlobalEntity(THING, uri2, 0);
		globalEntityMap.put(thing, ge);
		// build Alignment Ontology
		try {
			OWLClassExpression cls1 = getCellClass1AsOWL(cell, onto1);
			OWLClassExpression cls2 = getCellClass2AsOWL(cell, onto2);
			if( !(cls1.getClassExpressionType()==ClassExpressionType.OBJECT_COMPLEMENT_OF) ||
				!(cls2.getClassExpressionType()==ClassExpressionType.OBJECT_COMPLEMENT_OF) )
			{
			   //if( ! (cls1.getClassExpressionType()==ClassExpressionType.OBJECT_COMPLEMENT_OF) )
				//    cls1 = owlfactory.getOWLObjectComplementOf(cls1);
			   if( ! (cls2.getClassExpressionType()==ClassExpressionType.OBJECT_COMPLEMENT_OF) )
			   		cls2 = owlfactory.getOWLObjectComplementOf(cls2);
			}

			if ((cls1 == null) || (cls2 == null)) {
				System.err.println(
						"An EDOALCell has been skipped because one of its elements was not in the aligned ontologies");
			} else {
				OWLClassAxiom clsAx = null;

				if (cell.getRelation() instanceof EquivRelation) {
					clsAx = owlfactory.getOWLEquivalentClassesAxiom(cls1, cls2);

				} else if (cell.getRelation() instanceof SubsumedRelation) {
					clsAx = owlfactory.getOWLSubClassOfAxiom(cls1, cls2);

				} else if (cell.getRelation() instanceof SubsumeRelation) {
					clsAx = owlfactory.getOWLSubClassOfAxiom(cls2, cls1);

				} else if (cell.getRelation() instanceof IncompatRelation) {
					clsAx = owlfactory.getOWLDisjointClassesAxiom(cls1, cls2);
					if (sem == Semantics.IDDL)
						throw new DRAONException("DRAON : OWLDisjointClassesAxiom is not supported : " + cell);
				} else {
					throw new DRAONException("Cannot convert correspondence " + cell);
				}

				AddAxiom addaxiom = new AddAxiom(alignOnto, clsAx);
				manager.applyChange(addaxiom);
				//System.out.println("AXIOM = "+ clsAx);
			}
		} catch (AlignmentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Jérôme Euzenat
	 */
	public OWLClassExpression getCellClass1AsOWL(EDOALCell c, OWLOntology o1) throws AlignmentException {
		return getClassExpressionAsOWL(c.getObject1(), o1);
	}

	/**
	 * @author Jérôme Euzenat
	 */
	public OWLClassExpression getCellClass2AsOWL(EDOALCell c, OWLOntology o2) throws AlignmentException {
		return getClassExpressionAsOWL(c.getObject2(), o2);
	}

	/**
	 * @author Jérôme Euzenat
	 */
	private OWLEntity getEntity(OWLOntology o, URI u) {
		if (!o.getEntitiesInSignature(IRI.create(u)).iterator().hasNext())
			return null;
		return o.getEntitiesInSignature(IRI.create(u)).iterator().next();
	}

	/**
	 * @author Jérôme Euzenat
	 */
	public OWLClassExpression getClassExpressionAsOWL(Object expr, OWLOntology o) throws AlignmentException {
		if (expr instanceof Id) {
			return (OWLClass) getEntity(o, ((Id) expr).getURI());
		} else if (expr instanceof ClassConstruction) {
			Set<OWLClassExpression> components = new HashSet<OWLClassExpression>();
			for (final ClassExpression ce : ((ClassConstruction) expr).getComponents()) {
				components.add(getClassExpressionAsOWL(ce, o));
			}
			switch (((ClassConstruction) expr).getOperator()) {
			case AND: // TODO: creating a new OWLClassImpl may not be the best
				if (components.size() == 0)
					return new OWLClassImpl(IRI.create("http://www.w3.org/2002/07/owl#Thing"));
				else
					return new OWLObjectIntersectionOfImpl(components);
			case OR:
				if (components.size() == 0)
					return new OWLClassImpl(IRI.create("http://www.w3.org/2002/07/owl#Nohing"));
				else
					return new OWLObjectUnionOfImpl(components);
			case NOT:
				if (components.size() == 0)
					throw new AlignmentException("Complement constructor cannot be empty");
				else {
					return new OWLObjectComplementOfImpl(components.iterator().next());
				}
			default:
				throw new AlignmentException("Unknown class constructor : " + ((ClassConstruction) expr).getOperator());
			}
			/// Below depend on object/attribute...
		} else {
			OWLPropertyExpression pe = getPathExpressionAsOWL(((ClassRestriction) expr).getRestrictionPath(), o);
			if (expr instanceof ClassValueRestriction) {
				if (pe instanceof OWLObjectPropertyExpression) {
					OWLIndividual ind = getInstanceExpressionAsOWL(
							(InstanceExpression) ((ClassValueRestriction) expr).getValue(), o);
					return new OWLObjectHasValueImpl((OWLObjectPropertyExpression) pe, ind);
				} else {
					Value val = (Value) ((ClassValueRestriction) expr).getValue();
					OWLDatatype type = null;
					if (val.getType() != null)
						type = new OWLDatatypeImpl(IRI.create(val.getType()));
					OWLLiteral lit = new OWLLiteralImpl(val.getValue(), val.getLang(), type);
					return new OWLDataHasValueImpl((OWLDataPropertyExpression) pe, lit);
				}
			} else if (expr instanceof ClassTypeRestriction) {
				// In principle, it only works for ALL
				// TODO: Only works if the result of getType is a URI/IRI
				OWLDataRange type = new OWLDatatypeImpl(IRI.create(((ClassTypeRestriction) expr).getType().getType()));
				if (((ClassTypeRestriction) expr).isUniversal()) {
					return new OWLDataAllValuesFromImpl((OWLDataPropertyExpression) pe, type);
				} else {
					return new OWLDataSomeValuesFromImpl((OWLDataPropertyExpression) pe, type);
				}
			} else if (expr instanceof ClassDomainRestriction) {
				OWLClassExpression domain = getClassExpressionAsOWL(((ClassDomainRestriction) expr).getDomain(), o);
				if (((ClassDomainRestriction) expr).isUniversal()) {
					return new OWLObjectAllValuesFromImpl((OWLObjectPropertyExpression) pe, domain);
				} else {
					return new OWLObjectSomeValuesFromImpl((OWLObjectPropertyExpression) pe, domain);
				}
			} else if (expr instanceof ClassOccurenceRestriction) {
				// TODO: the interface needs a OWLClassExpression filler @nonnull
				int card = ((ClassOccurenceRestriction) expr).getOccurence();
				if (pe instanceof OWLObjectPropertyExpression) {
					Comparator comp = ((ClassOccurenceRestriction) expr).getComparator();
					if (comp == Comparator.EQUAL)
						return new OWLObjectExactCardinalityImpl((OWLObjectPropertyExpression) pe, card, null);
					else if (comp == Comparator.LOWER)
						return new OWLObjectMinCardinalityImpl((OWLObjectPropertyExpression) pe, card, null);
					else if (comp == Comparator.GREATER)
						return new OWLObjectMaxCardinalityImpl((OWLObjectPropertyExpression) pe, card, null);
				} else {
					Comparator comp = ((ClassOccurenceRestriction) expr).getComparator();
					if (comp == Comparator.EQUAL)
						return new OWLDataExactCardinalityImpl((OWLDataPropertyExpression) pe, card, null);
					else if (comp == Comparator.LOWER)
						return new OWLDataMinCardinalityImpl((OWLDataPropertyExpression) pe, card, null);
					else if (comp == Comparator.GREATER)
						return new OWLDataMaxCardinalityImpl((OWLDataPropertyExpression) pe, card, null);
				}
			}
			throw new AlignmentException("Not yet implemented: dereferencing non URI classes");
		}
	}

	/**
	 * @author Jérôme Euzenat
	 */
	public OWLPropertyExpression getPathExpressionAsOWL(PathExpression expr, OWLOntology o) throws AlignmentException {
		if (expr instanceof Id) {
			return (OWLProperty) getEntity(o, ((Id) expr).getURI());
		} else if (expr instanceof PropertyConstruction) {
			// in the visitor I tried with propertyChainAxioms, but this is likely illegal
			// (and more useful for relation)
			logger.warn("Cannot transcribe property construction {} in OWL",
					((PropertyConstruction) expr).getOperator());
			throw new AlignmentException("Cannot transcribe property construction in OWL");
		} else if (expr instanceof RelationConstruction) {
			final Constructor op = ((RelationConstruction) expr).getOperator();
			if (op == Constructor.INVERSE) {
				return new OWLObjectInverseOfImpl((OWLObjectPropertyExpression) getPathExpressionAsOWL(
						((RelationConstruction) expr).getComponents().iterator().next(), o));
			} else {
				// in the visitor I tried with propertyChainAxioms, but this is likely illegal
				// (and more useful for relation)
				logger.warn("Cannot transcribe relation construction {} in OWL", op);
				throw new AlignmentException("Cannot transcribe relation construction " + op + " in OWL");
			}
		} else if (expr instanceof PropertyRestriction) {
			// in the visitor I tried with propertyChainAxioms, but this is likely illegal
			logger.warn("Cannot transcribe property restrictions in OWL");
			throw new AlignmentException("Cannot transcribe property restrictions in OWL");
		} else if (expr instanceof RelationRestriction) {
			logger.warn("Cannot transcribe relation restrictions in OWL");
			throw new AlignmentException("Cannot transcribe relation restrictions in OWL");
		} else
			throw new AlignmentException("Unknown property/relation expression " + expr);
	}

	/**
	 * @author Jérôme Euzenat
	 */
	public OWLIndividual getInstanceExpressionAsOWL(InstanceExpression expr, OWLOntology o) throws AlignmentException {
		if (expr instanceof Id) {
			return (OWLIndividual) getEntity(o, ((Id) expr).getURI());
		} else
			throw new AlignmentException("Not yet implemented: dereferencing non URI paths");
	}

	// JE : In fact, it should be possible to do all EDOAL
	public OWLAxiom correspToAxiom(Alignment align, Cell corresp) throws DRAONException {
		// LoadedOntology onto1 = al.ontology1();
		// LoadedOntology onto2 = al.ontology2();
		// Object e1 = corresp.getObject1();
		// Object e2 = corresp.getObject2();
		Relation r = corresp.getRelation();

		BasicAlignment al = (BasicAlignment) align;
		// retrieve entity1 and entity2
		// create the axiom in function of their labels

		try {
			IRI ur1 = IRI.create(corresp.getObject1AsURI(al));
			IRI ur2 = IRI.create(corresp.getObject2AsURI(al));

			String obUri1 = ur1.toString();
			String obUri2 = ur2.toString();
			String ontoFile1 = new File(al.getOntologyObject1().getFile().toString()).getAbsolutePath();
			String ontoFile2 = new File(al.getOntologyObject2().getFile().toString()).getAbsolutePath();
			OWLOntology onto1 = manager.loadOntologyFromOntologyDocument(new File(ontoFile1));
			OWLOntology onto2 = manager.loadOntologyFromOntologyDocument(new File(ontoFile2));

			// LoadedOntology onto1 =
			// ontoFact.loadOntology(al.getOntologyObject1().getFile());
			// LoadedOntology onto2 =
			// ontoFact.loadOntology(al.getOntologyObject2().getFile());

			if (onto1.containsClassInSignature(ur1)) {
				if (onto2.containsClassInSignature(ur2)) {
					OWLClass entity1 = owlfactory.getOWLClass(ur1);
					OWLClass entity2 = owlfactory.getOWLClass(ur2);
					if (r instanceof EquivRelation) {
						return owlfactory.getOWLEquivalentClassesAxiom(entity1, entity2);
					} else if (r instanceof SubsumeRelation) {
						return owlfactory.getOWLSubClassOfAxiom(entity2, entity1);
					} else if (r instanceof SubsumedRelation) {
						return owlfactory.getOWLSubClassOfAxiom(entity1, entity2);
					} else if (r instanceof IncompatRelation) {

						return owlfactory.getOWLDisjointClassesAxiom(entity1, entity2);
					}
				} else if (onto2.containsIndividualInSignature(ur2) && (r instanceof HasInstanceRelation)) {
					return owlfactory.getOWLClassAssertionAxiom(owlfactory.getOWLClass(ur1),
							owlfactory.getOWLNamedIndividual(ur2));
				}
			} else if (onto1.containsDataPropertyInSignature(ur1) && onto2.containsDataPropertyInSignature(ur2)) {
				OWLDataProperty entity1 = owlfactory.getOWLDataProperty(ur1);
				OWLDataProperty entity2 = owlfactory.getOWLDataProperty(ur2);
				if (r instanceof EquivRelation) {
					return owlfactory.getOWLEquivalentDataPropertiesAxiom(entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					return owlfactory.getOWLSubDataPropertyOfAxiom(entity2, entity1);
				} else if (r instanceof SubsumedRelation) {
					return owlfactory.getOWLSubDataPropertyOfAxiom(entity1, entity2);
				} else if (r instanceof IncompatRelation) {

					return owlfactory.getOWLDisjointDataPropertiesAxiom(entity1, entity2);
				}
			} else if (onto1.containsDataPropertyInSignature(ur1) && onto2.containsDataPropertyInSignature(ur2)) {
				OWLObjectProperty entity1 = owlfactory.getOWLObjectProperty(ur1);
				OWLObjectProperty entity2 = owlfactory.getOWLObjectProperty(ur2);
				if (r instanceof EquivRelation) {
					return owlfactory.getOWLEquivalentObjectPropertiesAxiom(entity1, entity2);
				} else if (r instanceof SubsumeRelation) {
					return owlfactory.getOWLSubObjectPropertyOfAxiom(entity2, entity1);
				} else if (r instanceof SubsumedRelation) {
					return owlfactory.getOWLSubObjectPropertyOfAxiom(entity1, entity2);
				} else if (r instanceof IncompatRelation) {

					return owlfactory.getOWLDisjointObjectPropertiesAxiom(entity1, entity2);
				}
			} else if (onto1.containsIndividualInSignature(ur1)) {
				if (onto2.containsIndividualInSignature(ur2)) {
					OWLIndividual entity1 = owlfactory.getOWLNamedIndividual(ur1);
					OWLIndividual entity2 = owlfactory.getOWLNamedIndividual(ur2);
					if (r instanceof EquivRelation) {
						return owlfactory.getOWLSameIndividualAxiom(entity1, entity2);
					} else if (r instanceof IncompatRelation) {
						return owlfactory.getOWLDifferentIndividualsAxiom(entity1, entity2);
					}
				} else if (onto2.containsClassInSignature(ur1) && (r instanceof InstanceOfRelation)) {
					return owlfactory.getOWLClassAssertionAxiom(owlfactory.getOWLClass(ur2),
							owlfactory.getOWLNamedIndividual(ur1));
				}
			}
		} catch (AlignmentException ex) {
			throw new DRAONException("Cannot convert correspondence.");
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		throw new DRAONException("Cannot convert correspondence " + corresp);
	}
	// Jerome : In fact, it should be possible to do all EDOAL
	/*
	 * public OWLAxiom correspToAxiom(Alignment align, Cell corresp) throws
	 * DRAONException {
	 * 
	 * // LoadedOntology onto1 = al.ontology1(); // LoadedOntology onto2 =
	 * al.ontology2(); Object e1 = corresp.getObject1(); Object e2 =
	 * corresp.getObject2(); Relation r = corresp.getRelation();
	 * 
	 * BasicAlignment al = (BasicAlignment) align; // retrieve entity1 and entity2
	 * // create the axiom in function of their labels
	 * 
	 * try { LoadedOntology onto1 = ontoFact.loadOntology(al
	 * .getOntologyObject1().getFile()); LoadedOntology onto2 =
	 * ontoFact.loadOntology(al .getOntologyObject2().getFile());
	 * 
	 * if (onto1.isClass(e1)) { if (onto2.isClass(e2)) { OWLClass entity1 =
	 * owlfactory.getOWLClass(IRI.create(onto1 .getEntityURI(e1))); OWLClass entity2
	 * = owlfactory.getOWLClass(IRI.create(onto2 .getEntityURI(e2))); if (r
	 * instanceof EquivRelation) { return
	 * owlfactory.getOWLEquivalentClassesAxiom(entity1, entity2); } else if (r
	 * instanceof SubsumeRelation) { return
	 * owlfactory.getOWLSubClassOfAxiom(entity2, entity1); } else if (r instanceof
	 * SubsumedRelation) { return owlfactory.getOWLSubClassOfAxiom(entity1,
	 * entity2); } else if (r instanceof IncompatRelation) { return
	 * owlfactory.getOWLDisjointClassesAxiom(entity1, entity2); } } else if
	 * (onto2.isIndividual(e2) && (r instanceof HasInstanceRelation)) { return
	 * owlfactory.getOWLClassAssertionAxiom(owlfactory
	 * .getOWLClass(IRI.create(onto1.getEntityURI(e1))),
	 * owlfactory.getOWLNamedIndividual(IRI.create(onto2 .getEntityURI(e2)))); } }
	 * else if (onto1.isDataProperty(e1) && onto2.isDataProperty(e2)) {
	 * OWLDataProperty entity1 = owlfactory.getOWLDataProperty(IRI
	 * .create(onto1.getEntityURI(e1))); OWLDataProperty entity2 =
	 * owlfactory.getOWLDataProperty(IRI .create(onto2.getEntityURI(e2))); if (r
	 * instanceof EquivRelation) { return
	 * owlfactory.getOWLEquivalentDataPropertiesAxiom( entity1, entity2); } else if
	 * (r instanceof SubsumeRelation) { return
	 * owlfactory.getOWLSubDataPropertyOfAxiom(entity2, entity1); } else if (r
	 * instanceof SubsumedRelation) { return
	 * owlfactory.getOWLSubDataPropertyOfAxiom(entity1, entity2); } else if (r
	 * instanceof IncompatRelation) { return
	 * owlfactory.getOWLDisjointDataPropertiesAxiom( entity1, entity2); } } else if
	 * (onto1.isObjectProperty(e1) && onto2.isObjectProperty(e2)) {
	 * OWLObjectProperty entity1 = owlfactory.getOWLObjectProperty(IRI
	 * .create(onto1.getEntityURI(e1))); OWLObjectProperty entity2 =
	 * owlfactory.getOWLObjectProperty(IRI .create(onto2.getEntityURI(e2))); if (r
	 * instanceof EquivRelation) { return
	 * owlfactory.getOWLEquivalentObjectPropertiesAxiom( entity1, entity2); } else
	 * if (r instanceof SubsumeRelation) { return
	 * owlfactory.getOWLSubObjectPropertyOfAxiom(entity2, entity1); } else if (r
	 * instanceof SubsumedRelation) { return
	 * owlfactory.getOWLSubObjectPropertyOfAxiom(entity1, entity2); } else if (r
	 * instanceof IncompatRelation) { return
	 * owlfactory.getOWLDisjointObjectPropertiesAxiom( entity1, entity2); } } else
	 * if (onto1.isIndividual(e1)) { if (onto2.isIndividual(e2)) { OWLIndividual
	 * entity1 = owlfactory .getOWLNamedIndividual(IRI.create(onto1
	 * .getEntityURI(e1))); OWLIndividual entity2 = owlfactory
	 * .getOWLNamedIndividual(IRI.create(onto2 .getEntityURI(e2))); if (r instanceof
	 * EquivRelation) { return owlfactory.getOWLSameIndividualAxiom(entity1,
	 * entity2); } else if (r instanceof IncompatRelation) { return
	 * owlfactory.getOWLDifferentIndividualsAxiom( entity1, entity2); } } else if
	 * (onto2.isClass(e2) && (r instanceof InstanceOfRelation)) { return
	 * owlfactory.getOWLClassAssertionAxiom(owlfactory
	 * .getOWLClass(IRI.create(onto2.getEntityURI(e2))),
	 * owlfactory.getOWLNamedIndividual(IRI.create(onto1 .getEntityURI(e1)))); } } }
	 * catch (OntowrapException owex) { throw new
	 * DRAONException("Error interpreting URI " + owex); } throw new
	 * DRAONException("Cannot convert correspondence " + corresp); }
	 */

	public HashSet<String> getGlobalConceptSet() {
		return globalConceptSet;

	}

	public HashSet<String> getGlobalObjectRoleSet() {
		return globalObjectRoleSet;

	}

	public HashSet<String> getGlobalDataRoleSet() {
		return globalDataRoleSet;

	}

	public HashSet<String> getGlobalRoleSet() {
		return globalRoleSet;

	}

	public HashSet<String> getGlobalIndSet() {
		return globalIndSet;

	}

	public LocalEntityMap getLocalMap() {
		return localMap;

	}

	public HashMap<String, GlobalEntity> getGlobalEntityMap() {
		return globalEntityMap;

	}

	public void setGlobalEntityMap(HashMap<String, GlobalEntity> val) {
		globalEntityMap = val;

	}

	public OWLOntology getOWLAlignOnto() {
		return alignOnto;
	}

	public void setOWLAlignOnto(OWLOntology tmp) {
		alignOnto = tmp;
	}

	public String getGlobalOntoUri() {
		return globalOntoUri;
	}

	public String getLocationFromUri(String uri) {
		return uriToLocation.get(uri);
	}

	public String getLCsByGC(String g) {
		for (String t : globalConceptSet) {
			if (t.equals(g))
				return t;
		}
		return null;
	}

	public Set<String> getLCsByGC(Set<String> g) {
		Set<String> res = new HashSet();
		for (String t : g) {
			res.add(getLCsByGC(t));
		}
		return res;
	}

	public String getLRsByGR(String g) {
		// Set<String> gl = globalRoleMap.keySet();
		// Set<String> res = new HashSet();
		for (String t : globalRoleSet) {
			if (t.equals(g))
				return t;
		}
		return null;
	}

	public Set<String> getLRsByGR(Set<String> g) {
		Set<String> res = new HashSet();
		for (String t : g) {
			res.add(getLRsByGR(t));
		}
		return res;
	}

	public Set<String> getLocalUris() {
		return localUris;
	}

	public Set<String> getAlignLocations() {
		return alignLocations;
	}

	public Set<String> getOntoLocations() {
		return ontoLocations;

	}
}

