/*
 * $Id$
 *
 * Copyright (C) INRIA de Grenoble, 2007-2009
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.paris8.iut.info.draon.localnode;

//import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import org.semanticweb.HermiT.Reasoner;

import fr.paris8.iut.info.draon.DRAON;

import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.BufferingMode;
import org.semanticweb.owlapi.reasoner.IllegalConfigurationException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

public class LocalReasonerFactory implements OWLReasonerFactory {

	public String getReasonerName() {
		return "DRAON reasoner";
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology owlo) {
		// PelletReasoner reasoner = new PelletReasoner(owlo,
		// BufferingMode.NON_BUFFERING);
		Reasoner reasoner = new Reasoner(owlo);
		// DRAONReasoner reasoner = new DRAONReasoner();
		// reasoner.localReasoner.loadOntology(owlo);
		return reasoner;
	}

	public OWLReasoner createReasoner(OWLOntology owlo) {
		// DRAONReasoner reasoner = new DRAONReasoner();
		// reasoner.localReasoner.loadOntology(owlo);
		// PelletReasoner reasoner = new PelletReasoner(owlo,
		// BufferingMode.BUFFERING);
		Reasoner reasoner = new Reasoner(owlo);
		return reasoner;
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology owlo,
			OWLReasonerConfiguration owlrc)
			throws IllegalConfigurationException {
		// PelletReasoner reasoner = new PelletReasoner(owlo,
		// BufferingMode.NON_BUFFERING);
		Reasoner reasoner = new Reasoner(owlo);
		return reasoner;
	}

	public OWLReasoner createReasoner(OWLOntology owlo,
			OWLReasonerConfiguration owlrc)
			throws IllegalConfigurationException {
		// PelletReasoner reasoner = new PelletReasoner(owlo,
		// BufferingMode.NON_BUFFERING);
		Reasoner reasoner = new Reasoner(owlo);
		return reasoner;
	}

}
