package fr.paris8.iut.info.draon.localnode;

/*
 * $Id$
 *
 * Copyright (C) Paris8-IUT de Montreuil, 2012-2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */


/*
 * By PHAM Tuan Anh *
 */

import java.net.URL;

import org.semanticweb.owlapi.owllink.OWLlinkReasonerConfiguration;
import org.semanticweb.owlapi.owllink.server.AbstractOWLlinkReasonerConfiguration;
import org.semanticweb.owlapi.owllink.server.OWLlinkHTTPXMLServer;
import org.semanticweb.owlapi.owllink.server.OWLlinkServer;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

public class LocalReasonerNode implements OWLlinkServer 
{
	private OWLlinkReasonerConfiguration reasonerConfiguration = null;
	private OWLlinkServer server = null;

	private String url = null;// Configure the server end-point
	private OWLReasonerFactory reasonerFactory;;
	private int port = 8080;
	public LocalReasonerNode(String url, int port) 
	{
		this.url = url;
		this.port = port;
	}
	// Fonction pour démarrer le serveur
	public void run() 
	{
		try 
		{
			reasonerConfiguration = new OWLlinkReasonerConfiguration(new URL(
					url + ":" + port)); // Configure la configuration de
										// raisonneur
			reasonerFactory = new LocalReasonerFactory();
			AbstractOWLlinkReasonerConfiguration serverConfiguration = new AbstractOWLlinkReasonerConfiguration(
					reasonerConfiguration);
			server = new OWLlinkHTTPXMLServer(reasonerFactory, serverConfiguration, port);
			server.run();
		} catch (Exception e) {

		}
	}

	// Fonction pour arrêter le serveur
	public void stop() throws InterruptedException 
	{
		if (server != null) 
		{
			try {
				server.stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
